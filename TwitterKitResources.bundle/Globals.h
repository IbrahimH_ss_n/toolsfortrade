//
//  Globals.h
//  mIracleNoodle
//
//  Created by Matrid on 10/02/16.
//  Copyright © 2016 Matrid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Globals : NSObject

+ (void)customHeader:(UIViewController *)views headerTitle:(NSString *)headerTitle hideBtn:(BOOL)hideBtn currentView:(int)currentView;
+ (void)bgImageView:(UIViewController *)views bgLogoYaxis:(int)YAxis hideBgLogo:(BOOL)flag;
+ (void)ssmediaView:(UIViewController *)views frame:(CGRect)frame;
+ (CGRect)frame:(NSString *)string font:(UIFont *)font;
+ (BOOL)loginFor:(NSString *)login;
+ (void)mapController:(UIViewController *)views;
+ (void)navigationView:(BOOL)flag;

@end
