//
//  DownloadableCellTableViewCell.h
//  ShopApp
//
//  Created by Matrid on 06/06/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface DownloadableCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblOder_Id;
@property (weak, nonatomic) IBOutlet UILabel *lblOrder_Date;
@property (weak, nonatomic) IBOutlet UILabel *lblOderDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnDownloadLink;

@end
