//
//  WishListViewController.m
//  ShopApp
//
//  Created by Matrid on 01/03/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import "WishListViewController.h"

@interface WishListViewController ()

@end

@implementation WishListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    self.view.backgroundColor  screenBackgroundColor;
    
    [Global customHeader:self headerTitle:@"W I S H L I S T" itemFound:NO hideBackButton:NO hideSearchButton:YES hideNotificationButton:YES];
    
    [Global customFooter:self.view];
    
    [self wishList];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    labelItem = (UILabel *)[self.view viewWithTag:111];
    
    labelHeader = (UILabel *)[self.view viewWithTag:1111];
    headerFrame = labelHeader.frame;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrWishList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *defaultCell = @"cellDefault";
    WishListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:defaultCell];
    
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"WishListTableViewCell" owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activity.center =cell.imgWishList.center;
    activity.hidesWhenStopped = YES;
    [activity startAnimating];
    [cell addSubview:activity];
    
    [cell.imgWishList sd_setImageWithURL:[[arrWishList objectAtIndex:indexPath.row] valueForKey:@"image"] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
        // Get rid of the activity indicator when the image has been loaded
        [activity stopAnimating];
    }];
    
    
    NSString *cur=[[arrWishList objectAtIndex:indexPath.row] valueForKey:@"currency"];
    
    cell.lblCurrencyPrice.text=[Global PriceRoundOffToString:[[arrWishList objectAtIndex:indexPath.row] valueForKey:@"price"] currency:cur];
    
    cell.imgWishList.contentMode=UIViewContentModeScaleAspectFit;
    cell.lblDiscription.text = [[arrWishList objectAtIndex:indexPath.row] valueForKey:@"product_name"];
    cell.lblDiscription.font = [UIFont fontWithName:gmFont size:13];
    [cell.btnDelete addTarget:self action:@selector(btnDeleteWish:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnDelete.tag = indexPath.row;
    
    cell.btnAddToCart.layer.cornerRadius = 3;
    cell.btnAddToCart.clipsToBounds = YES;
    cell.btnAddToCart.titleLabel.font = [UIFont fontWithName:gmFont size:12];
    [cell.btnAddToCart addTarget:self action:@selector(addToCart:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnAddToCart.tag=indexPath.row;
    
    //    UILabel *lblLine = [[UILabel alloc]init];
    //    lblLine.backgroundColor minGrayColor;
    //    lblLine.frame = CGRectMake(0, 97, 320, 1);
    //    [cell addSubview:lblLine];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProductDetailViewController *productDetail=[[ProductDetailViewController alloc]init];
    productDetail.product_Id = [[arrWishList objectAtIndex:indexPath.row] valueForKey:@"product_id"];
    [self.navigationController pushViewController:productDetail animated:YES];
}

-(void) addToCart:(UIButton*)sender {
    
}

-(void) btnDeleteWish:(UIButton*)sender {
    
    deleteWishList = sender.tag;
    
    UIAlertController * alert=[UIAlertController
                               
                               alertControllerWithTitle:@"Are You Sure!" message:@""preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes, Delete it"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    selectedWishList = [[arrWishList objectAtIndex:sender.tag] valueForKey:@"wishlist_item_id"];
                                    //[arrWishList removeObjectAtIndex:sender.tag];
                                    [self removeToWishList];
                                }];
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"No"
                               style:UIAlertActionStyleDestructive
                               handler:^(UIAlertAction * action)
                               {
                                   
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)removeToWishList {
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:selectedWishList,@"id", nil];
    [Global urlString:[apiDomain stringByAppendingString:apiUrlRemoveWishList] parameter:dic indicator:YES currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response) {
        if(result)
        {
            if ([[response valueForKey:@"status"] isEqualToString:@"SUCCESS"]) {
                if (arrWishList.count == 0) {
                    labelItem.text = @"";
                    labelHeader.frame = headerFrame;
                    [Global addErrorView:self errorMessage:@"No More items in 'Wishlist'"];
                    labelItem.text = [NSString stringWithFormat:@"%lu %@",(unsigned long)arrWishList.count,@"item found"];
                    //[Global alertAdd:@"No More items in wishlist" withTitle:@"No Data" navBack:NO];
                }
                else {
                    [arrWishList removeObjectAtIndex:deleteWishList];
                    
                    if (arrWishList.count == 0) {
                        labelItem.text = @"";
                        labelHeader.frame = headerFrame;
                        [Global addErrorView:self errorMessage:@"No More items in 'Wishlist'"];
                        [_tblWishList reloadData];
                        //labelItem.text = [NSString stringWithFormat:@"%lu %@",(unsigned long)arrWishList.count,@"items found"];
                        //[Global alertAdd:@"No More items in wishlist" withTitle:@"No Data" navBack:NO];
                    }
                    else {
                        
                        [_tblWishList reloadData];
                        labelItem.text = [NSString stringWithFormat:@"%lu %@",(unsigned long)arrWishList.count,@"items found"];
                        //[Global alertAdd:[response valueForKey:@"msg"] withTitle:nil navBack:NO];
                        
                    }
                }
            }
        }
    }];
}

//apiUrlWishLists
- (void) wishList {
    
    NSString *custome_Id = [[NSUserDefaults standardUserDefaults] objectForKey:@"Uid"];
    NSDictionary *dicty = [NSDictionary dictionaryWithObjectsAndKeys:custome_Id,@"customer_id", nil];
    [Global urlString:[apiDomain stringByAppendingString:apiUrlWishLists] parameter:dicty indicator:YES currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response) {
        if(result) {
            arrWishList = [[response valueForKey:@"detail"] mutableCopy];
            
            if (arrWishList.count == 0) {
                labelItem.text = @"";
                labelHeader.frame = headerFrame;
                //[Global alertAdd:@"'WishList' is epty" withTitle:@"No Data" navBack:NO];
                [Global addErrorView:self errorMessage:@"Your 'WishList' is empty"];
            }
            
            else
            {
                _tblWishList.hidden = NO;
                [_tblWishList reloadData];
                
                [UIView animateWithDuration:0.3 animations:^{
                    labelHeader.frame = CGRectMake(100, 20, 120, 21.5);
                }
                                 completion:nil
                 ];
                
                [self.view addSubview:labelHeader];
                
                if (arrWishList.count==1) {
                    labelItem.text = [NSString stringWithFormat:@"%lu %@",(unsigned long)arrWishList.count,@"item found"];
                }
                else
                {
                    labelItem.text = [NSString stringWithFormat:@"%lu %@",(unsigned long)arrWishList.count,@"items found"];
                }
                CGRect frameItem = [Global label:labelItem];
                
                frameItem.origin.x = (self.view.frame.size.width/2)-(frameItem.size.width/2);
                frameItem.origin.y = 44;//(self.frame.size.height/2)-(frame1.size.height/2)+4;
                labelItem.lineBreakMode = NSLineBreakByWordWrapping;
                [labelItem sizeToFit];
                labelItem.frame = frameItem;
                [labelItem setTextColor:[UIColor whiteColor]];
                [self.view addSubview:labelItem];
            }
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
