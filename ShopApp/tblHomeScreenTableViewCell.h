//
//  tblHomeScreenTableViewCell.h
//  ShopApp
//
//  Created by Matrid on 30/12/16.
//  Copyright © 2016 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface tblHomeScreenTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgOfferList;
@property (weak, nonatomic) IBOutlet UILabel *lblHeadOfferList;
@property (weak, nonatomic) IBOutlet UILabel *lblDesOfferList;
@property (weak, nonatomic) IBOutlet UIButton *btnShopNowOfferList;
@property (weak, nonatomic) IBOutlet UIImageView *imgCellBackgorund;

@end
