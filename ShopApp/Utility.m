//
//  Utility.m
//  ShopApp
//
//  Created by Md Ibrahim Hassan on 26/04/18.
//  Copyright © 2018 Test. All rights reserved.
//

#import "Utility.h"

@implementation Utility

+(BOOL)isNullOrEmpty:(NSString*)string{
    return string == nil || string==(id)[NSNull null] || [string isEqualToString:@""];
}

@end
