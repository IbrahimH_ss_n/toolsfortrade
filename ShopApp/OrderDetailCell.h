//
//  OrderDetailCell.h
//  ShopApp
//
//  Created by Matrid on 05/05/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface OrderDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgOuterView;
@property (weak, nonatomic) IBOutlet UIImageView *imgProductImage;
@property (weak, nonatomic) IBOutlet UILabel *lblProductDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblQty;
@property (weak, nonatomic) IBOutlet UILabel *lblSoldBy;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;

@end
