//
//  VenderListTableCell.h
//  ShopApp
//
//  Created by Matrid on 21/04/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface VenderListTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnVenderName;

@end
