//
//  HomeViewController.m
//  ShopApp
//
//  Created by Matrid on 28/12/16.
//  Copyright © 2016 Test. All rights reserved.
//

#import "HomeViewController.h"
#import "Constant.h"
#import <objc/runtime.h>
#import <objc/objc-api.h>
#import <objc/NSObjCRuntime.h>

@interface HomeViewController ()
{
    NSTimer *myTimer;
    NSString *strNewArrival;
    NSArray *pageImages,*arrCataogory,*arrOfferList;
}

@end

@implementation HomeViewController

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor  screenBackgroundColor;
   // [_mscrollview setContentSize: CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];
   // _mmScrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 460)];
    
    
    NSString *title = @"Tools4Trade";
    [Global customHeader:self headerTitle:title itemFound:NO hideBackButton:YES hideSearchButton:NO hideNotificationButton:NO];
    
    
    [Global customFooter:self.view];
    
    //_mmScrollview.contentOffset = CGPointMake(320,5000);
     //[self.view bringSubviewToFront:_mmScrollview];

    _mmScrollview.backgroundColor  screenBackgroundColor;

    
    _scrollBanner.contentSize=CGSizeMake(320, 150);

    _scrollBanner.layer.cornerRadius=4;

    _colCatagoryView.layer.cornerRadius=4;
    
    _btnNewArrival.layer.cornerRadius=4;
    _mbtnHOtdeals.layer.cornerRadius=4;
    
    
    [self GetBannrs];
    [self GetOfferList];
    [self GetCatagory];
    [self OfferMethod];
    
//    [self.tblHomeScreen removeFromSuperview];
//    [self.view addSubview:self.mOuterView];
    
    _tblHomeScreen.tableHeaderView = _mOuterView;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [_tblHomeScreen addGestureRecognizer:singleFingerTap];

    [_tblHomeScreen reloadData];
  //  [self mmScrollview];
//[self.mmScrollview addSubview:_colCatagoryView];
  // [self.mmScrollview addSubview:_btnNewArrival];
   // [self.mmScrollview addSubview:_mbtnClearance];

   //[self.mmScrollview addSubview:_viewHeader];
   //_mmScrollview.contentSize=CGSizeMake(320, 5000);
    
    
    
//     self.mOuterView = _mmScrollview;
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [_colCatagoryView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    
    [_colCatagoryView registerNib:[UINib nibWithNibName:@"ColCatagoryViewCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationHeader) name:@"notificationHeader" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(naviGationMenuOpen) name:@"navigationListOpen" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(naviGationMenuClose) name:@"navigationListClose" object:nil];
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(naviGationSearch) name:@"navigationSearch" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(homeFooter) name:@"homeFooter" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userFooter) name:@"userFooter" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cartFooter) name:@"cartFooter" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moreFooter) name:@"moreFooter" object:nil];
    
}
//-(void)naviGationSearch{
//    ProductListViewController *product=[[ProductListViewController alloc]init];
//    product.search=@"SEARCH";
//    [self.navigationController pushViewController:product animated:YES];
//   
//}
-(void)viewDidAppear:(BOOL)animated
{
    //_mmScrollview.contentOffset = CGPointMake(320,1000);

    //_mmScrollview.contentInset=UIEdgeInsetsMake(28, 0, 1000, 0);
    //_mmScrollview.contentSize=self.view.frame.size;
    //_mmScrollview.contentSize=CGSizeMake(320, 5000);


}

-(void)notificationHeader{
}

-(void)naviGationMenuOpen{
    [Global navigationViewOpen];
}

-(void)naviGationMenuClose{
    [Global hideNavigation];
}


-(void)homeFooter{
    for (UIViewController *view in self.navigationController.viewControllers) {
        if ([view isKindOfClass:[HomeViewController class]]) {
            HomeViewController *home = (HomeViewController *)view;
            [self.navigationController popToViewController:home animated:NO];
        }
    }
}
-(void)userFooter{
    UserViewController *user=[[UserViewController alloc]init];
     NSArray *arr=self.navigationController.viewControllers;
   NSMutableArray *navigationStack = [[NSMutableArray alloc] initWithArray:self.navigationController.viewControllers];
    NSMutableArray *nav=[[NSMutableArray alloc]init];
    for (int i=0; i<self.navigationController.viewControllers.count; i++) {
        [nav addObject:[navigationStack objectAtIndex:i]];
        if ([[arr objectAtIndex:i] isKindOfClass:[HomeViewController class]]) {
            break;
        }
    }
     [nav addObject:user];
     self.navigationController.viewControllers = nav;
    [self.navigationController popToViewController:user animated:YES];
}

-(void)cartFooter{
 //   NSString *customer_Id = [[NSUserDefaults standardUserDefaults] objectForKey:@"Uid"];
    NSString *strCustomerId = [[NSUserDefaults standardUserDefaults] objectForKey:@"Uid"];

  
            NSString *mCart_id= [[NSUserDefaults standardUserDefaults] objectForKey:@"cart_id"];
            if (mCart_id==nil) {
                [Global alertAdd:@"Cart is empty!" withTitle:nil navBack:NO];
            }else{
                if (strCustomerId==nil) {
                    WebViewController *web = [[WebViewController alloc] init];
                    web.urlString = @"view_cart_Anonymous";
                    
                    [self.navigationController pushViewController:web animated:YES];
                    
                }else{
                   
                    WebViewController *web = [[WebViewController alloc] init];
                    web.urlString = @"view_cart";
                    
                    [self.navigationController pushViewController:web animated:YES];
               
            }
            
            //[Global alertAdd:@"Please sign in first!" withTitle:nil navBack:NO];
         
            
            
        
    }
        
       /* CartViewController *cart=[[CartViewController alloc]init];
        NSArray *arr=self.navigationController.viewControllers;
        NSMutableArray *navigationStack = [[NSMutableArray alloc] initWithArray:self.navigationController.viewControllers];
        NSMutableArray *nav=[[NSMutableArray alloc]init];
        for (int i=0; i<self.navigationController.viewControllers.count; i++) {
            [nav addObject:[navigationStack objectAtIndex:i]];
            if ([[arr objectAtIndex:i] isKindOfClass:[HomeViewController class]]) {
                break;
            }
        }
        [nav addObject:cart];
        self.navigationController.viewControllers = nav;
        [self.navigationController popToViewController:cart animated:YES];*/
    
    
}
-(void)moreFooter{
    MoreViewController *more=[[MoreViewController alloc]init];
    NSArray *arr=self.navigationController.viewControllers;
    NSMutableArray *navigationStack = [[NSMutableArray alloc] initWithArray:self.navigationController.viewControllers];
    NSMutableArray *nav=[[NSMutableArray alloc]init];
    for (int i=0; i<self.navigationController.viewControllers.count; i++) {
        [nav addObject:[navigationStack objectAtIndex:i]];
        if ([[arr objectAtIndex:i] isKindOfClass:[HomeViewController class]]) {
            break;
        }
    }
    [nav addObject:more];
    self.navigationController.viewControllers = nav;
    [self.navigationController popToViewController:more animated:YES];
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
        return [arrCataogory count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ColCatagoryViewCell *cell = (ColCatagoryViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    NSString *strct=[[arrCataogory objectAtIndex:indexPath.row] valueForKey:@"name"];
    cell.lblCatagoryCell.text=strct;
    cell.lblCatagoryCell.textColor darkGrayColor;
    cell.imgCatagoryCell.layer.cornerRadius = cell.imgCatagoryCell.frame.size.width/2;
    cell.imgCatagoryCell.clipsToBounds=YES;
    
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activity.center =cell.imgCatagoryCell.center;
   // activity.hidesWhenStopped = YES;
    [activity startAnimating];
    [cell addSubview:activity];
    
    [cell.imgCatagoryCell sd_setImageWithURL:[[arrCataogory objectAtIndex:indexPath.row] valueForKey:@"img"] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
        // Get rid of the activity indicator when the image has been loaded
        [activity stopAnimating];
    }];
    //cell.imgCatagoryCell.backgroundColor=[UIColor redColor];
    cell.imgCatagoryCell.contentMode=UIViewContentModeScaleAspectFill;

    return cell;
}

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    return CGSizeMake(85, 85);
//}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([[[arrCataogory objectAtIndex:indexPath.row] valueForKey:@"children_data"] count]==0) {
        
        ProductListViewController *product=[[ProductListViewController alloc]init];
        product.catgory_Id=[[arrCataogory objectAtIndex:indexPath.row] valueForKey:@"id"];
        product.btnSearchFlag=YES;
        [self.navigationController pushViewController:product animated:YES];
    }
    else
    {
        [Singletons singleton].shopByCategoryArrayHome=[[arrCataogory objectAtIndex:indexPath.row] valueForKey:@"children_data"];
        CategoryHomeViewController *catHome=[[CategoryHomeViewController alloc]init];
        catHome.strTitle = [[arrCataogory objectAtIndex:indexPath.row] valueForKey:@"name"];
        [self.navigationController pushViewController:catHome animated:YES];
    
    }
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;//arrOfferList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 170;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}



//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return _mOuterView.frame.size.height;
//}
//
//-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    _mOuterView.backgroundColor screenBackgroundColor;
//
//    return _mOuterView;
//}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    tblHomeScreenTableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"tblHomeScreenTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
   
    activity.center =cell.imgOfferList.center;
    activity.hidesWhenStopped = YES;
    [activity startAnimating];
    [cell addSubview:activity];
    
    [cell.imgOfferList sd_setImageWithURL:[[arrOfferList  objectAtIndex:indexPath.row] valueForKey:@"image"] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
        // Get rid of the activity indicator when the image has been loaded
        [activity stopAnimating];
    }];
    
   // [cell.imgOfferList sd_setImageWithURL:[[arrOfferList  objectAtIndex:indexPath.row] valueForKey:@"url"] placeholderImage:[UIImage sd_animatedGIFNamed:@"loading.gif"]];
   // cell.imgOfferList.contentMode=UIViewContentModeScaleAspectFill;
    
    cell.lblHeadOfferList.text = [[arrOfferList objectAtIndex:indexPath.row] valueForKey:@"name"];
    cell.lblHeadOfferList.font = [UIFont fontWithName:gmFont size:13];
    cell.lblHeadOfferList.textColor darkGrayColor;
    
    cell.lblDesOfferList.text = [[arrOfferList objectAtIndex:indexPath.row] valueForKey:@"description"];
    cell.lblDesOfferList.font = [UIFont fontWithName:gmFont size:9];
    cell.lblDesOfferList.textColor grayColor;
    
    
    cell.btnShopNowOfferList.layer.cornerRadius = 3;
    cell.btnShopNowOfferList.clipsToBounds = YES;
    cell.btnShopNowOfferList.titleLabel.font = [UIFont fontWithName:gmFont size:12];
    [cell.btnShopNowOfferList addTarget:self action:@selector(shopNow:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnShopNowOfferList.tag=indexPath.row;
    
//    cell.layer.cornerRadius=5;
//    cell.clipsToBounds=YES;
    cell.imgCellBackgorund.layer.cornerRadius=5;
    cell.imgCellBackgorund.clipsToBounds=YES;
    return cell;
}

-(void)OfferMethod{
    
    //UITableView *tableView;
    static NSString *CellIdentifier = @"Cell";
    tblHomeScreenTableViewCell *cell =[_tblHomeScreen dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"tblHomeScreenTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activity.center =cell.imgOfferList.center;
    activity.hidesWhenStopped = YES;
    [activity startAnimating];
    [cell addSubview:activity];
    
//    [cell.imgOfferList sd_setImageWithURL:[[arrOfferList  objectAtIndex:indexPath.row] valueForKey:@"image"] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//        
//        // Get rid of the activity indicator when the image has been loaded
//        [activity stopAnimating];
//    }];
    
    // [cell.imgOfferList sd_setImageWithURL:[[arrOfferList  objectAtIndex:indexPath.row] valueForKey:@"url"] placeholderImage:[UIImage sd_animatedGIFNamed:@"loading.gif"]];
    // cell.imgOfferList.contentMode=UIViewContentModeScaleAspectFill;
    
    cell.lblHeadOfferList.text = @"Test";//[[arrOfferList objectAtIndex:indexPath.row] valueForKey:@"name"];
    cell.lblHeadOfferList.font = [UIFont fontWithName:gmFont size:13];
    cell.lblHeadOfferList.textColor darkGrayColor;
    
//    cell.lblDesOfferList.text = [[arrOfferList objectAtIndex:indexPath.row] valueForKey:@"description"];
//    cell.lblDesOfferList.font = [UIFont fontWithName:gmFont size:9];
//    cell.lblDesOfferList.textColor grayColor;
//    
//    
//    cell.btnShopNowOfferList.layer.cornerRadius = 3;
//    cell.btnShopNowOfferList.clipsToBounds = YES;
//    cell.btnShopNowOfferList.titleLabel.font = [UIFont fontWithName:gmFont size:12];
//    [cell.btnShopNowOfferList addTarget:self action:@selector(shopNow:) forControlEvents:UIControlEventTouchUpInside];
//    cell.btnShopNowOfferList.tag=indexPath.row;
    
    //    cell.layer.cornerRadius=5;
    //    cell.clipsToBounds=YES;
    
    cell.imgCellBackgorund.layer.cornerRadius=5;
    cell.imgCellBackgorund.clipsToBounds=YES;
    [_tblHomeScreen indexPathForCell:cell];
    
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    NSString *catgory_ID = [[arrOfferList objectAtIndex:indexPath.row] valueForKey:@"category_id"];
//    ProductListViewController *product=[[ProductListViewController alloc]init];
//    product.catgory_Id=catgory_ID;
//    [self.navigationController pushViewController:product animated:YES];
//}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = self.scrollBanner.frame.size.width;
    int page = floor((self.scrollBanner.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if (page == pageImages.count) {
        page = 0;
        self.pageControl.currentPage = 0;
        return;
    }
    self.pageControl.currentPage = page;
}

-(void)shopNow:(UIButton *)sender
{
    NSString *catgory_ID = [[arrOfferList objectAtIndex:sender.tag] valueForKey:@"id"];
    ProductListViewController *product=[[ProductListViewController alloc]init];
    product.catgory_Id=catgory_ID;
    [Singletons singleton].isFrom = @"";
    product.btnSearchFlag=YES;
    [self.navigationController pushViewController:product animated:YES];
}

-(void)bannerMethod {
    
    // ADDING PAGES TO PAGES SCROLLVIEW.
    for (int i = 0; i < [pageImages count]; i++) {
        //We'll create an imageView object in every 'page' of our scrollView.
        CGRect frame;
        frame.origin.x = self.scrollBanner.frame.size.width * i;
        frame.origin.y = 0;
        frame.size = self.scrollBanner.frame.size;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
        NSString *url = [pageImages objectAtIndex:i];
        
        imageView.image = [UIImage imageNamed:url];
        UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        activity.center = imageView.center;
        activity.hidesWhenStopped = YES;
        [activity startAnimating];
        [imageView addSubview:activity];
        
//        [imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//            
//            // Get rid of the activity indicator when the image has been loaded
//            [activity stopAnimating];
//        }];
        

        //[imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage sd_animatedGIFNamed:@"loading"]];
        
        [self.scrollBanner addSubview:imageView];
        //imageView.contentMode=UIViewContentModeScaleAspectFit;
        
        
    }
    [self.view bringSubviewToFront:_pageControl];
    _pageControl.numberOfPages = pageImages.count;
    _pageControl.backgroundColor = [UIColor clearColor];
    _scrollBanner.contentSize = CGSizeMake(_scrollBanner.frame.size.width * [pageImages count], _scrollBanner.frame.size.height);
    //to hide status bar from top (battery,wifi & time icon)
    
    myTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(changePage) userInfo:nil repeats:YES];
}

-(void)GetCatagory
{
    [Global urlString:apiUrlGetCategoryList parameter:nil indicator:YES currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response) {
        if(result)
        {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             arrCataogory = [response valueForKey:@"details"];
            if (arrCataogory.count == 0) {
                //_tblHomeScreen.hidden = YES;
                //[Global addErrorView];
            }
            else {
                //_tblHomeScreen.hidden = NO;
                [[self.view viewWithTag:1000] removeFromSuperview];
            }

            
            [_colCatagoryView reloadData];
            [Singletons singleton].shopByCategoryArray=arrCataogory;
            _colCatagoryView.backgroundColor = [UIColor whiteColor];
        }
    }];
}
-(void)GetOfferList
{
//    [Global urlString:[apiDomain stringByAppendingString:apiUrlGetOfferList] parameter:nil indicator:NO currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response) {
//        if(result)
//        {
//            [MBProgressHUD hideHUDForView:self.view animated:YES];
//            if ([[response valueForKey:@"status"] isEqualToString:@"SUCCESS"]) {
//               
//                
                UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
                
                activity.center =_btnNewArrival.center;
                 activity.hidesWhenStopped = YES;
                [activity startAnimating];
                [_btnNewArrival addSubview:activity];
    [_mbtnClearance addSubview:activity];
       [_mbtnHOtdeals addSubview:activity];
   [_mmScrollview addSubview:activity];
//     UIImage *btnImage = [UIImage imageNamed:@"nnewarrivals_d"];
//                 [_btnNewArrival setBackgroundImage:btnImage forState:UIControlStateNormal];
                _btnNewArrival.imageView.contentMode = UIViewContentModeScaleAspectFit;
    UIImage *btnImage2 = [UIImage imageNamed:@"hotDeals_d"];
   [_mbtnHOtdeals setBackgroundImage:btnImage2 forState:UIControlStateNormal];
    _mbtnHOtdeals.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    UIImage *btnImage1 = [UIImage imageNamed:@"clearance_d"];
    [_mbtnClearance setBackgroundImage:btnImage1 forState:UIControlStateNormal];
    _mbtnClearance.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
                //[_btnNewArrival sd_setBackgroundImageWithURL:[NSURL URLWithString:@"hotdeals"]]
            //forState:UIControlStateNormal completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                    
//                    // Get rid of the activity indicator when the image has been loaded
//                    [activity stopAnimating];
//                }];
              //  strNewArrival = [[[response valueForKey:@"details"] objectAtIndex:0] valueForKey:@"id"];
               // NSMutableArray *aray = [[NSMutableArray alloc]initWithArray:[response valueForKey:@"details"]];
                //[aray removeObjectAtIndex:0];
               // arrOfferList = aray;
    activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activity.center =_btnNewArrival.center;
    activity.hidesWhenStopped = YES;
    [activity startAnimating];
    //[_imgNewOffer1 addSubview:activity];
    
                //_imgNewOffer1.image = [UIImage imageNamed:@"banr_1"];
    
                [_tblHomeScreen reloadData];
//            }
//            
//        }
//    }];
}



-(void)GetBannrs {
    NSArray *arrayWithObjects= @[ @"banr_1",
     @"banr_2",
     @"banr_3",
     @"banr_4"];
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    pageImages = arrayWithObjects;//[response valueForKey:@"ImageUrl"];
    [self bannerMethod];
    
//    [Global urlString:[apiDomain stringByAppendingString:apiUrlGetBannerList] parameter:nil indicator:NO currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response){
//        if(result){
//            [MBProgressHUD hideHUDForView:self.view animated:YES];
//            pageImages = [response valueForKey:@"ImageUrl"];
//            [self bannerMethod];
//            
//        }
//    }];
}
- (void)changePage {
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.scrollBanner.frame.size.width;
    int page = floor((self.scrollBanner.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if (page == pageImages.count-1) {
        self.scrollBanner.contentOffset = CGPointMake(0, 0);
        return;
    }
    [UIView animateWithDuration:1.0 animations:^{
        self.scrollBanner.contentOffset = CGPointMake(self.scrollBanner.contentOffset.x + pageWidth,self.scrollBanner.contentOffset.y);
    }];
    
}
- (IBAction)btnNewArrivalClick:(id)sender {
    strNewArrival=@"509";
    ProductListViewController *product=[[ProductListViewController alloc]init];
    product.catgory_Id = strNewArrival;
    [Singletons singleton].isFrom = @"";

    [self.navigationController pushViewController:product animated:YES];
    
}
//- (IBAction)btnHOtDdealisClck:(id)sender {
//    strNewArrival=@"632";
//    ProductListViewController *product=[[ProductListViewController alloc]init];
//    product.catgory_Id = strNewArrival;
//    [Singletons singleton].isFrom = @"";
//
//    [self.navigationController pushViewController:product animated:YES];
//
//}
- (IBAction)btnClearanceClick:(id)sender {
    strNewArrival=@"161";
    ProductListViewController *product=[[ProductListViewController alloc]init];
    product.catgory_Id = strNewArrival;
    [Singletons singleton].isFrom = @"";
    
    [self.navigationController pushViewController:product animated:YES];
    
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    strNewArrival=@"161";
//    ProductListViewController *product=[[ProductListViewController alloc]init];
//    product.catgory_Id = strNewArrival;
//    [Singletons singleton].isFrom = @"";
//    [self.navigationController pushViewController:product animated:YES];
//}

- (IBAction)hotdealss:(id)sender {
    strNewArrival=@"632";
    ProductListViewController *product=[[ProductListViewController alloc]init];
    product.catgory_Id = strNewArrival;
    [Singletons singleton].isFrom = @"";
    
    [self.navigationController pushViewController:product animated:YES];
    
    
}
- (IBAction)offer1Click:(id)sender {
    strNewArrival=@"121";
    ProductListViewController *product=[[ProductListViewController alloc]init];
    product.catgory_Id = strNewArrival;
    [Singletons singleton].isFrom = @"";
    
    [self.navigationController pushViewController:product animated:YES];
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    if (location.y > 409.0 && location.y < 500.0) {
        strNewArrival=@"161";
        ProductListViewController *product=[[ProductListViewController alloc]init];
        product.catgory_Id = strNewArrival;
        [Singletons singleton].isFrom = @"";
        
        [self.navigationController pushViewController:product animated:YES];
    }
}


//- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    UITouch *touch = [[event allTouches] anyObject];
//
//    CGPoint location = [touch locationInView:self.view];
//
//    NSLog(@"%@", location);
////    if (location.x >= img.x && location.x <= img.x && location.y >= img.y && location.y <= img.y) {
////        // your code here...
////    }
//}

@end
