//
//  ProductListCollectionViewCell.h
//  ShopApp
//
//  Created by Matrid on 02/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductListCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgProductList;
@property (weak, nonatomic) IBOutlet UILabel *lblProductListDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblProductListPrice;
@property (weak, nonatomic) IBOutlet UIButton *btnProductList;

@end
