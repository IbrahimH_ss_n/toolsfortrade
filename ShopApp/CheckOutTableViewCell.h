//
//  CheckOutTableViewCell.h
//  ShopApp
//
//  Created by Matrid on 20/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckOutTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblReviewYourOrder;
@property (weak, nonatomic) IBOutlet UILabel *lblOffOnProduct;
@property (weak, nonatomic) IBOutlet UILabel *lblNewPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblOldPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIImageView *imgProductImage;
@property (weak, nonatomic) IBOutlet UILabel *lblProductQuatity;
@property (weak, nonatomic) IBOutlet UILabel *lblSize;

@end
