//
//  VendorViewController.h
//  ShopApp
//
//  Created by Matrid on 18/04/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface VendorViewController : UIViewController

{
    NSArray *arrVendor;
}
@property (weak, nonatomic) IBOutlet UITableView *tblVendorList;
@property (weak, nonatomic) IBOutlet UILabel *lblMenuTitle;

@end
