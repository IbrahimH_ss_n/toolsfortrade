//
//  DownloadableProductsVC.h
//  ShopApp
//
//  Created by Matrid on 05/06/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface DownloadableProductsVC : UIViewController

{
    NSArray *arrDownLoadableProducts;
}
@property (weak, nonatomic) IBOutlet UILabel *lblDownloadable;
@property (weak, nonatomic) IBOutlet UITableView *tblDownloadProducts;
@property (strong, nonatomic) IBOutlet UIView *viewHeader;

@end
