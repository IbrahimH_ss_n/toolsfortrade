//
//  relatedPrdctImageCell.h
//  ShopApp
//
//  Created by Matrid on 19/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface relatedPrdctImageCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgProduct;
@property (strong, nonatomic) IBOutlet UILabel *lblProduct;

@end
