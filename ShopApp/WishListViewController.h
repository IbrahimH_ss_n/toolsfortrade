//
//  WishListViewController.h
//  ShopApp
//
//  Created by Matrid on 01/03/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface WishListViewController : UIViewController

{
    NSInteger deleteWishList;
    NSString *selectedWishList;
    NSMutableArray *arrWishList;
    UILabel *labelItem, *labelHeader;
    CGRect headerFrame;
}
@property (weak, nonatomic) IBOutlet UITableView *tblWishList;

@end
