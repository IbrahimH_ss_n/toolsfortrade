//
//  ssMediaLogin.h
//  mIracleNoodle
//
//  Created by Matrid on 11/02/16.
//  Copyright © 2016 Matrid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TwitterKit/TwitterKit.h>
#import <Fabric/Fabric.h>
#import <Google/SignIn.h>

@interface ssMediaLogin : NSObject <GIDSignInUIDelegate>

@property UIViewController *view;

- (void)fbLogin;
- (void)twitterLogin;
- (void)googleLogin;

@end
