//
//  tblFilterChildTableViewCell.h
//  ShopApp
//
//  Created by Matrid on 19/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface tblFilterChildTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblFilterChildCount;
@property (weak, nonatomic) IBOutlet UILabel *lblFilterChild;
@property (weak, nonatomic) IBOutlet UIButton *btnFilterChildCheck;

@end
