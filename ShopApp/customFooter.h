//
//  customFooter.h
//  maegntoShopApp
//
//  Created by Matrid on 23/05/16.
//  Copyright © 2016 Matrid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customFooter : UIView
{
    UIImageView *imgHome;
    UIImageView *imgUser;
    UIImageView *imgMyCart;
    UIImageView *imgMore;
    UIButton *btnHome;
}

@end
