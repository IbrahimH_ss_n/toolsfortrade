//
//  VendorViewController.m
//  ShopApp
//
//  Created by Matrid on 18/04/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import "VendorViewController.h"

@interface VendorViewController ()

@end

@implementation VendorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
   
    
    [Global customHeader:self headerTitle:@"Tools4Trade" itemFound:NO hideBackButton:NO hideSearchButton:YES hideNotificationButton:YES];
    [Global customFooter:self.view];
    
    [self getVendorList];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    _lblMenuTitle.text= @"Shop By Vendor";
    _lblMenuTitle.font = [UIFont fontWithName:gmFont size:16];
    _lblMenuTitle.textColor darkGrayColor;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrVendor.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"newFriendCell";
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    VenderListTableCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"VenderListTableCell" owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    if ([[[arrVendor objectAtIndex:indexPath.row] valueForKey:@"shop_title"]  isKindOfClass:[NSNull class]]) {
        NSString *str = [[arrVendor objectAtIndex:indexPath.row] valueForKey:@"firstname"];
        [cell.btnVenderName setTitle:str forState:UIControlStateNormal];
    }
    else {
        NSString *str = [[arrVendor objectAtIndex:indexPath.row] valueForKey:@"shop_title"];
        [cell.btnVenderName setTitle:str forState:UIControlStateNormal];
    }
    cell.btnVenderName.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    cell.btnVenderName.titleLabel.font = [UIFont fontWithName:gmFont size:13];
    cell.btnVenderName.tintColor darkGrayColor;
    [cell.btnVenderName addTarget:self action:@selector(btnVenderNameClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnVenderName.tag = indexPath.row;
    return cell;
}


- (void)btnVenderNameClick:(UIButton *)sender
{
    NSInteger tag = sender.tag;
    [Singletons singleton].isFrom = @"VendorProduct";
    ProductListViewController *product=[[ProductListViewController alloc]init];
    product.seller_Id = [[arrVendor objectAtIndex:tag] valueForKey:@"seller_id"];
    [self.navigationController pushViewController:product animated:YES];
}

- (void) getVendorList {
    [Global urlString:[apiDomain stringByAppendingString:apiUrlVendorList] parameter:nil indicator:YES currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response) {
        if(result) { //DATA_MISSING
            if ([[response valueForKey:@"status"] isEqualToString:@"success"]) {
                response = [Global stripNullFromResponse:[response mutableCopy]];
                arrVendor = [response valueForKey:@"details"];
                [_tblVendorList reloadData];
            }
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
