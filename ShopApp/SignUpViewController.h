//
//  SignUpViewController.h
//  ShopApp
//
//  Created by Matrid on 06/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;

@property (strong, nonatomic) IBOutlet UITextField *txtName;
@property (strong, nonatomic) IBOutlet UITextField *txtLName;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UITextField *txtRPassword;

- (IBAction)btnSignUpClick:(id)sender;

- (IBAction)btnBackClick:(id)sender;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end
