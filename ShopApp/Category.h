//
//  Category.h
//  ShopApp
//
//  Created by Md Ibrahim Hassan on 26/04/18.
//  Copyright © 2018 Test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SubCategory.h"
@interface Category : NSObject
@property (nonatomic, strong) NSString *code;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSMutableArray *options;
- (id) initWithDictionary:(NSDictionary*)dictionary;
- (NSInteger)getSubCategoryCount;
- (void)handleSelectionFor:(NSString *)label;
- (BOOL)isOptionSelectedFor:(NSString *)label;
- (void)handleSelectionAt:(NSInteger)index;
- (BOOL)isOptionSelectedAt:(NSInteger)index;
- (void)clearAllSelection;
- (NSMutableDictionary *) dictionaryValues;
- (NSInteger)getSubCategoryCountFor: (NSString *)filterString;
- (SubCategory *)filteredOptionsFor: (NSString *)filterString And : (int)index;
@end
