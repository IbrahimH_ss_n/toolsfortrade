//
//  HomeViewController.h
//  ShopApp
//
//  Created by Matrid on 28/12/16.
//  Copyright © 2016 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tblHomeScreen;
@property (strong, nonatomic) IBOutlet UIView *viewHeader;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollBanner;
 @property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UICollectionView *colCatagoryView;
@property (weak, nonatomic) IBOutlet UIImageView *imgNewArrival;
@property (weak, nonatomic) IBOutlet UIScrollView *Main_Scroll;
@property (weak, nonatomic) IBOutlet UIButton *btnNewArrival;
 @property (weak, nonatomic) IBOutlet UIImageView *imgNewOffer1;
@property (weak, nonatomic) IBOutlet UIButton *mbtnHOtdeals;
@property (weak, nonatomic) IBOutlet UIScrollView *mscrollview;
 @property (weak, nonatomic) IBOutlet UIScrollView *mmScrollview;
@property (strong, nonatomic) IBOutlet UIView *mOuterView;
@property (strong, nonatomic) IBOutlet UIView *mInnerView;
@property (weak, nonatomic) IBOutlet UIButton *mbtnClearance;
@property (strong, nonatomic) IBOutlet UIImageView *offer1;
@end
