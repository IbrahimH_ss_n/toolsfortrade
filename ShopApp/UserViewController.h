//
//  UserViewController.h
//  ShopApp
//
//  Created by Matrid on 30/12/16.
//  Copyright © 2016 Test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface UserViewController : UIViewController
{
    NSArray *arrUserOrder, *arrImageOrder, *arrUserMyInfo, *arrImageMyInfo;
    NSMutableArray *arrShippingAddress;
}
@property (strong, nonatomic) IBOutlet UIView *viewUserHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblWelcome;

@property (strong, nonatomic) IBOutlet UITableView *tblUserProfile;
@property (strong, nonatomic) IBOutlet UIButton *btnLogOut;
@property (weak, nonatomic) IBOutlet UIView *viewAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UIView *viewAddressMain;

@end
