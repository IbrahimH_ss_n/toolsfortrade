//
//  Utility.h
//  ShopApp
//
//  Created by Md Ibrahim Hassan on 26/04/18.
//  Copyright © 2018 Test. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject
+(BOOL)isNullOrEmpty:(NSString*)string;

@end
