//
//  productDetailDescriptionCell.h
//  ShopApp
//
//  Created by Matrid on 19/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface productDetailDescriptionCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIButton *btnDescription;
@property (strong, nonatomic) IBOutlet UIButton *btnSpecification;
@property (strong, nonatomic) IBOutlet UILabel *lblContent;
@property (strong, nonatomic) IBOutlet UIView *viewContainer;
@end
