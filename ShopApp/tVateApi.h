//
//  CourseApi.h
//  Golfie
//
//  Created by Pankaj Khatri on 06/04/15.
//  Copyright (c) 2015 cogniter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Global.h"
#import "UIImageView+WebCache.h"
@interface tVateApi : NSObject{
    //    UIImage *ReturnImage;
    NSMutableData *responseData ;
    NSDictionary* response;
}
+ (NSString *)domain;
+ (NSString *)baseUrl;

+(void)createImageCache:(NSDictionary *)dicDetails :(NSString *)ImagePath;
+(UIImage *)checkForImagesFromCaching:(NSURL *)imageUrl andFolder:(NSString *)folderName;
+(UIImage*)getImagesFromCache :(NSString*)imageUrl;

@end
