//
//  OrderDetailViewController.m
//  ShopApp
//
//  Created by Matrid on 02/05/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import "OrderDetailViewController.h"

@interface OrderDetailViewController ()

@end

@implementation OrderDetailViewController
@synthesize Order_Id;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [Global customHeader:self headerTitle:@"S A V I S H O P S" itemFound:NO hideBackButton:NO hideSearchButton:NO hideNotificationButton:NO];
    _lblOrderDetail.backgroundColor screenBackgroundColor;
    _lblOrderDetail.font = [UIFont fontWithName:gmFont size:14];
    _lblOrderDetail.text = @"  Order Detail";
    [Global customFooter:self.view];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self getOrderDetail];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrOrderDetails.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 280;
}


-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return _viewForFooter;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"newFriendCell";
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    OrderDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"OrderDetailCell" owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    NSString *strShoptitle;
    if ([[[arrOrderDetails objectAtIndex:indexPath.row] valueForKey:@"shop_title"]  isKindOfClass:[NSNull class]]) {
        strShoptitle = @"Savishop";
    }
    else {
        strShoptitle = [[arrOrderDetails objectAtIndex:indexPath.row] valueForKey:@"shop_title"];
    }
    
    cell.lblSoldBy.text = strShoptitle;
    cell.lblProductDescription.text = [[arrOrderDetails objectAtIndex:indexPath.row] valueForKey:@"name"];
    
    CGFloat price = [[[arrOrderDetails objectAtIndex:indexPath.row] valueForKey:@"price"] floatValue];
    cell.lblPrice.text = [NSString stringWithFormat:@"Price : %.02f",price];
    
    CGFloat qty = [[[arrOrderDetails objectAtIndex:indexPath.row] valueForKey:@"qty_ordered"] floatValue];
    cell.lblQty.text = [NSString stringWithFormat:@"QTY : %.f",qty];
    
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activity.center = cell.imgProductImage.center;
    activity.hidesWhenStopped = YES;
    [activity startAnimating];
    [cell addSubview:activity];
    
    NSURL *url = [[arrOrderDetails objectAtIndex:indexPath.row] valueForKey:@"image"];
    
    [cell.imgProductImage sd_setImageWithURL:url placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
        // Get rid of the activity indicator when the image has been loaded
        [activity stopAnimating];
    }];
    
    cell.imgOuterView.layer.cornerRadius = 5;
    cell.imgOuterView.layer.borderWidth = 2;
    cell.imgOuterView.layer.borderColor = [[UIColor colorWithRed:130/255.0 green:199/255.0 blue:132/255.0 alpha:1.0] CGColor];
    cell.imgOuterView.clipsToBounds = YES;

    
    return cell;
}


- (void)getOrderDetail {
    
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:Order_Id,@"order_id", nil];
    
    [Global urlString:[apiDomain stringByAppendingString:apiUrlFullOderDetails] parameter:dic indicator:YES currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response) {
        if(result) { //DATA_MISSING
            if ([[response valueForKey:@"status"] isEqualToString:@"SUCCESS"]) {
                arrOrderDetails  = [[response valueForKey:@"details"] valueForKey:@"item_details"];
                arrBillingAddress  = [[response valueForKey:@"details"] valueForKey:@"billing_address"];
                arrShipingAddress  = [[response valueForKey:@"details"] valueForKey:@"shipping_address"];
                _tblOrderDetail.hidden = NO;
                
                _lblBillingAddresName.text = [NSString stringWithFormat:@"%@ %@",[[arrBillingAddress objectAtIndex:0] valueForKey:@"firstname"],[[arrBillingAddress objectAtIndex:0] valueForKey:@"lastname"]];
                
                NSMutableArray *arrBilling = [[NSMutableArray alloc]init];
    
                [arrBilling addObject:[[arrBillingAddress objectAtIndex:0] valueForKey:@"street"]];
                [arrBilling addObject:[[arrBillingAddress objectAtIndex:0] valueForKey:@"city"]];
                [arrBilling addObject:[[arrBillingAddress objectAtIndex:0] valueForKey:@"region"]];
                [arrBilling addObject:[[arrBillingAddress objectAtIndex:0] valueForKey:@"country_id"]];
                [arrBilling addObject:[[arrBillingAddress objectAtIndex:0] valueForKey:@"postcode"]];
                
                NSString *strBillAddress = [arrBilling componentsJoinedByString:@"\r"];
                
                _lblBillingAddress.text = strBillAddress;
                
                _lblShippingAddressName.text = [NSString stringWithFormat:@"%@ %@",[[arrShipingAddress objectAtIndex:0] valueForKey:@"firstname"],[[arrShipingAddress objectAtIndex:0] valueForKey:@"lastname"]];
                
                NSMutableArray *arrShipping = [[NSMutableArray alloc]init];
                
                [arrShipping addObject:[[arrShipingAddress objectAtIndex:0] valueForKey:@"street"]];
                [arrShipping addObject:[[arrShipingAddress objectAtIndex:0] valueForKey:@"city"]];
                [arrShipping addObject:[[arrShipingAddress objectAtIndex:0] valueForKey:@"region"]];
                [arrShipping addObject:[[arrShipingAddress objectAtIndex:0] valueForKey:@"country_id"]];
                [arrShipping addObject:[[arrShipingAddress objectAtIndex:0] valueForKey:@"postcode"]];
                
                NSString *strShipAddress = [arrShipping componentsJoinedByString:@"\r"];
                
                _lblShippingAddress.text = strShipAddress;
                
                _viewBillingAddressOuter.layer.cornerRadius = 5;
                _viewBillingAddressOuter.layer.borderWidth = 2;
                _viewBillingAddressOuter.layer.borderColor = [[UIColor colorWithRed:130/255.0 green:199/255.0 blue:132/255.0 alpha:1.0] CGColor];
                _viewBillingAddressOuter.clipsToBounds = YES;
                
                _viewShippingAddressOuter.layer.cornerRadius = 5;
                _viewShippingAddressOuter.layer.borderWidth = 2;
                _viewShippingAddressOuter.layer.borderColor = [[UIColor colorWithRed:130/255.0 green:199/255.0 blue:132/255.0 alpha:1.0] CGColor];
                _viewShippingAddressOuter.clipsToBounds = YES;
                
                [_tblOrderDetail reloadData];
                
                
            }
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
