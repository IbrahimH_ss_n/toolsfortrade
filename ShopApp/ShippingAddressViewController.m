//
//  ShippingAddressViewController.m
//  ShopApp
//
//  Created by Matrid on 24/03/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import "ShippingAddressViewController.h"

@interface ShippingAddressViewController ()
{
    pickerViewController *picker;;
}
@end

@implementation ShippingAddressViewController
@synthesize arrShippingAddress, fromView;

- (void)viewDidLoad {
    [super viewDidLoad];
    countryClick = YES;
    stateClick = YES;
    
     [Global customHeader:self headerTitle:@"Tools4Trade" itemFound:NO hideBackButton:NO hideSearchButton:NO hideNotificationButton:NO];
    _scrollView.contentSize=CGSizeMake(320, 520);
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self doSomethingWithTheJson];
}

- (void)viewWillAppear:(BOOL)animated {
    UIView *viewCategoryHeader=[[UIView alloc]initWithFrame:CGRectMake(0, 60, 320, 41)];
    viewCategoryHeader.backgroundColor screenBackgroundColor;
    UILabel *lblSectionTitle = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, 320, 41)];
    lblSectionTitle.font = [UIFont fontWithName:gmFont size:16];
    
    if ([fromView isEqualToString:@"Shipping"]) {
        lblSectionTitle.text = @"SHIPPING ADDRESS";
    }
    
    if ([fromView isEqualToString:@"Billing"]) {
        lblSectionTitle.text = @"BILLING ADDRESS";
    }
    
    [viewCategoryHeader addSubview:lblSectionTitle];
    lblSectionTitle.font = [UIFont fontWithName:gmFont size:14];
    lblSectionTitle.textColor grayColor;
    [self.view addSubview:viewCategoryHeader];
    
    _btnSave.titleLabel.font = [UIFont fontWithName:gmFont size:17];
    _btnSave.tintColor = [UIColor whiteColor];
    _btnSave.backgroundColor greenColor;
     _btnSave.layer.cornerRadius = 2;
    
    [_tblDropDown.layer setShadowColor:[[UIColor whiteColor] CGColor]];
    [_tblDropDown.layer setShadowOffset:CGSizeMake(2, 2)];
    [_tblDropDown.layer setShadowRadius:15.0];
    [_tblDropDown.layer setShadowOpacity:5];
    _tblDropDown.clipsToBounds = NO;
    
    [_tblStateDropDown.layer setShadowColor:[[UIColor whiteColor] CGColor]];
    [_tblStateDropDown.layer setShadowOffset:CGSizeMake(2, 2)];
    [_tblStateDropDown.layer setShadowRadius:15.0];
    [_tblStateDropDown.layer setShadowOpacity:5];
    _tblStateDropDown.clipsToBounds = NO;
    
    [self loadDataToView];
    [self getStateFromCountry];
}


- (void)loadDataToView {
    if (arrShippingAddress.count == 0) {
        [_btnSave setTitle:@"Save" forState:UIControlStateNormal];
        return;
    }
    else {
        [_btnSave setTitle:@"Update" forState:UIControlStateNormal];
        coutry_id = [arrShippingAddress valueForKey:@"country_id"];
        
        for (int i = 0; i < countryList.count; i++) {
            if ([coutry_id isEqualToString:[[countryList objectAtIndex:i] valueForKey:@"country_id"]]) {
                _txtCountryName.text = [[countryList objectAtIndex:i] valueForKey:@"country_name"];
                break;
            }
        }
        //regionName = [[arrShippingAddress valueForKey:@"region"] valueForKey:@"region"];
        region_id = [[arrShippingAddress valueForKey:@"region"] valueForKey:@"region_id"];
        regionCode = [[arrShippingAddress valueForKey:@"region"] valueForKey:@"region_code"];
        
        _txtFirstName.text = [arrShippingAddress valueForKey:@"firstname"];
        _txtLastName.text = [arrShippingAddress valueForKey:@"lastname"];
        _txtStreet.text = [[arrShippingAddress  valueForKey:@"street"] objectAtIndex:0];
        _txtCity.text = [arrShippingAddress valueForKey:@"city"];
        _txtState.text = [[arrShippingAddress valueForKey:@"region"] valueForKey:@"region"];
        _txtZipCode.text = [arrShippingAddress valueForKey:@"postcode"];
        _txtPhone.text = [arrShippingAddress valueForKey:@"telephone"];
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == _tblStateDropDown) {
        return stateList.count;
    }
    return countryList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"newFriendCell";
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    ShippingAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"ShippingAddressCell" owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    if (tableView == _tblDropDown) {
        NSString *str = [[countryList objectAtIndex:indexPath.row] valueForKey:@"country_name"];
        cell.lblCountryName.text = str;
        
        return cell;
    }
    
    NSString *str = [[stateList objectAtIndex:indexPath.row] valueForKey:@"default_name"];
    cell.lblCountryName.text = str;
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tblDropDown) {
        _txtCountryName.text = [[countryList objectAtIndex:indexPath.row] valueForKey:@"country_name"];
        coutry_id = [[countryList objectAtIndex:indexPath.row] valueForKey:@"country_id"];
        _tblDropDown.hidden = YES;
        
        [self getStateFromCountry];
        //_txtState.text = @"Please Select State";
        _txtState.placeholder = @"Please Select State";
    }
    else {
        _txtState.text = [[stateList objectAtIndex:indexPath.row] valueForKey:@"default_name"];
        region_id = [[stateList objectAtIndex:indexPath.row] valueForKey:@"region_id"];
        regionCode = [[stateList objectAtIndex:indexPath.row] valueForKey:@"code"];
        coutry_id = [[stateList objectAtIndex:indexPath.row] valueForKey:@"country_id"];
        _tblStateDropDown.hidden = YES;
    }
}

- (void)doSomethingWithTheJson
{
    NSDictionary *dict = [self JSONFromFile];
    countryList = [dict objectForKey:@"details"];
}

- (NSDictionary *)JSONFromFile
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"country_list" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    return [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
}

-(void)getStateFromCountry {
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:coutry_id,@"country_id", nil];
    
    [Global urlString:[apiDomain stringByAppendingString:apiUrlShippingCoutryRegion] parameter:dic indicator:YES currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response) {
        if(result) { //DATA_MISSING
            if ([[response valueForKey:@"status"] isEqualToString:@"SUCCESS"]) {
                stateList = [response valueForKey:@"details"];
                [_tblStateDropDown reloadData];
            }
        }
    }];
}



- (IBAction)btnCoutryDropDown:(id)sender {
//    if (countryClick) {
//        _tblStateDropDown.hidden = YES;
//        _tblDropDown.hidden = NO;
//        [_tblDropDown reloadData];
//        countryClick = NO;
//    }
//    else {
//        _tblDropDown.hidden = YES;
//        countryClick = YES;
//
//    }
    [[NSUserDefaults standardUserDefaults] setObject:@"Country" forKey:@"PickerFor"];
    [[NSUserDefaults standardUserDefaults] setObject:countryList forKey:@"CountryList"];
    picker = [[pickerViewController alloc] init];
    picker.delegate = self;
    
    [self.view addSubview:picker];
    [Global pickerShow:self.view];
}

- (IBAction)btnStateDropDown:(id)sender {
//    if (stateClick) {
//        _tblDropDown.hidden = YES;
//        _tblStateDropDown.hidden = NO;
//        [_tblStateDropDown reloadData];
//        stateClick = NO;
//    }
//    else {
//         _tblStateDropDown.hidden = YES;
//        stateClick = YES;
//    }
    
    [[NSUserDefaults standardUserDefaults] setObject:@"State" forKey:@"PickerFor"];
    [[NSUserDefaults standardUserDefaults] setObject:stateList forKey:@"StateList"];
    picker = [[pickerViewController alloc] init];
    picker.delegate = self;
    [self.view addSubview:picker];
    [Global pickerShow:self.view];

}

-(void)pickerValue:(NSString *)value {
    NSString *str = [[NSUserDefaults standardUserDefaults] valueForKey:@"PickerFor"];
    
    if ([str isEqualToString:@"Country"])  {
        _txtCountryName.text = [value valueForKey:@"country_name"];
        coutry_id = [value valueForKey:@"country_id"];
        _tblDropDown.hidden = YES;
       _txtState.placeholder = @"Please Select State";
        [self getStateFromCountry];
    }
    
    if ([str isEqualToString:@"State"]) {
        _txtState.text = [value valueForKey:@"default_name"];
        coutry_id = [value valueForKey:@"country_id"];
        region_id = [value valueForKey:@"region_id"];
        regionCode = [value valueForKey:@"code"];
        coutry_id = [value valueForKey:@"country_id"];
        
    }
}

- (IBAction)btnSave:(id)sender {
    
    if ([_txtCountryName.text isEqualToString:@""]) {
        [Global alertAdd:@"Please Select Coutry" withTitle:@"Select" navBack:NO];
        return;
    }
    if ([_txtCountryName.text isEqualToString:@""]) {
        [Global alertAdd:@"Please Select 'Coutry'" withTitle:@"Select" navBack:NO];
        return;
    }
    if ([_txtFirstName.text isEqualToString:@""]) {
        [Global alertAdd:@"Please Enter 'First Name'" withTitle:@"Select" navBack:NO];
        return;
    }
    if ([_txtLastName.text isEqualToString:@""]) {
        [Global alertAdd:@"Please Enter 'Last Name'" withTitle:@"Select" navBack:NO];
        return;
    }
    if ([_txtStreet.text isEqualToString:@""]) {
        [Global alertAdd:@"Please Enter 'Street Number'" withTitle:@"Select" navBack:NO];
        return;
    }
    if ([_txtCity.text isEqualToString:@""]) {
        [Global alertAdd:@"Please Enter 'City'" withTitle:@"Select" navBack:NO];
        return;
    }
    if ([_txtState.text isEqualToString:@""]) {
        [Global alertAdd:@"Please Select 'State'" withTitle:@"Select" navBack:NO];
        return;
    }
    if ([_txtZipCode.text isEqualToString:@""]) {
        [Global alertAdd:@"Please Enter 'Zip Code'" withTitle:@"Select" navBack:NO];
        return;
    }
    if ([_txtPhone.text isEqualToString:@""]) {
        [Global alertAdd:@"Please Enter 'Phone Number'" withTitle:@"Select" navBack:NO];
        return;
    }
    
    
    NSString *URL;
    
    if ([fromView isEqualToString:@"Shipping"]) {
        URL = apiUrlShippingAddressUpdate;
    }
    
    if ([fromView isEqualToString:@"Billing"]) {
        URL = apiUrlBillingAddressUpdate;
    }
    NSString *customerId = [[NSUserDefaults standardUserDefaults] valueForKey:@"Uid"];
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
                         customerId ,@"customer_Id",
                         _txtCity.text,@"city",
                         _txtState.text,@"region",
                         _txtZipCode.text,@"post_code",
                         coutry_id,@"country_Id",
                         _txtPhone.text,@"phone",
                         _txtFirstName.text,@"first_name",
                         _txtLastName.text,@"last_name",
                         region_id,@"region_id",
                         regionCode,@"region_code",
                         _txtStreet.text,@"street",
                         nil];
    
    [Global urlString:[apiDomain stringByAppendingString:URL] parameter:dic indicator:YES currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response) {
        if(result) { //DATA_MISSING
            if ([[response valueForKey:@"status"] isEqualToString:@"SUCCESS"]) {
                //stateList = [response valueForKey:@"id"];
                if (arrShippingAddress.count == 0) {
                    [Global alertAdd:nil withTitle:@"Address Added Successfully." navBack:YES];
                }
                    [Global alertAdd:nil withTitle:@"Address Updated Successfully." navBack:YES];
            }
        }
    }];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField == _txtPhone) {
        NSString *resultText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        return resultText.length <= 10;
    }
    if (textField == _txtZipCode) {
        NSString *resultText = [textField.text stringByReplacingCharactersInRange:range withString:string];
        return resultText.length <= 6;
    }
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
