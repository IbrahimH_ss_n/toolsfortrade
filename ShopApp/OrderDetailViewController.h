//
//  OrderDetailViewController.h
//  ShopApp
//
//  Created by Matrid on 02/05/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
@interface OrderDetailViewController : UIViewController
{
    NSArray *arrOrderDetails, *arrBillingAddress, *arrShipingAddress;
}
@property (strong, nonatomic) NSString *Order_Id;

@property (weak, nonatomic) IBOutlet UILabel *lblOrderDetail;
@property (weak, nonatomic) IBOutlet UITableView *tblOrderDetail;
@property (strong, nonatomic) IBOutlet UIView *viewForFooter;
@property (weak, nonatomic) IBOutlet UIView *viewShippingAddressOuter;
@property (weak, nonatomic) IBOutlet UIView *viewBillingAddressOuter;
@property (weak, nonatomic) IBOutlet UILabel *lblBillingAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblShippingAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblShippingAddressName;
@property (weak, nonatomic) IBOutlet UILabel *lblBillingAddresName;

@end
