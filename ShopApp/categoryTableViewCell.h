//
//  categoryTableViewCell.h
//  ShopApp
//
//  Created by Matrid on 07/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface categoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblCategoryTableCell;
- (IBAction)btnCategoryCellClick:(id)sender;

@end
