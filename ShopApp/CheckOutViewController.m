//
//  CheckOutViewController.m
//  ShopApp
//
//  Created by Matrid on 20/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import "CheckOutViewController.h"

@interface CheckOutViewController ()

@end

@implementation CheckOutViewController
@synthesize arrOrder;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    FooterClick = NO;
    
    self.view.backgroundColor screenBackgroundColor;
    
    [Global customHeader:self headerTitle:@"Tools4Trade" itemFound:NO hideBackButton:NO hideSearchButton:YES hideNotificationButton:YES];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    //    activityAddress = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    //
    //    activityAddress.center = _lblAddressViewForHeader.center;
    //    activityAddress.hidesWhenStopped = YES;
    //    [activityAddress startAnimating];
    //    [_lblAddressViewForHeader addSubview:activityAddress];
    //btnPayY = btnPayY.origin.y;
    btnPayMentFrameY = _btnMakePayment.frame;
    
}

- (void)viewWillAppear:(BOOL)animated{
    
    [self shippingAddress];
    
    _lblCirleLogin.backgroundColor greenColor;
    _lblCirleLogin.layer.cornerRadius = _lblCirleLogin.frame.size.width/2;
    _lblCirleLogin.clipsToBounds = YES;
    
    _lblCirclePayment.backgroundColor greenColor;
    _lblCirclePayment.layer.cornerRadius = _lblCirclePayment.frame.size.width/2;
    _lblCirclePayment.clipsToBounds = YES;
    
    _lblCircleComplete.backgroundColor mediumGrayColor;
    _lblCircleComplete.layer.cornerRadius = _lblCircleComplete.frame.size.width/2;
    _lblCircleComplete.clipsToBounds = YES;
    
    _lblLineLoginToPayment.backgroundColor greenColor;
    _lblLinePaymentToComplete.backgroundColor mediumGrayColor;
    
    _lblLogin.font = [UIFont fontWithName:gmFont size:10];
    _lblLogin.textColor grayColor;
    _lblPayment.font = [UIFont fontWithName:gmFont size:10];
    _lblPayment.textColor grayColor;
    _lblComplete.font = [UIFont fontWithName:gmFont size:10];
    _lblComplete.textColor minGrayColor;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrOrder.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 170;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (FooterClick) {
        return FooterFrame;
    }
    return 302;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 240;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    _viewForHeaderCheckOut.backgroundColor screenBackgroundColor;
    [_btnChangeViewForHeader setTitleColor:[UIColor colorWithRed:130/255.0 green:199/255.0 blue:132/255.0 alpha:1.0] forState:UIControlStateNormal];
    _lblDeliverAddressViewForHeader.textColor grayColor;
    _lblAddressViewForHeader.textColor minGrayColor;
    _lblReviewYourOrder.textColor grayColor;
    
    return _viewForHeaderCheckOut;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    _viewForFooter.backgroundColor screenBackgroundColor;
    _lblChoosePayment.textColor grayColor;
    _btnMakePayment.backgroundColor greenColor;
    NSString *strTotalCost = [[arrOrder valueForKey:@"totalCost"] objectAtIndex:0];
    NSString *strCurrency= [[arrOrder valueForKey:@"currency"] objectAtIndex:0];
    
    [_btnMakePayment setTitle:[NSString stringWithFormat:@"Pay %@ %@",strCurrency,strTotalCost] forState:UIControlStateNormal];
    
    _lblDebitCard.textColor grayColor;
    _btnDebitCard.layer.borderWidth = 1;
    _btnDebitCard.layer.borderColor = [[UIColor colorWithRed:229.0/255.0 green:229.0/255.0 blue:229.0/255.0 alpha:1.0] CGColor];
    _btnDebitCard.clipsToBounds = YES;
    
    _lblCOD.textColor grayColor;
    _btnCOD.layer.borderWidth = 1;
    _btnCOD.layer.borderColor = [[UIColor colorWithRed:229.0/255.0 green:229.0/255.0 blue:229.0/255.0 alpha:1.0] CGColor];
    _btnCOD.clipsToBounds = YES;
    
    return _viewForFooter;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    CheckOutTableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CheckOutTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.lblDescription.text = [[arrOrder objectAtIndex:indexPath.row] valueForKey:@"name"];
    cell.lblProductQuatity.text = [[[arrOrder objectAtIndex:indexPath.row] valueForKey:@"qty"] stringValue];
    
    NSArray *arrOption = [[[[arrOrder objectAtIndex:indexPath.row] valueForKey:@"product_option"] valueForKey:@"extension_attributes"] valueForKey:@"configurable_item_options"];
    NSString *strSize = @"";
    for (int i = 0; i<arrOption.count; i++) {
        if ([[[arrOption objectAtIndex:i] valueForKey:@"attribute_code"] isEqualToString:@"size"]) {
            strSize = [[arrOption objectAtIndex:i]valueForKey:@"option_label"];
        }
    }
    
    if (![strSize isEqualToString:@""]) {
        cell.lblSize.text = [NSString stringWithFormat:@"Size: %@",strSize];
    }
    else
    {
        cell.lblSize.text = @"";
    }
    
    NSString *strNPrice = [[arrOrder objectAtIndex:indexPath.row] valueForKey:@"sale_price"];
    NSString *strOldPrice = [[arrOrder objectAtIndex:indexPath.row] valueForKey:@"price"];
    
    CGFloat percentageOff = ([strNPrice floatValue]/[strOldPrice floatValue])*100;
    
    percentageOff = 100-percentageOff;
    
    cell.lblOffOnProduct.text = [NSString stringWithFormat:@"(%@%@ OFF)",[[NSNumber numberWithFloat:percentageOff] stringValue],@"%"];
    
    NSString *strCurr= [[arrOrder objectAtIndex:indexPath.row] valueForKey:@"currency"];
    
    NSString *NewPrice = [Global PriceRoundOffToString:strNPrice currency:strCurr];
    NSString *OldPrice = [Global PriceRoundOffToString:strOldPrice currency:strCurr];
    
    NSDictionary *attributes = @{
                                 NSStrikethroughStyleAttributeName: [NSNumber numberWithInt:NSUnderlineStyleSingle]
                                 };
    
    NSAttributedString *attrText = [[NSAttributedString alloc] initWithString:OldPrice attributes:attributes];
    
    
    cell.lblOldPrice.attributedText = attrText;
    
    cell.lblNewPrice.text = NewPrice;
    
    
    NSString *url = [[arrOrder objectAtIndex:indexPath.row] valueForKey:@"image"];
    
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activity.center = cell.imgProductImage.center;
    activity.hidesWhenStopped = YES;
    [activity startAnimating];
    [cell.imgProductImage addSubview:activity];
    
    [cell.imgProductImage sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
        // Get rid of the activity indicator when the image has been loaded
        [activity stopAnimating];
    }];
    cell.imgProductImage.contentMode = UIViewContentModeScaleAspectFit;
    
    cell.lblReviewYourOrder.backgroundColor screenBackgroundColor;
    return cell;
    
}
- (IBAction)btnCreditCard:(id)sender {
    
    FooterClick = YES;
    
    FooterFrame = 302;
    
    if ([_ImgRadioCreditCard.image isEqual:[UIImage imageNamed:@"radio-btn"]])
    {
        
        [_lblCreditCard setTextColor:[UIColor blackColor]];
        _btnCreditCard.layer.borderWidth = 0;
        _ImgRadioCreditCard.image = [UIImage imageNamed:@"check-radio-btn"];
        
        _lblDebitCard.textColor grayColor;
        _imgRadioDebitCard.image = [UIImage imageNamed:@"radio-btn"];
        
        _btnDebitCard.layer.borderWidth = 1;
        _btnDebitCard.layer.borderColor = [[UIColor colorWithRed:229.0/255.0 green:229.0/255.0 blue:229.0/255.0 alpha:1.0] CGColor];
        _btnDebitCard.clipsToBounds = YES;
        
        _lblCOD.textColor grayColor;
        _imgRadioCOD.image = [UIImage imageNamed:@"radio-btn"];
        
        _btnCOD.layer.borderWidth = 1;
        _btnCOD.layer.borderColor = [[UIColor colorWithRed:229.0/255.0 green:229.0/255.0 blue:229.0/255.0 alpha:1.0] CGColor];
        _btnCOD.clipsToBounds = YES;
        
        _viewCardDetails.hidden=NO;
        
        _btnMakePayment.frame = btnPayMentFrameY;
        [_tblCheckOut reloadData];
        [_tblCheckOut scrollRectToVisible:CGRectMake(0, _tblCheckOut.contentSize.height - _tblCheckOut.bounds.size.height, _tblCheckOut.bounds.size.width, _tblCheckOut.bounds.size.height) animated:YES];
    }
}

- (IBAction)btnDebitCard:(id)sender {
    
    FooterClick = YES;
    
    FooterFrame = 302;
    
    if ([_imgRadioDebitCard.image isEqual:[UIImage imageNamed:@"radio-btn"]])
    {
        [_lblDebitCard setTextColor:[UIColor blackColor]];
        _btnDebitCard.layer.borderWidth = 0;
        _imgRadioDebitCard.image = [UIImage imageNamed:@"check-radio-btn"];
        
        _lblCreditCard.textColor grayColor;
        _ImgRadioCreditCard.image = [UIImage imageNamed:@"radio-btn"];
        
        _btnCreditCard.layer.borderWidth = 1;
        _btnCreditCard.layer.borderColor = [[UIColor colorWithRed:229.0/255.0 green:229.0/255.0 blue:229.0/255.0 alpha:1.0] CGColor];
        _btnCreditCard.clipsToBounds = YES;
        
        _lblCOD.textColor grayColor;
        _imgRadioCOD.image = [UIImage imageNamed:@"radio-btn"];
        
        _btnCOD.layer.borderWidth = 1;
        _btnCOD.layer.borderColor = [[UIColor colorWithRed:229.0/255.0 green:229.0/255.0 blue:229.0/255.0 alpha:1.0] CGColor];
        _btnCOD.clipsToBounds = YES;
        
        _viewCardDetails.hidden=NO;
        
        _btnMakePayment.frame = btnPayMentFrameY;
        [_tblCheckOut reloadData];
        
        
        [_tblCheckOut scrollRectToVisible:CGRectMake(0, _tblCheckOut.contentSize.height - _tblCheckOut.bounds.size.height, _tblCheckOut.bounds.size.width, _tblCheckOut.bounds.size.height) animated:YES];
    }
}
- (IBAction)btnCOD:(id)sender {
    
    FooterClick = YES;
    
    FooterFrame = 200;
    
    if ([_imgRadioCOD.image isEqual:[UIImage imageNamed:@"radio-btn"]]) {
        
        [_lblCOD setTextColor:[UIColor blackColor]];
        _btnCOD.layer.borderWidth = 0;
        _imgRadioCOD.image = [UIImage imageNamed:@"check-radio-btn"];
        
        _lblCreditCard.textColor grayColor;
        _ImgRadioCreditCard.image = [UIImage imageNamed:@"radio-btn"];
        
        _btnCreditCard.layer.borderWidth = 1;
        _btnCreditCard.layer.borderColor = [[UIColor colorWithRed:229.0/255.0 green:229.0/255.0 blue:229.0/255.0 alpha:1.0] CGColor];
        _btnCreditCard.clipsToBounds = YES;
        
        _lblDebitCard.textColor grayColor;
        _imgRadioDebitCard.image = [UIImage imageNamed:@"radio-btn"];
        
        _btnDebitCard.layer.borderWidth = 1;
        _btnDebitCard.layer.borderColor = [[UIColor colorWithRed:229.0/255.0 green:229.0/255.0 blue:229.0/255.0 alpha:1.0] CGColor];
        _btnDebitCard.clipsToBounds = YES;
        
        _viewCardDetails.hidden=YES;
        
        CGRect frame = btnPayMentFrameY;
        
        frame.origin.y = 80;
        
        _btnMakePayment.frame = frame;
        [_tblCheckOut reloadData];
    }
    
}

- (void)shippingAddress {
    
    NSString *URL = apiUrlShippingAddress;
    NSString *customerId = [[NSUserDefaults standardUserDefaults] valueForKey:@"Uid"];
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:customerId,@"customerId", nil];
    [Global urlString:[apiDomain stringByAppendingString:URL] parameter:dic indicator:NO currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response) {
        if(result)
        {
            arrShippingAddress = [[NSMutableArray alloc]init];
            NSString *srtAddress;
            if ([[response valueForKey:@"status"] isEqualToString:@"SUCCESS"]) {
                
                arrShippingAddress = [response valueForKey:@"details"];
                
                NSMutableArray *arrAddress = [[NSMutableArray alloc]init];
                
                [arrAddress addObject:[[arrShippingAddress valueForKey:@"street"] objectAtIndex:0]];
                [arrAddress addObject:[arrShippingAddress valueForKey:@"city"]];
                [arrAddress addObject:[[arrShippingAddress valueForKey:@"region"] valueForKey:@"region"]];
                [arrAddress addObject:[arrShippingAddress valueForKey:@"country_id"]];
                [arrAddress addObject:[arrShippingAddress valueForKey:@"postcode"]];
                [arrAddress addObject:[NSString stringWithFormat:@"Mobile - %@",[arrShippingAddress valueForKey:@"telephone"]]];
                
                srtAddress = [arrAddress componentsJoinedByString:@"\r"];
                
                NSMutableAttributedString *attrString = [[NSMutableAttributedString  alloc] initWithString:srtAddress];
                NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
                [style setLineSpacing:5];
                [attrString addAttribute:NSParagraphStyleAttributeName
                                   value:style
                                   range:NSMakeRange(0, [srtAddress length])];
                _lblAddressViewForHeader.attributedText = attrString;
                
                _lblAddressViewForHeader.font = [UIFont fontWithName:gmFont size:12];
                //[activityAddress stopAnimating];
                _btnChangeViewForHeader.hidden = NO;
                _imgAddressIcon.hidden = NO;
            }
        }
        if ([[response valueForKey:@"status"] isEqualToString:@"ERROR"]) {
            arrShippingAddress = [response valueForKey:@"details"];
            if (arrShippingAddress.count == 0) {
                [_btnChangeViewForHeader setTitle:@"Add" forState:UIControlStateNormal];
                _lblAddressViewForHeader.text = @"Please add 'Shipping Address' to deliver an order";
                _btnChangeViewForHeader.hidden = NO;
                _imgAddressIcon.hidden = NO;
            }
        }
    }];
}
- (IBAction)btnAddressChange:(id)sender {
    ShippingAddressViewController *wish = [[ShippingAddressViewController alloc] init];
    wish.arrShippingAddress = arrShippingAddress;
    wish.fromView = @"Shipping";
    [self.navigationController pushViewController:wish animated:YES];
}
//- (IBAction)btnMakePayment:(id)sender {
//
//    PUMMainVController *payMoney = [[PUMMainVController alloc]init];
//
//    [self.navigationController pushViewController:payMoney animated:YES];
//
//    self.params = [PUMRequestParams sharedParams];//Using ’init’ and    //‘new’should be avoided here. ‘sharedParams’ is the dedicated
//    //initializer (or designated initializer)for ‘PUMRequestParams’.
//    self.params.environment = PUMEnvironmentProduction;
//    self.params.surl = @"https://www.yourdomain.com/success.php";
//    self.params.furl = @"https://www.yourdomain.com/failure.php";
//    self.params.amount = @"100";
//    self.params.key = @"pass your own key here";
//    self.params.merchantid = @"pass your unique id here";
//    self.params.txnid =@"pass random transaction id each time";
//    self.params.delegate = self;
//    self.params.hashValue = @"653fdfdf79f8074e6cf638f869bc5ff9ae6bd2aeea3cd1d6d17965b74e599afb4fad5ccf3ec423217aec1e2539267f9212dbde6de1cc88045376e9f967b14db5";
//    /// Pass the hash generated from the server side code here. Refer ‘Calculating Hash’ section below.
//    self.params.firstname = @"Ashish";
//    self.params.productinfo = @"iPhone";
//    self.params.email = @"test@gmail.com";
//    self.params.phone = @"9876543210";
//    self.params.udf1 = @"";
//    self.params.udf2 = @"";
//    self.params.udf3 = @"";
//    self.params.udf4 = @"";
//    self.params.udf5 = @"";
//

    
//    NSString *paymentMethod, *cart_Id;
//    if ([_lblAddressViewForHeader.text isEqualToString:@"Loading...Please wait."]) {
//        [Global alertAdd:@"Please wait while 'Shipping Address' is loading" withTitle:@"Address" navBack:NO];
//        return;
//    }
//    if ([_lblAddressViewForHeader.text isEqualToString:@"Please add 'Shipping Address' to deliver an order"]) {
//        [Global alertAdd:@"Please add 'Shipping Address' to deliver an order" withTitle:@"Address" navBack:NO];
//        return;
//    }
//    
//    if ([_imgRadioCOD.image isEqual:[UIImage imageNamed:@"check-radio-btn"]])
//    {
//        paymentMethod = apiUrlCOD;
//        
//    }
//    if ([_imgRadioDebitCard.image isEqual:[UIImage imageNamed:@"check-radio-btn"]])
//    {
//        paymentMethod = apiUrlDebitCard;
//        
//    }
//    if ([_ImgRadioCreditCard.image isEqual:[UIImage imageNamed:@"check-radio-btn"]])
//    {
//        paymentMethod = apiUrlCreditCard;
//        
//    }
//    
//    cart_Id = [[arrOrder objectAtIndex:0] valueForKey:@"cartId"];
//    
//    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:cart_Id,@"cartId",@"cashondelivery",@"payment_method", nil];
//    [Global urlString:[apiDomain stringByAppendingString:paymentMethod] parameter:dic indicator:YES currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response) {
//        if(result) { //DATA_MISSING
//            if ([[response valueForKey:@"status"] isEqualToString:@"DATA_MISSING"]) {
//                
//                
//                //Response coming from API
//                
//                
//                //                {
//                //                    details =     {
//                //                        message = "Please check the shipping address information. %1";
//                //                        parameters =         (
//                //                                              "Please enter the first name. Please enter the last name. Please enter the street. Please enter the city. Please enter the phone number. Please enter the zip/postal code. Please enter the country."
//                //                                              );
//                //                    };
//                //                    status = ERROR;
//                //                }
//                
//            }
//        }
//    }];
    
 

@end
