//
//  ShippingAddressViewController.h
//  ShopApp
//
//  Created by Matrid on 24/03/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"


@interface ShippingAddressViewController : UIViewController
{
    NSArray *countryList, *stateList;
    NSString *coutry_id, *region_id, *regionName, *regionCode;
    BOOL countryClick, stateClick;
}

@property (strong, nonatomic) NSString *fromView;

@property (strong, nonatomic) NSMutableArray *arrShippingAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtCountryName;

@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;

@property (weak, nonatomic) IBOutlet UITextField *txtStreet;

@property (weak, nonatomic) IBOutlet UITextField *txtCity;
@property (weak, nonatomic) IBOutlet UITextField *txtState;
@property (weak, nonatomic) IBOutlet UITextField *txtZipCode;
@property (weak, nonatomic) IBOutlet UITextField *txtPhone;

@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITableView *tblDropDown;
@property (weak, nonatomic) IBOutlet UITableView *tblStateDropDown;

@end
