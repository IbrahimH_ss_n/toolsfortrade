//
//  ProductDetailModel.h
//  ShopApp
//
//  Created by Matrid on 13/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductDetailModel : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *sku;
@property (nonatomic, strong) NSString *current_price;
@property (nonatomic, strong) NSString *old_price;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSString *specification;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *rating;
@property (nonatomic, strong) NSString *userCount;

@property (nonatomic, strong) NSMutableArray *productImages;
@property (nonatomic, strong) NSMutableArray *related_products;
@property (nonatomic, strong) NSMutableArray *optionColor;
@property (nonatomic, strong) NSMutableArray *optionSize;

@end
