//
//  ColCatagoryViewCell.h
//  ShopApp
//
//  Created by Matrid on 29/12/16.
//  Copyright © 2016 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColCatagoryViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgCatagoryCell;
@property (weak, nonatomic) IBOutlet UILabel *lblCatagoryCell;

@end
