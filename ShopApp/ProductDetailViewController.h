//
//  ProductDetailViewController.h
//  ShopApp
//
//  Created by Matrid on 10/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"
#import "productSizeCell.h"
#import "HCSStarRatingView.h"

@interface ProductDetailViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource,UIWebViewDelegate>
{
    BOOL status;
    NSString *attribute_idColor;
    NSString *value_indexColor;
    NSString *attribute_idSize;
    NSString *value_indexSize;
    NSString *cartId;
    UIActivityIndicatorView *activity;
    
}

@property (strong, nonatomic) NSString *product_Id;

@property (weak, nonatomic) IBOutlet UICollectionView *colProductDetail;
@property (weak, nonatomic) IBOutlet UICollectionView *colProductColorImg;
@property (weak, nonatomic) IBOutlet UICollectionView *colProductSize;
@property (strong, nonatomic) IBOutlet UICollectionView *colRelatedProduct;

@property (weak, nonatomic) IBOutlet UIButton *btnLeftMove;
@property (weak, nonatomic) IBOutlet UIButton *btnRightMove;
@property (weak, nonatomic) IBOutlet UILabel *lblProductName;
@property (weak, nonatomic) IBOutlet UILabel *lblProductPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblProductRating;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRatingView;

@property (strong, nonatomic) IBOutlet UITableView *tblDetailView;
@property (strong, nonatomic) IBOutlet UIButton *btnAddToBag;
@property (strong, nonatomic) IBOutlet UIView *viewProductSize;
@property (strong, nonatomic) IBOutlet UIView *viewRelatedProduct;
@property (strong, nonatomic) IBOutlet UIWebView *mwebview_productdetail;


@property (strong, nonatomic) IBOutlet UIView *viewProductHeader;
@end
