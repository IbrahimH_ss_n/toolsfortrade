//
//  SignUpViewController.m
//  ShopApp
//
//  Created by Matrid on 06/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import "SignUpViewController.h"
#import "Constant.h"

@interface SignUpViewController ()

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _btnSignUp.layer.cornerRadius=20;
    _scrollView.contentSize=CGSizeMake(320, 420);
    self.edgesForExtendedLayout = UIRectEdgeNone;
}


- (IBAction)btnSignUpClick:(id)sender {
    if ([_txtName.text isEqualToString:@""]) {
        [Global alertAdd:@"Please enter name." withTitle:nil navBack:NO];
        return;
    }
    if ([_txtLName.text isEqualToString:@""]) {
        [Global alertAdd:@"Please enter last name." withTitle:nil navBack:NO];
        return;
    }
    if ([_txtEmail.text isEqualToString:@""]) {
        [Global alertAdd:@"Please enter email." withTitle:nil navBack:NO];
        return;
    }
    else if(![Global validateEmailWithString:_txtEmail.text]){
        [Global alertAdd:@"Please enter valid email." withTitle:nil navBack:NO];
        return;
    }
   
    NSString *length1 = _txtPassword.text;
    NSString *length2 = _txtRPassword.text;
    
    if ([_txtPassword.text isEqualToString:@""]) {
        [Global alertAdd:@"Please enter password." withTitle:nil navBack:NO];
        return;
        
    }
    else if (length1.length<8)
    {
        [Global alertAdd:@"Password Must Contain Atleast 8 Letters!" withTitle:nil navBack:NO];
        _txtPassword.text = @"";
        [_txtPassword becomeFirstResponder];
        return;
    }
    
    else if ([_txtRPassword.text isEqualToString:@""]) {
        [Global alertAdd:@"Please Re-enter password." withTitle:nil navBack:NO];
        return;
    }
//    else if (length2.length<8)
//    {
//        [Global alertAdd:@"Password Must Contain Atleast 8 Letters!" withTitle:nil navBack:NO];
//        [_txtRPassword becomeFirstResponder];
//        return;
//    }
    else if (![_txtPassword.text isEqualToString:_txtRPassword.text]) {
        [Global alertAdd:@"Password Mismatch Please Enter Again!" withTitle:nil navBack:NO];
        _txtPassword.text = @"";
        _txtRPassword.text = @"";
        [_txtPassword becomeFirstResponder];
        return;
    }
    else {
        if (![self isPasswordCorrect]) {
            return;
        }
    }
    
    NSDictionary *dicty = [NSDictionary dictionaryWithObjectsAndKeys:_txtName.text,@"name",_txtLName.text,@"last",_txtEmail.text,@"email",_txtPassword.text,@"password", nil];
    [Global urlString:[apiDomain stringByAppendingString:apiUrlSignUp] parameter:dicty indicator:YES currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response) {
        if(result) {
            NSLog(@"Value of response = %@", response);
            if ([[response valueForKey:@"status"] isEqualToString:@"DATA_MISSING"]) {
                [Global alertAdd:@"Your entred email or password is incorrect." withTitle:nil navBack:NO];
            }
            else if ([[response valueForKey:@"status"] isEqualToString:@"SUCCESS"]) {
                [[NSUserDefaults standardUserDefaults] setValue:[[response valueForKey:@"detail"] valueForKey:@"id"] forKey:@"Uid"];
                [Global alertAdd:@"Account Created Successfully" withTitle:nil navBack:YES];
                
            }
            else if([[response valueForKey:@"status"] isEqualToString:@"Already Exist Email Id"])
            {
                [Global alertAdd:@"This email id is already exists." withTitle:nil navBack:NO];
            }
           
        }
    }];
}

- (BOOL)isPasswordCorrect{
    
    BOOL lowerCaseLetter = 0,upperCaseLetter = 0,digit = 0,specialCharacter = 0;
    
    for (int i = 0; i < [_txtPassword.text length]; i++)
    {
        unichar c = [_txtPassword.text characterAtIndex:i];
        if(!lowerCaseLetter)
        {
            lowerCaseLetter = [[NSCharacterSet lowercaseLetterCharacterSet] characterIsMember:c];
        }
        if(!upperCaseLetter)
        {
            upperCaseLetter = [[NSCharacterSet uppercaseLetterCharacterSet] characterIsMember:c];
        }
        if(!digit)
        {
            digit = [[NSCharacterSet decimalDigitCharacterSet] characterIsMember:c];
        }
        if(!specialCharacter)
        {
            NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@"-/:;()$&@“.,?!’"];
            NSRange range = [_txtPassword.text rangeOfCharacterFromSet:cset];
            if (range.location == NSNotFound)
            {
                specialCharacter = NO;
            }
            else
            {
                specialCharacter = YES;
            }
            
        }
    }
    
    if(specialCharacter && digit && lowerCaseLetter && upperCaseLetter)
    {
        return YES;
    }
    else
    {
        [Global alertAdd:@"Please Ensure that you have at least one lower case letter, one upper case letter, one digit and one special character" withTitle:@"Error" navBack:NO];
        _txtPassword.text = @"";
        _txtRPassword.text = @"";
        return NO;
    }
    return NO;
}

- (IBAction)btnBackClick:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }
    return YES;
} 

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{

//    if (textField == _txtPassword) {
//        BOOL lowerCaseLetter = 0,upperCaseLetter = 0,digit = 0,specialCharacter = 0;
//        if([textField.text length] >= 8)
//        {
//            for (int i = 0; i < [_txtPassword.text length]; i++)
//            {
//                unichar c = [_txtPassword.text characterAtIndex:i];
//                if(!lowerCaseLetter)
//                {
//                    lowerCaseLetter = [[NSCharacterSet lowercaseLetterCharacterSet] characterIsMember:c];
//                }
//                if(!upperCaseLetter)
//                {
//                    upperCaseLetter = [[NSCharacterSet uppercaseLetterCharacterSet] characterIsMember:c];
//                }
//                if(!digit)
//                {
//                    digit = [[NSCharacterSet decimalDigitCharacterSet] characterIsMember:c];
//                }
//                if(!specialCharacter)
//                {
//                    NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@"-/:;()$&@“.,?!’"];
//                    NSRange range = [_txtPassword.text rangeOfCharacterFromSet:cset];
//                    if (range.location == NSNotFound)
//                    {
//                        specialCharacter = NO;
//                    }
//                    else
//                    {
//                        specialCharacter = YES;
//                    }
//                    
//                }
//            }
//            
//            if(specialCharacter && digit && lowerCaseLetter && upperCaseLetter)
//            {
//                return YES;
//            }
//            else
//            {
//                [Global alertAdd:@"Please Ensure that you have at least one lower case letter, one upper case letter, one digit and one special character" withTitle:@"Error" navBack:NO];
//                _txtPassword.text = @"";
//                return NO;
//            }
//            
//        }
//        else
//        {
//            [Global alertAdd:@"Please Enter at least 8 password" withTitle:@"Error" navBack:NO];
//            _txtPassword.text = @"";
//            [_txtPassword resignFirstResponder];
//            return NO;
//        }
//
//    }
    return YES;
}

@end
