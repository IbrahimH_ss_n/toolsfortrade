//
//  ProductDetailCollectionViewCell.h
//  ShopApp
//
//  Created by Matrid on 11/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface ProductDetailCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgProductDetailCell;

@end
