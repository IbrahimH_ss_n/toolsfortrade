//
//  Singletons.h
//  ShopApp
//
//  Created by Matrid on 27/12/16.
//  Copyright © 2016 Test. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Singletons : NSObject



@property(strong, nonatomic) NSString *headerTitle;
@property(strong, nonatomic) NSString *loginType;
@property(strong, nonatomic) NSString *loginUserName;

@property NSString *isFrom;
@property BOOL ismenuOpen;

@property(strong, nonatomic) NSArray *shopByCategoryArray;
@property(strong, nonatomic) NSArray *shopByCategoryArrayHome;

@property(strong,nonatomic) NSMutableDictionary *ssLoginData;


+(Singletons *)singleton;



@end
