//
//  tblFilterGroupTableViewCell.h
//  ShopApp
//
//  Created by Matrid on 19/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface tblFilterGroupTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblFilterGroupCount;
@property (weak, nonatomic) IBOutlet UIButton *btnFilterGroupName;


@end
