//
//  Category.m
//  ShopApp
//
//  Created by Md Ibrahim Hassan on 26/04/18.
//  Copyright © 2018 Test. All rights reserved.
//

#import "Category.h"
#import "Utility.h"
#import "SubCategory.h"

@interface Category()
@property (strong, nonatomic) NSMutableArray *selectedValues;
@property (nonatomic, strong) NSMutableArray *selectedOptions;
@property (nonatomic, strong) NSMutableArray *selectedLabels;
@property (nonatomic, strong) NSMutableDictionary *selectionParameters;
@end

@implementation Category

-(id) initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super init]) {
        self.selectionParameters = [[NSMutableDictionary alloc]init];
        self.selectedValues = [[NSMutableArray alloc]init];
        NSString *title = [dictionary valueForKey:@"title"];
        if (![Utility isNullOrEmpty:title]) {
            self.title = title;
        }
        NSString *code = [dictionary valueForKey:@"code"];
        if (![Utility isNullOrEmpty:code]) {
            self.code = code;
        }
        id listObj = [dictionary valueForKey:@"options"];
        NSMutableArray* optionsArray = [[NSMutableArray alloc] init];
        if (listObj && [listObj isKindOfClass:[NSArray class]]) {
            NSArray *options = (NSArray*)listObj;
            for (NSDictionary *dictionary in options) {
                SubCategory *subCategory = [[SubCategory alloc] initWithDictionary:dictionary];
                [optionsArray addObject: subCategory];
            }
            self.options = [NSMutableArray arrayWithArray:optionsArray];
            NSMutableArray *mutableArray = [NSMutableArray arrayWithCapacity:self.options.count];
            for (NSUInteger i = 0; i < self.options.count; i++) {
                [mutableArray insertObject:[NSNumber numberWithInt:0] atIndex:i];
            }
            self.selectedOptions = [mutableArray mutableCopy];
        }
    }
    return self;
}

- (NSInteger)getSubCategoryCountFor: (NSString *)filterString {
    int countOfFiteredData = 0;
    if ([filterString length] == 0) {
        return self.getSubCategoryCount;
    } else {
        for (int i = 0; i < self.options.count; i ++) {
            SubCategory *subCat = [self.options objectAtIndex:i];
            if ([subCat.label.lowercaseString containsString:filterString.lowercaseString] && [filterString length] != 0) {
                countOfFiteredData ++;
            }
        }
    }
    return countOfFiteredData;
}

-(NSInteger)getSubCategoryCount {
    return self.options.count;
}

- (void)handleSelectionAt:(NSInteger)index {
    SubCategory *selectedSubCategory = (SubCategory *)[self.options objectAtIndex:index];
    [self.selectedValues addObject:selectedSubCategory.value];
    if (index < self.selectedOptions.count) {
        if ([[self.selectedOptions objectAtIndex:index] isEqual: [NSNumber numberWithInt:1]]) {
            [self.selectedOptions replaceObjectAtIndex:index withObject: [NSNumber numberWithInt:0]];
            if ([self.selectedValues containsObject:selectedSubCategory.value]) {
                [self.selectedValues removeObject:selectedSubCategory.value];
            }
        } else if ([[self.selectedOptions objectAtIndex:index]  isEqual: [NSNumber numberWithInt:0]]) {
            [self.selectedOptions replaceObjectAtIndex:index withObject: [NSNumber numberWithInt:1]];
        }
    }
    NSString *joinedString = [self.selectedValues componentsJoinedByString:@"_"];
    NSDictionary *dict = @{self.code : joinedString};
    NSMutableDictionary* tempDict = [[NSMutableDictionary alloc] initWithDictionary:dict];
    [self.selectionParameters addEntriesFromDictionary:tempDict];
}

- (void)handleSelectionFor:(NSString *)label {
    for (int i = 0; i < self.options.count; i ++) {
        SubCategory *selectedSubCategory = (SubCategory *)[self.options objectAtIndex:i];
        if ([selectedSubCategory.label isEqualToString:label]) {
            [self handleSelectionAt:i];
            return;
        }
    }
//    [self.selectedValues addObject:selectedSubCategory.value];
}

- (BOOL)isOptionSelectedFor:(NSString *)label {
    for (int i = 0; i < self.options.count; i ++) {
        SubCategory *selectedSubCategory = (SubCategory *)[self.options objectAtIndex:i];
        if ([selectedSubCategory.label isEqualToString:label]) {
            return [self isOptionSelectedAt:i];
        }
    }
    return false;
}

- (BOOL)isOptionSelectedAt:(NSInteger)index {
    if (index < self.selectedOptions.count) {
        return [[self.selectedOptions objectAtIndex:index] boolValue];
    }
    return false;
}

- (void)clearAllSelection {
    NSMutableArray *mutableArray = [NSMutableArray arrayWithCapacity:self.selectedOptions.count];
    for (NSUInteger i = 0; i < self.selectedOptions.count; i++) {
        [mutableArray insertObject:[NSNumber numberWithInt:0] atIndex:i];
    }
    self.selectedOptions = [mutableArray mutableCopy];
    [self.selectedValues removeAllObjects];
    [self.selectionParameters removeAllObjects];
}

- (NSMutableDictionary *) dictionaryValues {
    return self.selectionParameters;
}

- (SubCategory *)filteredOptionsFor: (NSString *)filterString And : (int)index {
    NSMutableArray *filteredArray = [[NSMutableArray alloc]init];
    for (int i = 0; i < self.options.count; i ++) {
        SubCategory *subCategory = (SubCategory *)[self.options objectAtIndex:i];
        if ([subCategory.label.lowercaseString containsString:filterString.lowercaseString]) {
            [filteredArray addObject:subCategory];
        }
    }
    if ([filteredArray count] != 0) {
        return [filteredArray objectAtIndex:index];
    } else {
        return [self.options objectAtIndex:index];
    }
}

@end
