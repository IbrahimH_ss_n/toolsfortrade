//
//  pickerViewController.h
//  HapApp
//
//  Created by Matrid on 18/07/16.
//  Copyright © 2016 Mayank  Setia. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol pickerDeligate <NSObject>

@required

- (void) pickerValue:(NSString *)value;
@end


@interface pickerViewController : UIView <UIPickerViewDelegate, UIPickerViewDataSource, UIGestureRecognizerDelegate> {
    id<pickerDeligate> delegate;
}

@property (strong, nonatomic) NSString *pickerFor;

@property (strong, nonatomic) NSArray *arrCountryList;

@property (strong, nonatomic) NSArray *arrStateList;

@property (strong, nonatomic) UIPickerView *sortPicker;

@property (strong, nonatomic) NSMutableArray *arrSort;

@property (weak, nonatomic) NSString *selected;

@property (strong, nonatomic) UILabel *lblTitle;

@property (strong, nonatomic) UIButton *btnDone;
@property (strong, nonatomic) UIButton *btnHide;

@property (nonatomic,strong) id delegate;

@property BOOL flag;

@end



