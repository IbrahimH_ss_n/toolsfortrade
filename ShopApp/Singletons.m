//
//  Singletons.m
//  ShopApp
//
//  Created by Matrid on 27/12/16.
//  Copyright © 2016 Test. All rights reserved.
//

#import "Singletons.h"

@implementation Singletons

+(Singletons *)singleton {
    
    static dispatch_once_t pred;
    static Singletons *shared = nil;
    
    dispatch_once(&pred, ^{
        shared=[[Singletons alloc] init];

        shared.ssLoginData =[[NSMutableDictionary alloc]init];
        
        shared.loginType = [[NSString alloc] init];
        shared.headerTitle = [[NSString alloc] init];
        shared.loginUserName = [[NSString alloc] init];
        
        shared.isFrom = @"";
        shared.ismenuOpen = NO;
        
        shared.shopByCategoryArray=[[NSArray alloc]init];
        shared.shopByCategoryArrayHome=[[NSArray alloc]init];
    });
    
    return shared;
}

@end
