//
//  CategoryListViewController.m
//  ShopApp
//
//  Created by Matrid on 07/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import "CategoryListViewController.h"

@interface CategoryListViewController ()

@end

@implementation CategoryListViewController

@synthesize strTitle;

- (void)viewDidLoad {

    [super viewDidLoad];
   
   // self.view.backgroundColor  grayColor;
    
    selectedSection=-1;
    [Global customHeader:self headerTitle:@"Tools4Trade" itemFound:NO hideBackButton:NO hideSearchButton:NO hideNotificationButton:NO];
    
    [Global customFooter:self.view];

    shopByCategoryArrayMenu = [Singletons singleton].shopByCategoryArray;
    self.edgesForExtendedLayout = UIRectEdgeNone;

}
-(void)viewWillAppear:(BOOL)animated
{
//    if (shopByCategoryArrayMenu.count == 0) {
//        sleep(5);
//        shopByCategoryArrayMenu = [Singletons singleton].shopByCategoryArray;
//        [_tblCategoryList reloadData];
//    }
    
    
    btnBack=[[UIButton alloc] initWithFrame:CGRectMake(5, 20, 25, 40)];
    [btnBack setImage:[UIImage imageNamed:@"back"] forState:normal];
    [btnBack addTarget:self action:@selector(btnBackAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBack];
    
    _lblMenuTitle.text=strTitle;
    _lblMenuTitle.font = [UIFont fontWithName:gmFont size:16];
    _lblMenuTitle.textColor darkGrayColor;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return [[shopByCategoryArrayMenu valueForKey:@"children_data"] count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==selectedSection) {
        return [[[shopByCategoryArrayMenu objectAtIndex:section]valueForKey:@"children_data"] count];
    }
    return 0;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    /// Here you can set also height according to your section and row
    return 30;
}

- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)section
{
    UIView *viewCategoryHeader=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 40)];
    //viewCategoryHeader.backgroundColor grayColor;
    
    UIButton *btnSectionSelection=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 320, 40)];
    [btnSectionSelection addTarget:self action:@selector(seletedSectionFunc:) forControlEvents:UIControlEventTouchUpInside];
    btnSectionSelection.tag=section;
    [viewCategoryHeader addSubview:btnSectionSelection];
    
    UILabel *lblCategoryName=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, 260, 40)];
    NSString *unfilteredString=[NSString stringWithFormat:@"%@",[[shopByCategoryArrayMenu objectAtIndex:section]valueForKey:@"name"]];
    lblCategoryName.textColor  grayColor;
    
    lblCategoryName.text=unfilteredString;
    lblCategoryName.font=[UIFont fontWithName:gmFont size:14];
    [viewCategoryHeader addSubview:lblCategoryName];
    if ([[[shopByCategoryArrayMenu objectAtIndex:section] valueForKey:@"children_data"] count]==0) {
        UIImageView *imgForward=[[UIImageView alloc]initWithFrame:CGRectMake(290, 15, 10, 10)];
        //imgForward.image=[UIImage imageNamed:@"forwardBlackIcon.png"];
        [viewCategoryHeader addSubview:imgForward];
    }
    else if (section==selectedSection) {
        UIButton *btnIcon=[[UIButton alloc]initWithFrame:CGRectMake(280, 0, 30, 40)];
        [btnIcon setImage:[UIImage imageNamed:@"minus"] forState:UIControlStateNormal];
        btnIcon.tag=section;
        [btnIcon addTarget:self action:@selector(seletedSectionFunc:) forControlEvents:UIControlEventTouchUpInside];
        [viewCategoryHeader addSubview:btnIcon];
    }
    else
    {
        UIButton *btnIcon=[[UIButton alloc]initWithFrame:CGRectMake(280, 0, 30, 40)];
        [btnIcon setImage:[UIImage imageNamed:@"plus"] forState:UIControlStateNormal];
        btnIcon.tag=section;
        [btnIcon addTarget:self action:@selector(seletedSectionFunc:) forControlEvents:UIControlEventTouchUpInside];
        [viewCategoryHeader addSubview:btnIcon];
    }
    return viewCategoryHeader;
}

- (UIView *)tableView:(UITableView *)tv viewForFooterInSection:(NSInteger)section
{
    
    UIView *viewCategoryFooter=viewCategoryFooter=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 295, 2)];
    UILabel *lblCategoryFooterLine=[[UILabel alloc]initWithFrame:CGRectMake(10, 0, 295, 1)];
    lblCategoryFooterLine.backgroundColor=[UIColor lightGrayColor];
    [viewCategoryFooter addSubview:lblCategoryFooterLine];
    
    return viewCategoryFooter;
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *defaultCell = @"cellDefault";
    categoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:defaultCell];
    
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"categoryTableViewCell" owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    cell.lblCategoryTableCell.text=[[[[shopByCategoryArrayMenu objectAtIndex:indexPath.section]valueForKey:@"children_data"] objectAtIndex:indexPath.row] valueForKey:@"name"];
     cell.lblCategoryTableCell.font = [UIFont fontWithName:gmFont size:11];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if ([[[[[shopByCategoryArrayMenu objectAtIndex:indexPath.section]valueForKey:@"children_data"] objectAtIndex:indexPath.row] valueForKey:@"children_data"] count]==0) {
        
        ProductListViewController *product=[[ProductListViewController alloc]init];
        product.catgory_Id=[[[[shopByCategoryArrayMenu objectAtIndex:indexPath.section]valueForKey:@"children_data"] objectAtIndex:indexPath.row] valueForKey:@"id"];
         product.btnSearchFlag=YES;
        [Singletons singleton].isFrom = @"";
        [self.navigationController pushViewController:product animated:YES];
    }
    else
    {
        dicSubCategoryTemp=[[[shopByCategoryArrayMenu objectAtIndex:indexPath.section]valueForKey:@"children_data"] objectAtIndex:indexPath.row];
         strTitle = [[[[shopByCategoryArrayMenu objectAtIndex:indexPath.section]valueForKey:@"children_data"] objectAtIndex:indexPath.row]valueForKey:@"name"];
        shopByCategoryArrayMenu=[dicSubCategoryTemp valueForKey:@"children_data"];
       
        [_tblCategoryList reloadData];
        _lblMenuTitle.text=strTitle;
        
    }
}

-(IBAction)seletedSectionFunc:(id)sender
{
     UIButton *button = (UIButton*)sender;
    if ([[[shopByCategoryArrayMenu objectAtIndex:button.tag]valueForKey:@"children_data"] count]==0) {
        ProductListViewController *product=[[ProductListViewController alloc]init];
       // NSString *strCAt = [[shopByCategoryArrayMenu objectAtIndex:button.tag]valueForKey:@"category_id"];
        NSString *cat_Id = [NSString stringWithFormat:@"%@",[[shopByCategoryArrayMenu objectAtIndex:button.tag]valueForKey:@"id"]];
        product.catgory_Id = cat_Id;
        product.btnSearchFlag=YES;
        [Singletons singleton].isFrom = @"";
        [self.navigationController pushViewController:product animated:YES];
    }
    else
    {
        if (selectedSection==button.tag)
            selectedSection=-1;
        else
        {
            selectedSection=button.tag;
        }
        
        [_tblCategoryList reloadData];
    }
    
}
-(void)btnBackAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
