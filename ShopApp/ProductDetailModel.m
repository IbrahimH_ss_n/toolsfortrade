//
//  ProductDetailModel.m
//  ShopApp
//
//  Created by Matrid on 13/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import "ProductDetailModel.h"

@implementation ProductDetailModel

@synthesize name,
            current_price,
            description,
            specification,
            currency,
            productImages,
            rating,
            userCount,
            related_products,
            optionColor,
            url,
            optionSize;

@end
