//
//  UserScreenTableViewCell.h
//  ShopApp
//
//  Created by Matrid on 18/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserScreenTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblUserTable;
@property (strong, nonatomic) IBOutlet UIImageView *imgUserTable;

@end
