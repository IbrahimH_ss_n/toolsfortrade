//
//  ShoppingCartCollectionViewCell.h
//  ShopApp
//
//  Created by Matrid on 18/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShoppingCartCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgMainCartCell;
@property (weak, nonatomic) IBOutlet UIImageView *imgProductImage;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectedSize;

@property (weak, nonatomic) IBOutlet UILabel *lblProductDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblSoldByCompanyName;

@property (weak, nonatomic) IBOutlet UILabel *lblNewPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblOldePrice;
@property (weak, nonatomic) IBOutlet UILabel *lblOffonProduct;
@property (weak, nonatomic) IBOutlet UIButton *btnRemove;
@property (weak, nonatomic) IBOutlet UIButton *btnMoveToWishList;
@property (weak, nonatomic) IBOutlet UIButton *btnAddQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectedQty;
@property (weak, nonatomic) IBOutlet UIView *viewAddQty;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (weak, nonatomic) IBOutlet UIButton *btn3;
@property (weak, nonatomic) IBOutlet UIButton *btn4;
@property (weak, nonatomic) IBOutlet UIButton *btn5;

@end
