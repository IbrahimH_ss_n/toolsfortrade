//
//  UserViewController.m
//  ShopApp
//
//  Created by Matrid on 30/12/16.
//  Copyright © 2016 Test. All rights reserved.
//

#import "UserViewController.h"

@interface UserViewController ()
{
    NSString *strAdreess;
}
@end

@implementation UserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor  screenBackgroundColor;
    
    //[Global customHeader:self headerTitle:@"S A V I S H O P S" itemFound:NO hideBackButton:YES hideSearchButton:NO hideNotificationButton:NO];
    
    [Global customFooter:self.view];
    
    arrUserOrder=[[NSArray alloc]initWithObjects:@"My Cart",@"My Wishlist", nil];
    arrImageOrder=[[NSArray alloc]initWithObjects:@"my-cart",@"my-wishlist", nil];
    
    /*arrUserMyInfo=[[NSArray alloc]initWithObjects:@"Shipping Address",@"Billing Information", nil];
    arrImageMyInfo=[[NSArray alloc]initWithObjects:@"Personal-address",@"billing-information", nil];*/
    
    arrUserMyInfo=[[NSArray alloc]initWithObjects: nil];
    arrImageMyInfo=[[NSArray alloc]initWithObjects:nil];
    

}
- (void)viewWillAppear:(BOOL)animated{
    _viewUserHeader.backgroundColor greenColor;
    _lblUserName.text = [[NSUserDefaults standardUserDefaults]valueForKey:@"firstname"];
    _lblUserName.font = [UIFont fontWithName:gmFont size:17];
    _lblUserName.textColor = [UIColor whiteColor];
    
    //_lblWelcome.text = @"Welcome User";
    _lblWelcome.font = [UIFont fontWithName:gmFont size:17];
    _lblWelcome.textColor = [UIColor whiteColor];
    
    _btnLogOut.layer.cornerRadius = 2;

    NSString *customer_Id = [[NSUserDefaults standardUserDefaults] objectForKey:@"Uid"];
    
    NSInteger cutom_Id = [customer_Id intValue];
    if (cutom_Id == 0) {
        [_btnLogOut setTitle:@"Sign In" forState:UIControlStateNormal];
    }
    else {
        [_btnLogOut setTitle:@"Logout" forState:UIControlStateNormal];
    }
    _btnLogOut.titleLabel.font = [UIFont fontWithName:gmFont size:17];
    _btnLogOut.tintColor = [UIColor whiteColor];
    _btnLogOut.backgroundColor greenColor;
    

    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return arrUserOrder.count;
    }
    else
        return arrUserMyInfo.count;
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 41;
}
- (UIView *)tableView:(UITableView *)tv viewForHeaderInSection:(NSInteger)section
{
    UIView *viewCategoryHeader=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 41)];
  //  viewCategoryHeader.backgroundColor screenBackgroundColor;
    UILabel *lblSectionTitle = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, 320, 41)];
    if (section == 0) {
        lblSectionTitle.text = @"ORDERS";
        
    }
    else
    {
        lblSectionTitle.text = @"";
        
    }
    [viewCategoryHeader addSubview:lblSectionTitle];
    lblSectionTitle.font = [UIFont fontWithName:gmFont size:14];
    lblSectionTitle.textColor grayColor;
    return viewCategoryHeader;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath;
{
    return 41;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *defaultCell = @"cellDefault";
    UserScreenTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:defaultCell];
    
    
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"UserScreenTableViewCell" owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    if (indexPath.section == 0) {
        cell.lblUserTable.text = [arrUserOrder objectAtIndex:indexPath.row];
        cell.lblUserTable.font = [UIFont fontWithName:gmFont size:12];
        cell.lblUserTable.textColor grayColor;
        cell.imgUserTable.image = [UIImage imageNamed:[arrImageOrder objectAtIndex:indexPath.row]];
        return cell;
    }
    else
    {
        cell.lblUserTable.text = [arrUserMyInfo objectAtIndex:indexPath.row];
        cell.lblUserTable.font = [UIFont fontWithName:gmFont size:12];
        cell.lblUserTable.textColor grayColor;
        cell.imgUserTable.image = [UIImage imageNamed:[arrImageMyInfo objectAtIndex:indexPath.row]];
        return cell;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        NSString *customer_Id = [[NSUserDefaults standardUserDefaults] objectForKey:@"Uid"];
        
        NSInteger cutom_Id = [customer_Id intValue];
        if (cutom_Id == 0) {
            //[Global alertAdd:@"Please sign in first!" withTitle:nil navBack:NO];
            WebViewController *web = [[WebViewController alloc] init];
            web.urlString = @"view_cart_Anonymous";
            
            [self.navigationController pushViewController:web animated:YES];
            
            
        }
        else
        {
            WebViewController *web = [[WebViewController alloc] init];
            web.urlString = @"view_cart";
            
            [self.navigationController pushViewController:web animated:YES];
            /*CartViewController *cart = [[CartViewController alloc] init];
             
             [self.navigationController pushViewController:cart animated:YES];*/
        }

    }
    if (indexPath.section == 0 && indexPath.row == 1) {
        NSString *customer_Id = [[NSUserDefaults standardUserDefaults] objectForKey:@"Uid"];
        
        NSInteger cutom_Id = [customer_Id intValue];
        if (cutom_Id == 0) {
            [Global alertAdd:@"Please sign in first!" withTitle:nil navBack:NO];
        }
        else
        {
            WebViewController *web = [[WebViewController alloc] init];
            web.urlString = @"view_Wishlist";
            
            [self.navigationController pushViewController:web animated:YES];
            /* WishListViewController *wish = [[WishListViewController alloc] init];
             
             [self.navigationController pushViewController:wish animated:YES];*/
        }
    }
    
    if (indexPath.section == 0 && indexPath.row == 2) {
        NSString *customer_Id = [[NSUserDefaults standardUserDefaults] objectForKey:@"Uid"];
        
        
        /*if (cutom_Id == 0) {
            //[Global alertAdd:@"Please sign in first!" withTitle:nil navBack:NO];
            WebViewController *web = [[WebViewController alloc] init];
            web.urlString = @"view_cart_Anonymous";
            
            [self.navigationController pushViewController:web animated:YES];

            
        }
        else
        {
            WebViewController *web = [[WebViewController alloc] init];
            web.urlString = @"view_cart";
            
            [self.navigationController pushViewController:web animated:YES];
            //CartViewController *cart = [[CartViewController alloc] init];
            
            //[self.navigationController pushViewController:cart animated:YES];
        }*/
        NSString *strCustomerId = [[NSUserDefaults standardUserDefaults] objectForKey:@"Uid"];

        NSString *mCart_id= [[NSUserDefaults standardUserDefaults] objectForKey:@"cart_id"];
        if (mCart_id==nil) {
            [Global alertAdd:@"Cart is empty!" withTitle:nil navBack:NO];
        }else{
            if (strCustomerId==nil) {
                WebViewController *web = [[WebViewController alloc] init];
                web.urlString = @"view_cart_Anonymous";
                
                [self.navigationController pushViewController:web animated:YES];
                
            }else{
                
                WebViewController *web = [[WebViewController alloc] init];
                web.urlString = @"view_cart";
                
                [self.navigationController pushViewController:web animated:YES];
                
            }
            
        
    }
    }
    if (indexPath.section == 0 && indexPath.row == 3) {
        NSString *customer_Id = [[NSUserDefaults standardUserDefaults] objectForKey:@"Uid"];
        
        NSInteger cutom_Id = [customer_Id intValue];
        if (cutom_Id == 0) {
            [Global alertAdd:@"Please sign in first!" withTitle:nil navBack:NO];
        }
        else
        {
            WebViewController *web = [[WebViewController alloc] init];
            web.urlString = @"view_Wishlist";

 [self.navigationController pushViewController:web animated:YES];
           /* WishListViewController *wish = [[WishListViewController alloc] init];
            
            [self.navigationController pushViewController:wish animated:YES];*/
        }
        
        
    }
    if (indexPath.section == 1 && indexPath.row == 0) {
        
        NSString *customer_Id = [[NSUserDefaults standardUserDefaults] objectForKey:@"Uid"];
        
        NSInteger cutom_Id = [customer_Id intValue];
        if (cutom_Id == 0) {
            [Global alertAdd:@"Please sign in first!" withTitle:nil navBack:NO];
        }
        else
        {
            strAdreess = @"Shipping";
            [self shippingAddress];
        }
    }
    if (indexPath.section == 1 && indexPath.row == 1) {
        
        NSString *customer_Id = [[NSUserDefaults standardUserDefaults] objectForKey:@"Uid"];
        
        NSInteger cutom_Id = [customer_Id intValue];
        if (cutom_Id == 0) {
            [Global alertAdd:@"Please sign in first!" withTitle:nil navBack:NO];
        }
        else
        {
            strAdreess = @"Billing";
            [self shippingAddress];
        }
    }
}


- (void) shippingAddress {
    NSString *URL;
    NSString *customerId = [[NSUserDefaults standardUserDefaults] valueForKey:@"Uid"];
    
    if ([strAdreess isEqualToString:@"Shipping"]) {
        URL = apiUrlShippingAddress;
    }
    if ([strAdreess isEqualToString:@"Billing"]) {
        URL = apiUrlBillingAddress;
    }
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:customerId,@"customerId", nil];
    [Global urlString:[apiDomain stringByAppendingString:URL] parameter:dic indicator:YES currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response) {
        if(result)
        {
            arrShippingAddress = [[NSMutableArray alloc]init];
            NSString *srtAddress;
            if ([[response valueForKey:@"status"] isEqualToString:@"SUCCESS"]) {
                
                arrShippingAddress = [response valueForKey:@"details"];
                
                NSMutableArray *arrAddress = [[NSMutableArray alloc]init];
                
                [arrAddress addObject:[[arrShippingAddress valueForKey:@"street"] objectAtIndex:0]];
                [arrAddress addObject:[arrShippingAddress valueForKey:@"city"]];
                [arrAddress addObject:[[arrShippingAddress valueForKey:@"region"] valueForKey:@"region"]];
                [arrAddress addObject:[arrShippingAddress valueForKey:@"country_id"]];
                [arrAddress addObject:[arrShippingAddress valueForKey:@"postcode"]];
                
                srtAddress = [arrAddress componentsJoinedByString:@", "];
               
                [_btnEdit setTitle:@"Edit" forState:UIControlStateNormal];
            }
            
            else
            {
                srtAddress = @"Please Add Your Address";
                [_btnEdit setTitle:@"Add" forState:UIControlStateNormal];
            }
            
            UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:_viewAddress.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(10.0,10.0)];
            
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = _viewAddress.bounds;
            maskLayer.path  = maskPath.CGPath;
            _viewAddress.layer.mask = maskLayer;
            _viewAddressMain.hidden = NO;
            _viewAddress.hidden = NO;
            
            NSMutableAttributedString* attrString = [[NSMutableAttributedString  alloc] initWithString:srtAddress];
            NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
            [style setLineSpacing:10];
            [attrString addAttribute:NSParagraphStyleAttributeName
                               value:style
                               range:NSMakeRange(0, [srtAddress length])];
            _lblAddress.attributedText = attrString;
            
            _lblAddress.font = [UIFont fontWithName:gmFont size:13];

            
        }
    }];
}
- (IBAction)btnCloseAddressView:(id)sender {
    _viewAddressMain.hidden = YES;
    _viewAddress.hidden = YES;
}
- (IBAction)btnEditAddress:(id)sender {
    ShippingAddressViewController *wish = [[ShippingAddressViewController alloc] init];
    wish.arrShippingAddress = arrShippingAddress;
    wish.fromView = strAdreess;
    [self.navigationController pushViewController:wish animated:YES];
    _viewAddressMain.hidden = YES;
    _viewAddress.hidden = YES;
}


- (IBAction)btnLogOutClick:(id)sender {
    
    if ([_btnLogOut.currentTitle isEqualToString:@"Logout"]) {
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"savedEmail"];
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"savedPass"];
        [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"Uid"];

        [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"cart_id"];
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"firstname"];
        
      
        //-------------------------       --------------  ----------------
        //NSDictionary *dicty = [NSDictionary dictionaryWithObjectsAndKeys:@"gnk",@"user",@"kjhk",@"pass",nil];
       [Global urlString:apiUrlLogout parameter:nil indicator:YES currentView:self.view Get_Post:@"GET" completion:^(BOOL result, NSDictionary *response) {
            if(result) {
                if ([[response valueForKey:@"status"] isEqualToString:@"FAILED"]) {
                    [Global alertAdd:@"Your entred email or password is incorrect." withTitle:nil navBack:NO];
                  
                }
                else if ([[response valueForKey:@"status"] isEqualToString:@"SUCCESS"]) {
                    
                   [Global alertAdd:@"Logged out" withTitle:nil navBack:NO];
                    for (UIViewController *view in self.navigationController.viewControllers) {
                        if ([view isKindOfClass:[HomeViewController class]]) {
                            HomeViewController *home = (HomeViewController *)view;
                            [self.navigationController popToViewController:home animated:NO];
                        }
                    }
                }
            }
        }];
        
        //-------------------------  ----------  ------------ ---------------------
        
    }
    else {
        LoginViewController *login = [[LoginViewController alloc]init];
        [self.navigationController pushViewController:login animated:YES];
    }
    
}


@end
