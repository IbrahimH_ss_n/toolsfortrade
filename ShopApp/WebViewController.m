//
//  WebViewController.m
//  ShopApp
//
//  Created by Matrid on 14/04/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

NSString *fullURL;
NSString *deviceType = @"?d=iphone";

@implementation WebViewController

@synthesize urlString;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [Global customHeader:self headerTitle:@"S A V I S H O P S" itemFound:NO hideBackButton:NO hideSearchButton:YES hideNotificationButton:YES];
    NSString *passwrd=[[NSUserDefaults standardUserDefaults] objectForKey:@"savedPass"];
    NSString *email=[[NSUserDefaults standardUserDefaults] objectForKey:@"savedEmail"];

    if ([urlString isEqualToString:@"ForgotPassword"]) {
        fullURL = @"https://stage.tools4trade.co.uk/customer/account/forgotpassword/";
    }
    
    if ([urlString isEqualToString:@"CustomerService"]) {
        fullURL = @"http://demo.savishops.com/service/";
    }
    
    if ([urlString isEqualToString:@"ContactUs"]) {
        fullURL = @"http://stage.tools4trade.co.uk/contact-us/";
    }
    if ([urlString isEqualToString:@"FAQ"]) {
        fullURL = @"http://stage.tools4trade.co.uk/faqs";
    }
    if([urlString isEqualToString:@"ProductAddToCart"]){
        fullURL = @"http://stage.tools4trade.co.uk/Webservices_soap_updated.php/?method=viewproduct&user=navjeet.matridtech@gmail.com&pass=Test@123";

    }if([urlString isEqualToString:@"view_Wishlist"]){
        NSString *wishlisturl=@"http://stage.tools4trade.co.uk/T4T-mage-api.php?method=viewWishList&user=";
        NSString *passswrdstring=@"&pass=";
        NSString *mUrlstring=[NSString stringWithFormat:@"%@%@%@",wishlisturl,passswrdstring,passwrd];
        fullURL=mUrlstring;
    }
    if([urlString isEqualToString:@"view_cart"]){
       // NSString *mUrl=@"http://stage.tools4trade.co.uk/Webservices_soap_updated.php/?method=viewproduct&user=";
        NSString *mUrl=@"http://stage.tools4trade.co.uk/T4T-mage-api.php?method=LoadCart&cart_id=";
        NSString *passswrdstring=@"&pass=";
        NSString *mcartid=[[NSUserDefaults standardUserDefaults] objectForKey:@"cart_id"];

        NSString *entireURL = [NSString stringWithFormat:@"%@%@", mUrl, mcartid];
        NSLog(@"weburl-viewcart--%@",entireURL);
        fullURL = entireURL;

    }
 
    if ([urlString isEqualToString:@"view_cart_Anonymous"]) {
      //  NSString *mUrl2=@"http://stage.tools4trade.co.uk/Webservices_soap_updated.php/?method=LoadCart&cart_id=";
        NSString *mUrl2=@"http://stage.tools4trade.co.uk/T4T-mage-api.php?method=LoadCart&cart_id=";

        NSString *mcartid=[[NSUserDefaults standardUserDefaults] objectForKey:@"cart_id"];
        NSString *cmpleteUrl2=[NSString stringWithFormat:@"%@%@",mUrl2,mcartid];
        NSLog(@"weburl-anonymouse viewcart--%@",cmpleteUrl2);
        fullURL=cmpleteUrl2;
    }
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground=YES;
    hud.mode = MBProgressHUDModeIndeterminate;
    //load webview url
    NSString *urlWithDeviceType = [fullURL stringByAppendingString:deviceType];
    NSURL *url = [NSURL URLWithString:urlWithDeviceType];
    NSURLRequest *Request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    [[NSURLCache sharedURLCache] removeCachedResponseForRequest:Request];
    
    [_webView loadRequest:Request];
    [_webView setDelegate:self];

    self.edgesForExtendedLayout = UIRectEdgeNone;
    // Do any additional setup after loading the view from its nib.
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType
{
    BOOL isExternal = FALSE;
    BOOL isAllow = TRUE;
    NSString *urlStr = request.URL.absoluteString;
    //if the url begins with 'newtab:'
    if([urlStr hasPrefix:@"newtab:"]){
        isExternal = TRUE;
        //while newtab: is at the start of the URL
        while([urlStr hasPrefix:@"newtab:"]){
            //remove the 'newtab:' from the start of the url
            urlStr = [urlStr substringFromIndex:7];
        }
    }else{
        //url does NOT begin with 'newtab:'...
        //if the url request doesn't start with anchor tag
        if (![urlStr hasPrefix:@"#"]){
            //if the url request doesn't start with the webview url
            if(![urlStr hasPrefix:fullURL]){
                //if the link is not a phone number
                if(![urlStr hasPrefix:@"tel:"]){
                    //if the link is not a mailto link
                    if(![urlStr hasPrefix:@"mailto:"]){
                        //then this link should be opened in a separate view
                        isExternal = TRUE;
                    }
                }
            }
        }
    }
    
    return isAllow;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
