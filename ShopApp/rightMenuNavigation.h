//
//  rightMenuNavigation.h
//  mIracleNoodle
//
//  Created by Matrid on 05/04/16.
//  Copyright © 2016 Matrid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface rightMenuNavigation : UIView <UITableViewDataSource, UITableViewDelegate>
{
    NSArray *arrMenuList,*arrImageMenu;
    UIButton *btnHelloUserName;
    UIViewController *curView;
}
@property (strong, nonatomic) UITableView *tblNavigationMenu;
@property (strong, nonatomic) UIButton *btnBackground;
@property (strong, nonatomic) UIView *viewMenuHeader;
@property (strong, nonatomic) UILabel *lblBlank;

@end
