//
//  ProductDetailViewController.m
//  ShopApp
//
//  Created by Matrid on 10/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "Constant.h"

@interface ProductDetailViewController () {
    ProductDetailModel *prdctModel;
    UILabel *labelToGetSize;
    NSInteger colorImagesCount;
    NSInteger sizeCount;
    NSInteger selectedColor;
    NSInteger selectedSize;
    BOOL loadContent;
}

@end

@implementation ProductDetailViewController
{
    NSInteger nextImage;
}
@synthesize product_Id;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    nextImage=-1;
    
    selectedColor = -1;
    
    selectedSize = -1;
    
    loadContent = NO;
    
    prdctModel = [ProductDetailModel new];
    
    attribute_idColor = @"";
    value_indexColor = @"";
    
    attribute_idSize = @"";
    value_indexSize = @"";
    
    //self.view.backgroundColor = [UIColor whiteColor];
    
    labelToGetSize = [[UILabel alloc] initWithFrame:CGRectMake(8, 62, 274, 23)];
    [labelToGetSize setFont:[UIFont systemFontOfSize:9]];
    [Global customHeader:self headerTitle:@"Tools4Trade" itemFound:NO hideBackButton:NO hideSearchButton:NO hideNotificationButton:NO];
    
    _btnAddToBag.titleLabel.font = [UIFont fontWithName:gmFont size:15];
    
    [self GetProductRating];
    [self GetProductDetail];
    [self GetRelatedProducts];
    
    [_colProductDetail registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    [_colProductDetail registerNib:[UINib nibWithNibName:@"ProductDetailCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
    
    [_colProductColorImg registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    [_colProductColorImg registerNib:[UINib nibWithNibName:@"productColorImgCell" bundle:nil] forCellWithReuseIdentifier:@"cell1"];
    
    [_colProductSize registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    [_colProductSize registerNib:[UINib nibWithNibName:@"productSizeCell" bundle:nil] forCellWithReuseIdentifier:@"Cell3"];
    
//    [_colProductSize registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
//    [_colProductSize registerNib:[UINib nibWithNibName:@"relatedPrdctImageCell" bundle:nil] forCellWithReuseIdentifier:@"cells"];
    
    [_colRelatedProduct registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    [_colRelatedProduct registerNib:[UINib nibWithNibName:@"relatedPrdctImageCell" bundle:nil] forCellWithReuseIdentifier:@"cell4"];
    
    //_scrollViewMainScreen.contentSize=CGSizeMake(320, 2000);
     self.edgesForExtendedLayout = UIRectEdgeNone;
    
    //loading description in webview
   // NSString *mUrl=@"http://stage.tools4trade.co.uk/Webservices_soap_updated.php?method=ProductDescription&product_id=";
    NSString *mUrl=@"http://stage.tools4trade.co.uk/T4T-mage-api.php?method=ProductDescription&product_id=";

    NSString *productID=product_Id;
    NSString *completeUrl=[mUrl stringByAppendingString:product_Id];
    NSURL *url=[NSURL URLWithString:completeUrl];
    NSURLRequest *urlrequest=[NSURLRequest requestWithURL:url];
    [_mwebview_productdetail loadRequest:urlrequest];
    self.mwebview_productdetail.scalesPageToFit = NO;
    //..............................

}

#pragma Webview Delegate

- (void)webViewDidStartLoad:(UIWebView *)webView {
    activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activity.center = _mwebview_productdetail.center;
    activity.hidesWhenStopped = YES;
    [activity startAnimating];
    [_mwebview_productdetail addSubview:activity];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    if (webView == self.mwebview_productdetail) {
//        CGFloat height = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"] floatValue];
        self.mwebview_productdetail.frame = CGRectMake(webView.frame.origin.x, webView.frame.origin.y, webView.frame.size.width, webView.scrollView.contentSize.height);
        [_tblDetailView reloadData];
    }
    [activity stopAnimating];
//    
//NSString *string =[_mwebview_productdetail stringByEvaluatingJavaScriptFromString:@"document.getElementById(\"body\").offsetHeight;"];
//    CGFloat height = [string floatValue] + 8;
//    CGRect frame = [_mwebview_productdetail frame];
//    frame.size.height = height;
//    [_mwebview_productdetail setFrame:frame];
    
   // if ([[self.view] respondsToSelector:@selector(webViewController:webViewDidResize:)]){
      //  [[self delegate] webViewController:self webViewDidResize:frame.size];}
}

#pragma TableView Delegates and DataSources

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0)
        return 300 + self.mwebview_productdetail.frame.size.height;
    
    else if (indexPath.row == 1)
        return 35;
    
    else if (indexPath.row == 2)
        return 55;
    
    else if (indexPath.row == 3) {
        
        labelToGetSize.text = prdctModel.description;
        if (loadContent) {
            labelToGetSize.text = prdctModel.specification;
        }
        CGRect frame = [Global label:labelToGetSize];
        return 99+frame.size.height;
    }
    else if (indexPath.row == 4)
        if (prdctModel.related_products.count != 0) {
            return 220;
        }
        //return 0;

    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"CellIdentifier";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    if (indexPath.row == 0) {
        [cell addSubview:_viewProductHeader];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.row == 1){
       [cell addSubview:_colProductColorImg];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.row == 2){
        [cell.contentView addSubview:_viewProductSize];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    if (indexPath.row == 3) {
       
        if (prdctModel.specification.length == 0 || prdctModel.description.length == 0 ) {
            return cell;
        }
        else
        {
             productDetailDescriptionCell *cell1 = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (cell1 == nil) {
                NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"productDetailDescriptionCell" owner:self options:nil];
                cell1 = [array objectAtIndex:0];
            }
            
            labelToGetSize.text = prdctModel.description;
            if (loadContent) {
                labelToGetSize.text = prdctModel.specification;
            }
            CGRect frame = [Global label:labelToGetSize];
            cell1.lblContent.frame = frame;
            
            CGRect frame1 = cell1.viewContainer.frame;
            frame1.size.height = frame1.size.height + frame.size.height;
            cell1.viewContainer.frame = frame1;
            
            cell1.viewContainer.layer.borderColor = [[UIColor colorWithRed:215/255.0 green:215/255.0 blue:215/255.0 alpha:1.0] CGColor];
            cell1.viewContainer.layer.borderWidth = 1;
            cell1.viewContainer.layer.cornerRadius = 3;
            cell1.viewContainer.clipsToBounds = YES;
            
            cell1.btnDescription.layer.borderColor = [[UIColor colorWithRed:215/255.0 green:215/255.0 blue:215/255.0 alpha:1.0] CGColor];
            cell1.btnDescription.layer.borderWidth = 1;
            
            cell1.btnSpecification.layer.borderColor = [[UIColor colorWithRed:215/255.0 green:215/255.0 blue:215/255.0 alpha:1.0] CGColor];
            cell1.btnSpecification.layer.borderWidth = 1;
            
            [cell1.btnDescription addTarget:self action:@selector(btnDescription) forControlEvents:UIControlEventTouchUpInside];
            [cell1.btnSpecification addTarget:self action:@selector(btnSpecification) forControlEvents:UIControlEventTouchUpInside];
            
            if (loadContent) {
                cell1.lblContent.text = prdctModel.specification;
                [cell1.btnDescription setTitleColor:[UIColor colorWithRed:85/255.0 green:85/255.0 blue:85/255.0 alpha:1.0] forState:normal];
                [cell1.btnSpecification setTitleColor:[UIColor colorWithRed:130/255.0 green:199/255.0 blue:132/255.0 alpha:1.0] forState:normal];
            }
            else {
                cell1.lblContent.text = prdctModel.description;
                [cell1.btnSpecification setTitleColor:[UIColor colorWithRed:85/255.0 green:85/255.0 blue:85/255.0 alpha:1.0] forState:normal];
                [cell1.btnDescription setTitleColor:[UIColor colorWithRed:130/255.0 green:199/255.0 blue:132/255.0 alpha:1.0] forState:normal];
            }
            
            [cell1.lblContent sizeToFit];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell1;
        }
        
    }
    
    _viewRelatedProduct.layer.borderColor = [[UIColor colorWithRed:215/255.0 green:215/255.0 blue:215/255.0 alpha:1.0] CGColor];
    _viewRelatedProduct.layer.borderWidth = 1;
    _viewRelatedProduct.layer.cornerRadius = 3;
    _viewRelatedProduct.clipsToBounds = YES;
    
    [cell addSubview:_viewRelatedProduct];
    _viewRelatedProduct.center = cell.center;
    
    CGRect frame = _viewRelatedProduct.frame;
    frame.origin.y = 0;
    _viewRelatedProduct.frame = frame;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell; 
}

- (void)btnDescription {
    loadContent = NO;
    [_tblDetailView reloadData];
}

- (void)btnSpecification {
    loadContent = YES;
    [_tblDetailView reloadData];
}

/*************************************************** TABLEVIEW END ***************************************************/



- (IBAction)didChangeValue:(HCSStarRatingView *)sender {
    NSLog(@"Changed rating to %.1f", sender.value);
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView ==_colProductDetail)
         return prdctModel.productImages.count;
    
    else if (collectionView == _colProductSize) {
        return sizeCount;
    }
    else if (collectionView == _colProductColorImg) {
        return colorImagesCount;
    }
    else if (collectionView == _colRelatedProduct) {
        return prdctModel.related_products.count;
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView == _colRelatedProduct) {
        relatedPrdctImageCell *cell12 = (relatedPrdctImageCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell4" forIndexPath:indexPath];
        
        UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        activity.center =cell12.imgProduct.center;
        activity.hidesWhenStopped = YES;
        [activity startAnimating];
        [cell12 addSubview:activity];
        
        cell12.lblProduct.text = [prdctModel.related_products[indexPath.row] valueForKey:@"name"];
        
        NSString *url = @"";
        
        url = [prdctModel.related_products[indexPath.row] valueForKey:@"images"];
        cell12.imgProduct.contentMode = UIViewContentModeScaleAspectFit;
        [cell12.imgProduct sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            // Get rid of the activity indicator when the image has been loaded
            [activity stopAnimating];
        }];
        
        return cell12;
    }
    
    if (collectionView == _colProductSize) {
        productSizeCell *cell = (productSizeCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell3" forIndexPath:indexPath];
        NSString *size = [[prdctModel.optionSize objectAtIndex:indexPath.row] valueForKey:@"default_label"];
        cell.lblSize.text = size;
        if (selectedSize == indexPath.row) {
            cell.lblSize.layer.borderWidth=1;
            [cell.lblSize setTextColor:[UIColor colorWithRed:130/255.0 green:199/255.0 blue:132/255.0 alpha:1.0]];
            cell.lblSize.layer.borderColor=[[UIColor colorWithRed:130/255.0 green:199/255.0 blue:132/255.0 alpha:1.0] CGColor];
        }
        else {
            cell.lblSize.layer.borderColor=[[UIColor lightGrayColor] CGColor];
            cell.lblSize.textColor grayColor;
            cell.lblSize.layer.borderWidth=1;
        }
        
        cell.lblSize.layer.cornerRadius=5;
        cell.lblSize.clipsToBounds=YES;
    
        return cell;
    }
    
    if (collectionView == _colProductColorImg) {
        productColorImgCell *cell = (productColorImgCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell1" forIndexPath:indexPath];
        UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        activity.center =cell.imgProductSizeImage.center;
        activity.hidesWhenStopped = YES;
        [activity startAnimating];
        [cell addSubview:activity];
        
        NSString *url = [[prdctModel.optionColor objectAtIndex:indexPath.row] valueForKey:@"color_image"];
        

        [cell.imgProductSizeImage sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            // Get rid of the activity indicator when the image has been loaded
            [activity stopAnimating];
        }];
        
        if (selectedColor == indexPath.row) {
            cell.imgCircleTick.image = [UIImage imageNamed:@"tick1"];
            
        }
        else
            cell.imgCircleTick.image = nil;
        
        cell.imgProductSizeImage.layer.cornerRadius=cell.imgProductSizeImage.frame.size.width/2;
        cell.imgProductSizeImage.clipsToBounds=YES;
        cell.imgProductSizeImage.contentMode=UIViewContentModeScaleAspectFit;
        
        
        return cell;

    }
    
    ProductDetailCollectionViewCell *cell = (ProductDetailCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activity.center =cell.imgProductDetailCell.center;
    activity.hidesWhenStopped = YES;
    [activity startAnimating];
    [cell addSubview:activity];
    
    NSString *url = [prdctModel.url stringByAppendingString:[prdctModel.productImages[indexPath.row] valueForKey:@"file"]];
    
    [cell.imgProductDetailCell sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
        // Get rid of the activity indicator when the image has been loaded
        [activity stopAnimating];
    }];
    cell.imgProductDetailCell.contentMode=UIViewContentModeScaleAspectFit;
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == _colProductColorImg) {
        NSLog(@"Index col.>>%ld",(long)indexPath.row);
        selectedColor = indexPath.row;
        attribute_idColor = [[prdctModel.optionColor objectAtIndex:indexPath.row] valueForKey:@"attribute_id"];
        value_indexColor = [[prdctModel.optionColor objectAtIndex:indexPath.row] valueForKey:@"value_index"];
        [_colProductColorImg reloadData];
    }
    else if (collectionView == _colProductSize) {
        NSLog(@"Index col.>>%ld",(long)indexPath.row);
        attribute_idSize = [[prdctModel.optionSize objectAtIndex:indexPath.row] valueForKey:@"attribute_id"];
        value_indexSize = [[prdctModel.optionSize objectAtIndex:indexPath.row] valueForKey:@"value_index"];
        
        selectedSize = indexPath.row;
        [_colProductSize reloadData];
    }
   // if ([[[arrCataogory objectAtIndex:indexPath.row] valueForKey:@"children"] count]==0) {
}

-(void)GetProductRating {
    NSString *strPrdId = product_Id;
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:strPrdId,@"product_id", nil];
    [Global urlString:[apiDomain stringByAppendingString:apiUrlProductRating] parameter:dic indicator:NO currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response) {
        if(result) { //DATA_MISSING
            if ([[response valueForKey:@"status"] isEqualToString:@"DATA_MISSING"]) {
                [Global alertAdd:@"No data found" withTitle:@"No Data" navBack:NO];
            }
            else{
                prdctModel.rating = [response valueForKey:@"rating"];
                prdctModel.userCount = [response valueForKey:@"user_count"];
                
                double strAverage = (CGFloat)[prdctModel.rating floatValue];
                
                double rating = ceilf(strAverage / 20);
                
                _starRatingView.value=rating;
                
                _starRatingView.enabled=NO;
                _lblProductRating.text=[NSString stringWithFormat:@"%@%@%@",@"(",prdctModel.userCount,@")"];
            }
        }
    }];
}
-(void)GetProductDetail {
    
    NSString *strPrdId = product_Id;
   NSString *customer_Id = [[NSUserDefaults standardUserDefaults] objectForKey:@"Uid"];
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:strPrdId,@"product_id",customer_Id,@"customer_id", nil];
    [Global urlString:apiUrlProductDetailWithProductId parameter:dic indicator:YES currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response) {
        if(result) { //DATA_MISSING
            if ([[response valueForKey:@"status"] isEqualToString:@"DATA_MISSING"]) {
                [Global alertAdd:@"No data found" withTitle:@"No Data" navBack:NO];
                
            }
            else {
                if ([[response valueForKey:@"flag_cart"] integerValue] == 0) {
                    [_btnAddToBag setTitle:@"ADD TO BAG" forState:UIControlStateNormal];
                }
                if ([[response valueForKey:@"flag_cart"] integerValue] == 1) {
                    [_btnAddToBag setTitle:@"View Cart" forState:UIControlStateNormal];
                }
                
                _viewProductHeader.hidden = NO;
                _btnAddToBag.hidden = NO;
                response = [Global stripNullFromResponse:[response mutableCopy]];
                prdctModel.name = [response valueForKey:@"name"];
                prdctModel.sku = [response valueForKey:@"sku"];
                prdctModel.current_price = [response valueForKey:@"price"];
                prdctModel.description = [response valueForKey:@"description"];
                prdctModel.specification = [response valueForKey:@"sepcification"];
                prdctModel.currency = [response valueForKey:@"currency"];
                prdctModel.url = [response valueForKey:@"url"];
                prdctModel.productImages = [[response valueForKey:@"images"] mutableCopy];
               // prdctModel.rating = [[response valueForKey:@"rating"] mutableCopy];
               // prdctModel.related_products = [[response valueForKey:@"related_products"] mutableCopy];
                prdctModel.optionColor = [[[response valueForKey:@"options"] valueForKey:@"color"] mutableCopy];
                prdctModel.optionSize = [[[response valueForKey:@"options"] valueForKey:@"size"] mutableCopy];
                
                colorImagesCount = prdctModel.optionColor.count;
                sizeCount = prdctModel.optionSize.count;
                
                if (sizeCount != 0) {
                    _viewProductSize.hidden = NO;
                }
                
                CGRect frame=_colProductColorImg.frame;
                CGFloat width=colorImagesCount*30;
                frame.origin.x=(self.view.frame.size.width-width)/2;
                frame.size.width=width;
                _colProductColorImg.frame=frame;
                
                
                frame=_colProductSize.frame;
                width=colorImagesCount*40;
                frame.origin.x=(self.view.frame.size.width-width)/2;
                frame.size.width=width;
                _colProductSize.frame=frame;
                
               // _colProductColorImg.backgroundColor=[UIColor orangeColor];
            
                [_colProductDetail reloadData];
                [_colProductSize reloadData];
                [_colProductColorImg reloadData];
               // _scrollViewMainScreen.hidden=NO;
                [_tblDetailView reloadData];
                
                _lblProductName.text = prdctModel.name;
                _lblProductPrice.text=[Global PriceRoundOffToString:prdctModel.current_price currency:prdctModel.currency];

            }
        }
    }];
}

-(void)GetRelatedProducts {
    NSString *strPrdId = product_Id;
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:strPrdId,@"id", nil];
    [Global urlString:[apiDomain stringByAppendingString:apiUrlProductRelated] parameter:dic indicator:NO currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response) {
        if(result) { //DATA_MISSING
            if ([[response valueForKey:@"status"] isEqualToString:@"SUCCESS"]) {
                prdctModel.related_products = [[response valueForKey:@"related_product"] mutableCopy];
                if (prdctModel.related_products.count != 0) {
                    _viewRelatedProduct.hidden = NO;
                    [_colRelatedProduct reloadData];
                }
            }
        }
    }];
}

- (IBAction)btnRightClick:(id)sender {
    NSIndexPath *indexPath;
    for (UICollectionViewCell *cell in [_colProductDetail visibleCells]) {
        indexPath = [_colProductDetail indexPathForCell:cell];
        nextImage=indexPath.row;
    }
    if (nextImage<prdctModel.productImages.count-1) {
        nextImage++;
        NSLog(@"count == %li",(long)nextImage);
        NSIndexPath *path = [NSIndexPath indexPathForRow:nextImage inSection:0];
        [_colProductDetail scrollToItemAtIndexPath:path atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];//paas index here to move to.
        nextImage=-1;
    }
    else {
        NSLog(@"No more items");
        nextImage=-1;
    }
    
}
- (IBAction)btnLeftClick:(id)sender {
    NSIndexPath *indexPath;
    for (UICollectionViewCell *cell in [_colProductDetail visibleCells]) {
        indexPath = [_colProductDetail indexPathForCell:cell];
        nextImage=indexPath.row;
    }
    if (nextImage>0) {
        nextImage--;
        NSIndexPath *path = [NSIndexPath indexPathForRow:nextImage inSection:0];
        [_colProductDetail scrollToItemAtIndexPath:path atScrollPosition:UICollectionViewScrollPositionRight animated:YES];//paas index here to move to.
        nextImage=-1;
    }
    else
    {
        NSLog(@"No more items");
        nextImage=-1;
    }
}

- (IBAction)btnAddToBagClick:(id)sender {
    
    [self CreateCart];
}

-(void)CreateCart {
    NSString *strCustomerId = [[NSUserDefaults standardUserDefaults] objectForKey:@"Uid"];
    NSString *passwrd=[[NSUserDefaults standardUserDefaults] objectForKey:@"savedPass"];
    NSString *email=[[NSUserDefaults standardUserDefaults] objectForKey:@"savedEmail"];
    if ([_btnAddToBag.currentTitle isEqualToString:@"View Cart"]) {
        //CartViewController *cart = [[CartViewController alloc]init];
        //cart.cartId = cartId;
        //[self.navigationController pushViewController:cart animated:YES];
        WebViewController *web = [[WebViewController alloc] init];
 
        if (strCustomerId==nil) {
            /* NSString *mUrl2=@"http://stage.tools4trade.co.uk/Webservices_soap_navjeet.php?method=LoadCart&cart_id=";
             NSString *mcartid=[[NSUserDefaults standardUserDefaults] objectForKey:@"cart_id"];
             NSString *cmpleteUrl2=[NSString stringWithFormat:@"%@%@",mUrl2,mcartid];
             NSLog(@"weburl-cart",cmpleteUrl2);*/
            
            web.urlString = @"view_cart_Anonymous";
            
            
        }else{
            
            /* NSString *mUrl=@"http://stage.tools4trade.co.uk/Webservices_soap_updated.php/?method=viewproduct&user=";
             NSString *passswrdstring=@"&pass=";
             NSString *entireURL = [NSString stringWithFormat:@"%@%@%@%@", mUrl, email, passswrdstring, passwrd];
             NSLog(@"weburl-cart",entireURL);*/
            web.urlString = @"view_cart";

        }
        
        //open web view
        
        
        [self.navigationController pushViewController:web animated:YES];
        
        
        
        
        
        
    }
    else {
        
        if (prdctModel.optionColor.count > 0) {
            if ([attribute_idColor isEqualToString:@""]) {
                [Global alertAdd:@"Please Select 'Color'" withTitle:@"Select Color" navBack:NO];
                return;
            }
        }
        if (prdctModel.optionSize.count > 0) {
            if ([attribute_idSize isEqualToString:@""]) {
                [Global alertAdd:@"Please Select 'Size'" withTitle:@"Select Size" navBack:NO];
                return;
            }
        }

        
       //cif (strCustomerId != nil) {[Global alertAdd:@"Please Login to add to cart" withTitle:nil navBack:NO];}else {
           /* NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:strCustomerId,@"customerId", strCustomerId,nil];
      //  NSLog(@"mmurl", [apiDomain stringByAppendingString:apiUrlCreateCart]);

            [Global urlString:[apiDomain stringByAppendingString:apiUrlCreateCart] parameter:nil indicator:YES currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response) {

                if(result) { //DATA_MISSING
                    if ([[response valueForKey:@"status"] isEqualToString:@"DATA_MISSING"]) {
                        [Global alertAdd:@"Not added in bag" withTitle:@"No Data" navBack:NO];
                    }
                    else if ([[response valueForKey:@"status"] isEqualToString:@"SUCCESS"]) {
                        
                        
                        NSString *options = @"";
                        if (![attribute_idColor isEqualToString:@""] && ! [attribute_idSize isEqualToString:@""]) {
                            options = [NSString stringWithFormat:@"%@%@%@%@%@%@%@",attribute_idColor,@":",value_indexColor,@";",attribute_idSize,@":",value_indexSize];
                        }
                        else if (![attribute_idColor isEqualToString:@""])
                        {
                            options = [NSString stringWithFormat:@"%@%@%@",attribute_idColor,@":",value_indexColor];
                        }
                        else if (![attribute_idSize isEqualToString:@""])
                        {
                            options = [NSString stringWithFormat:@"%@%@%@",attribute_idSize,@":",value_indexSize];
                        }
                        else
                        {
                            options = @"";
                        }
                        cartId = [response valueForKey:@"cart_id"];*/
        if (strCustomerId==nil) {
            NSString *mCart_id= [[NSUserDefaults standardUserDefaults] objectForKey:@"cart_id"];
            if (mCart_id==nil) {
mCart_id=@"0";
            }
            
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"1",@"qty",product_Id,@"product_id",mCart_id,@"cart_id",nil];
            // dictionaryWithObjectsAndKeys:cartId,@"quoteId",@"1",@"qty",options,@"options",prdctModel.sku,@"sku" ,nil];
            NSLog(@"%@", dic);
            [Global urlString:apiUrlAddtoCartAnonymous parameter:dic indicator:YES currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response) {
                if(result) { //DATA_MISSING
                    NSLog(@"addto cart response:-%@",response);
                    if ([[response valueForKey:@"status"] isEqualToString:@"Out Of stock"]) {
                        [Global alertAdd:@"Product not found" withTitle:@"Out of stock" navBack:NO];
                    }else if([[response valueForKey:@"status"]isEqualToString:@"FAILED"]){
                        [Global alertAdd:@"Product not found" withTitle:@"No result" navBack:NO];
                    }
                    else if ([[response valueForKey:@"status"] isEqualToString:@"SUCCESS"]) {
                        status = YES;
                        if (status) {
                            [[NSUserDefaults standardUserDefaults] setValue:[response valueForKey:@"cart_id"] forKey:@"cart_id"];
                            
                            [Global alertAdd:@"Product Added to Cart Successfully" withTitle:@"Product Added" navBack:NO];
                            //[Global alertAdd:@"Product Added to Cart Successfully" withTitle:@"Product Added" navBack:NO];
                            
                            [UIView transitionWithView:_btnAddToBag duration:2 options:(UIViewAnimationOptionTransitionFlipFromBottom) animations:^{
                                
                                [_btnAddToBag setTitle:@"View Cart" forState:UIControlStateNormal];
                                
                            } completion:^(BOOL finished) {
                                
                                
                            }];
                        }
                        
                        //[Global alertAdd:@"Product added to cart successfully" withTitle:nil navBack:NO];
                    }
                    
                }
            }];

            
            
                        
                        //[Global alertAdd:@"Product added to cart successfully" withTitle:nil navBack:NO];
        

        }else{
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:email,@"user" ,passwrd,@"pass",@"1",@"qty",product_Id,@"product_id",nil];
            // dictionaryWithObjectsAndKeys:cartId,@"quoteId",@"1",@"qty",options,@"options",prdctModel.sku,@"sku" ,nil];
            [Global urlString:apiUrlAddToCart parameter:dic indicator:YES currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response) {
                if(result) { //DATA_MISSING
                    NSLog(@"addto cart response:-%@",response);
                    if ([[response valueForKey:@"status"] isEqualToString:@"FAILED"]) {
                        [Global alertAdd:@"No data found" withTitle:@"No Data" navBack:NO];
                    }
                    else if([[response valueForKey:@"status"] isEqualToString:@"Out Of stock"]){
                        [Global alertAdd:@"Product not available" withTitle:@"Out of stock" navBack:NO];
                    }
                    else if ([[response valueForKey:@"status"] isEqualToString:@"SUCCESS"]) {
                        status = YES;
                        if (status) {
                            
                            [Global alertAdd:@"Product Added to Cart Successfully" withTitle:@"Product Added" navBack:NO];
                            [[NSUserDefaults standardUserDefaults] setValue:[response valueForKey:@"cart_id"] forKey:@"cart_id"];
                            //[Global alertAdd:@"Product Added to Cart Successfully" withTitle:@"Product Added" navBack:NO];
                            
                            [UIView transitionWithView:_btnAddToBag duration:2 options:(UIViewAnimationOptionTransitionFlipFromBottom) animations:^{
                                
                                [_btnAddToBag setTitle:@"View Cart" forState:UIControlStateNormal];
                                
                            } completion:^(BOOL finished) {
                                
                                
                            }];

                   }
                    }
                }
                
            }];
        }
        
    
        }
    
}


@end
