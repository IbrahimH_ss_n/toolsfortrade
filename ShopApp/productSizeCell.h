//
//  productSizeCell.h
//  ShopApp
//
//  Created by Matrid on 17/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface productSizeCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblSize;

@end
