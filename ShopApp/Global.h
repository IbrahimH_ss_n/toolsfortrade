//
//  Global.h
//  ShopApp
//
//  Created by Matrid on 27/12/16.
//  Copyright © 2016 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Global : UIView

+ (BOOL)loginFor:(NSString *)login;

+ (void)addSwitch:(BOOL)on frame:(CGRect)frame;

+ (BOOL)validateEmailWithString:(NSString*)email;

+ (BOOL)validatePasswordWithString:(NSString*)password;

+ (void)alertAdd:(NSString*)message withTitle:(NSString *)title navBack:(BOOL)flag;

+ (void)urlString:(NSString *)url parameter:(NSDictionary *)dicty indicator:(BOOL)indi currentView:(UIView *)viewCon Get_Post:(NSString *)method completion:(void ( ^ ) ( BOOL result , NSDictionary *response ))completionHandler;
//+(void)getFilterDataFromUrlString:(NSString *)url parameters:(NSDictionary *)params currentView: (UIView *)currentView completion:(void (^) (BOOL result, NSDictionary *response))completionHandler {

+ (void) getFilterDataFromURLString:(NSString *)url parameters: (NSDictionary *)params currentView : (UIView *)currentView completion: (void ( ^ ) (BOOL result, NSDictionary *response))completionHandler;

+ (void)customHeader:(UIViewController *)views headerTitle:(NSString *)headerTitle itemFound:(BOOL)itemFound hideBackButton:(BOOL)hideBackButton hideSearchButton:(BOOL)hideSearchButton hideNotificationButton:(BOOL)hideNotificationButton ;

+ (void)customFooter:(UIView *)currentView;

+ (void)navigationViewOpen;

+ (void)hideNavigation;

+ (CGRect)label:(UILabel *)label;

+ (BOOL)testInternetConnection;

+ (void)pickerShow:(UIView *)view;

+ (void)addErrorView:(UIViewController *)view errorMessage:(NSString *)errorMessage;

+ (NSString*)PriceRoundOffToString:(NSString*)stringToRoundOff currency:(NSString *)cury;

+(NSMutableDictionary *)stripNullFromResponse:(NSMutableDictionary *)dictionary;
//+(UIActivityIndicatorView*)loadingIndicatorStart;

@end
