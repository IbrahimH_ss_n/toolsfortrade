//
//  LoginViewController.h
//  ShopApp
//
//  Created by Matrid on 06/01/17.
//  Copyright © 2017 Test. All rights reserved.
//
//  LocalizationSetLanguage(Language);


#import <UIKit/UIKit.h>
#import "Constant.h"
#import <Google/SignIn.h>
#import "customSwitch.h"



@interface LoginViewController : UIViewController <GIDSignInUIDelegate, SwitchDelegate>

@property (strong, nonatomic) IBOutlet NSString *loginFrom;

@property (strong, nonatomic) IBOutlet UITextField *txtEmail;

@property (strong, nonatomic) IBOutlet UITextField *txtPassword;

- (IBAction)btnLogin:(id)sender;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIButton *btnLogin;

- (IBAction)btnForgotSubmit:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnForgotSubmit;

@property (weak, nonatomic) IBOutlet UIView *viewForgotPassword;

@property (weak, nonatomic) IBOutlet UIView *viewLogin;

@property (weak, nonatomic) IBOutlet UIButton *btnForgotPassword;

- (IBAction)btnForgotPassword:(id)sender;

- (IBAction)btnSignUpOnLoginClicked:(id)sender;

@end
