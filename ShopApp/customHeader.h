//
//  customHeader.h
//  maegntoShopApp
//
//  Created by Matrid on 23/05/16.
//  Copyright © 2016 Matrid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customHeader : UIView
{
    BOOL menuOpen;
}
@property (strong, nonatomic) UILabel *lblHeaderTitle;
@property (strong, nonatomic) UILabel *lblItemFound;
@property (strong, nonatomic) UILabel *lblNotification;
@property (strong, nonatomic) UIButton *btnBack;
@property (strong, nonatomic) UIButton *btnNav;
@property (strong, nonatomic) UIButton *btnNotification;
@property (strong, nonatomic) UIButton *btnSearch;
@property (strong, nonatomic) UITextField *txtSearch;


@property (strong, nonatomic) NSString *strTitle;


@property (strong,nonatomic) UIImageView *mTitle;
- (void)setView;

@end
