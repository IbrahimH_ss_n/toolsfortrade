//
//  YourOderTableViewCell.h
//  ShopApp
//
//  Created by Matrid on 02/05/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface YourOderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblOrderDate;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblShippedTo;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnViewDetails;
@property (weak, nonatomic) IBOutlet UIView *viewOrderDetail;

@end
