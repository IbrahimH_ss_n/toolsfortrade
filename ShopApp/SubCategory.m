//
//  SubCategory.m
//  ShopApp
//
//  Created by Md Ibrahim Hassan on 26/04/18.
//  Copyright © 2018 Test. All rights reserved.
//

#import "SubCategory.h"
#import "Utility.h"

@implementation SubCategory
-(id) initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super init]) {
        NSString *label = [dictionary valueForKey:@"label"];
        if (![Utility isNullOrEmpty:label]) {
            self.label = label;
        }
        NSString *value = [dictionary valueForKey:@"value"];
        if (![Utility isNullOrEmpty:value]) {
            self.value = value;
        }
        NSNumber *count = [dictionary valueForKey:@"count"];
        if (![Utility isNullOrEmpty:value]) {
            self.countVal = count;
        }
    }
    return self;
}
@end
