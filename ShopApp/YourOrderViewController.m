//
//  YourOrderViewController.m
//  ShopApp
//
//  Created by Matrid on 02/05/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import "YourOrderViewController.h"

@interface YourOrderViewController ()

@end

@implementation YourOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    [Global customHeader:self headerTitle:@"Tools4Trade" itemFound:NO hideBackButton:NO hideSearchButton:NO hideNotificationButton:NO];
    _lblYourOrder.backgroundColor screenBackgroundColor;
    _lblYourOrder.font = [UIFont fontWithName:gmFont size:14];
    _lblYourOrder.text = @"  Your Order";
    [Global customFooter:self.view];
    self.edgesForExtendedLayout = UIRectEdgeNone;

    [self getOrderDetail];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrOrderDetails.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"newFriendCell";
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    YourOderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"YourOderTableViewCell" owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    
    NSString *strOrderDate = [[arrOrderDetails objectAtIndex:indexPath.row] valueForKey:@"created_at"];
    
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    //[dateFormatter setDateFormat:@"MMM dd, yyyy"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString:strOrderDate];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSString *orderDate = [dateFormatter stringFromDate:date];
    
    cell.lblOrderDate.text = orderDate;
    cell.lblOrderNumber.text = [[arrOrderDetails objectAtIndex:indexPath.row] valueForKey:@"increment_id"];
    cell.lblOrderTotal.text = [[arrOrderDetails objectAtIndex:indexPath.row] valueForKey:@"grand_total"];
    
    NSString *strFullName = [NSString stringWithFormat:@"%@ %@",[[arrOrderDetails objectAtIndex:indexPath.row] valueForKey:@"firstname"],[[arrOrderDetails objectAtIndex:indexPath.row] valueForKey:@"firstname"]];
    
    cell.lblShippedTo.text = strFullName;
    cell.lblStatus.text = [[arrOrderDetails objectAtIndex:indexPath.row] valueForKey:@"status"];
    
    cell.viewOrderDetail.layer.cornerRadius = 5;
    cell.viewOrderDetail.layer.borderWidth = 2;
    cell.viewOrderDetail.layer.borderColor = [[UIColor colorWithRed:130/255.0 green:199/255.0 blue:132/255.0 alpha:1.0] CGColor];
    cell.viewOrderDetail.clipsToBounds = YES;
    
    
    cell.btnViewDetails.layer.cornerRadius = 3;
    cell.btnViewDetails.clipsToBounds = YES;
    cell.btnViewDetails.titleLabel.font = [UIFont fontWithName:gmFont size:12];
    [cell.btnViewDetails addTarget:self action:@selector(viewDetails:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnViewDetails.tag=indexPath.row;
    return cell;
}

- (void)viewDetails:(UIButton *)sender
{
    NSInteger tag = sender.tag;

    OrderDetailViewController *order = [[OrderDetailViewController alloc]init];
    order.Order_Id = [[arrOrderDetails objectAtIndex:tag] valueForKey:@"entity_id"];
    [self.navigationController pushViewController:order animated:YES];
}

- (void)getOrderDetail {
    
    NSString *customerId = [[NSUserDefaults standardUserDefaults] valueForKey:@"Uid"];
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:customerId ,@"customer_id", nil];
    
    [Global urlString:[apiDomain stringByAppendingString:apiUrlOrderDetails] parameter:dic indicator:YES currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response) {
        if(result) { //DATA_MISSING
            if ([[response valueForKey:@"status"] isEqualToString:@"SUCCESS"]) {
                arrOrderDetails  = [response valueForKey:@"details"];
                if (arrOrderDetails.count == 0) {
                    //[Global alertAdd:@"'Shopping Cart' is empty" withTitle:@"No Data" navBack:NO];
                    [Global addErrorView:self errorMessage:@"You have not purchased any product yet."];
                }
                else{
                    [_tblOrderDetails reloadData];
                }
            }
            if ([[response valueForKey:@"status"] isEqualToString:@"ERROR"]) {
                [Global addErrorView:self errorMessage:@"Something went wrong"];
            }
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
