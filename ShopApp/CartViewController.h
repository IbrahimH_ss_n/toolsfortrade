//
//  CartViewController.h
//  ShopApp
//
//  Created by Matrid on 30/12/16.
//  Copyright © 2016 Test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface CartViewController : UIViewController
{
    NSString *pId, *wishListItemtId;
    BOOL click;
    NSMutableArray *arrMyCart,*arrNewPrice;
    NSInteger totalItem, totalCost;
}
@property (weak, nonatomic) IBOutlet NSString *cartId;

@property (strong, nonatomic) IBOutlet UICollectionView *colCartItem;
@property (weak, nonatomic) IBOutlet UIButton *btnPlaceOrder;
@property (weak, nonatomic) IBOutlet UILabel *lblItemInCart;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalCost;

@property (strong, nonatomic) IBOutlet UIView *viewHeaderTotal;
@end
