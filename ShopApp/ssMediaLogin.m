
//
//  ssMediaLogin.m
//  mIracleNoodle
//
//  Created by Matrid on 11/02/16.
//  Copyright © 2016 Matrid. All rights reserved.
//

#import "ssMediaLogin.h"
#import "MBProgressHUD/MBProgressHUD.h"
#import "Constant.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@implementation ssMediaLogin

- (void)fbLogin{
    UIViewController *view = [(UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController topViewController];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view.view animated:YES];
    hud.dimBackground=YES;
    hud.mode = MBProgressHUDModeIndeterminate;
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"] fromViewController:view handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            // Process error
            hud.hidden =YES;
        
        } else if (result.isCancelled) {
            // Handle cancellations
        } else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            
            if ([result.grantedPermissions containsObject:@"public_profile"]) {
                if ([FBSDKAccessToken currentAccessToken]) {
                    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"email,name,first_name,last_name"}]
                     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                         [MBProgressHUD hideHUDForView:view.view animated:YES];
                         if (!error) {
                             NSDictionary *notiDict = [[NSDictionary alloc] initWithObjectsAndKeys:[result valueForKey:@"email"],@"email",@"facebook",@"type",[result valueForKey:@"first_name"],@"fistName",[result valueForKey:@"last_name"],@"lastName",[result valueForKey:@"id"],@"id", nil];
                             [[NSNotificationCenter defaultCenter] postNotificationName:@"ssSignup" object:nil userInfo:notiDict];
                             
                         }
                     }];
                }
            }
        }
    }];
}

- (void)twitterLogin {
    [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
        if (session) {
            NSLog(@"signed in as %@", [session userName]);
            TWTRAPIClient *client = [TWTRAPIClient clientWithCurrentUser];
            NSURLRequest *request = [client URLRequestWithMethod:@"GET"
                                                             URL:@"https://api.twitter.com/1.1/account/verify_credentials.json"
                                                      parameters:@{@"include_email": @"true", @"skip_status": @"true"}
                                                           error:nil];
            
            [client sendTwitterRequest:request completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                NSError* error;
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                NSLog(@"%@",json);
            }];
            
//            [[[Twitter sharedInstance] APIClient] loadUserWithID:[session userID] completion:^(TWTRUser *user, NSError *error)  {
//                // handle the response or error
//                if (![error isEqual:nil]) {
//                    NSLog(@"Twitter info  -> user = %@ ",user);
//                    NSString *urlString = [[NSString alloc]initWithString:user.profileImageLargeURL];
//                    NSURL *url = [[NSURL alloc]initWithString:urlString];
//                    NSData *pullTwitterPP = [[NSData alloc]initWithContentsOfURL:url];
//                    UIImage *profImage = [UIImage imageWithData:pullTwitterPP];
//                    [[Singletons singleton].ssLoginData setValue:user.screenName forKey:@"email"];
//                    [[Singletons singleton].ssLoginData setValue:@"twitter" forKey:@"type"];
//                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ssSignup" object:nil];
//                    
//                } else {
//                    NSLog(@"Twitter error getting profile : %@", [error localizedDescription]);
//                }
//            }];
        } else {
            NSLog(@"error: %@", [error localizedDescription]);
        }
    }];
}


- (void)googleLogin {
    _view = [(UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController topViewController];
    [GIDSignIn sharedInstance].uiDelegate = _view.self;
    [[GIDSignIn sharedInstance] signIn];
}

- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    
}


- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController {
    [_view presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController {
    [_view dismissViewControllerAnimated:YES completion:nil];
}



@end
