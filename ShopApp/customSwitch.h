//
//  customSwitch.h
//  ShopApp
//
//  Created by Matrid on 17/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SwitchDelegate <NSObject>

@required

- (void)isOn:(BOOL)flag;

@end


@interface customSwitch : UIView {
    
}

@property (nonatomic,strong) id  <SwitchDelegate> delegate;

@property CGRect frame1;
@property BOOL flag;
@property(strong, nonatomic) UIImageView *imgBtnBg;
@property(strong, nonatomic) UIImageView *imgCircle;

-(void)loadView;


@end
