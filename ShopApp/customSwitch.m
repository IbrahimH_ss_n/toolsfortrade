//
//  customSwitch.m
//  ShopApp
//
//  Created by Matrid on 17/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import "customSwitch.h"

@implementation customSwitch

-(void)loadView {
    
    self.tag = 200;
    
    _frame1 = self.frame;
    
    _imgBtnBg = [[UIImageView alloc] initWithFrame:CGRectMake(0, (_frame1.size.height/2)-((_frame1.size.height/2)/2), _frame1.size.width, _frame1.size.height/2)];
    [_imgBtnBg setBackgroundColor:[UIColor grayColor]];
    _imgBtnBg.layer.cornerRadius = _imgBtnBg.bounds.size.height/2;
    _imgBtnBg.clipsToBounds = YES;
    [self addSubview:_imgBtnBg];
    
    _imgCircle = [[UIImageView alloc] initWithFrame:CGRectMake(_frame1.size.width-_frame1.size.height, 0, _frame1.size.height, _frame1.size.height)];
    _imgCircle.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _imgCircle.layer.borderWidth = .5;
    _imgCircle.layer.cornerRadius = _imgCircle.bounds.size.height/2;
    _imgCircle.clipsToBounds = YES;
    [_imgCircle setBackgroundColor:[UIColor redColor]];
    [self addSubview:_imgCircle];
    
    UIButton *btnSwitches = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [btnSwitches addTarget:self action:@selector(btnSwitch) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnSwitches];
}

-(void)btnSwitch{
    CGRect frame = _imgCircle.frame;
    if (_flag) {
        frame.origin.x = 0;
        _flag = NO;
        [UIView animateWithDuration:0.2 animations:^{
            _imgCircle.frame = frame;
            _imgCircle.backgroundColor = [UIColor whiteColor];
            _imgBtnBg.backgroundColor = [UIColor lightGrayColor];
        }];
    }
    else {
        frame.origin.x = _frame1.size.width-frame.size.height;
        _flag = YES;
        [UIView animateWithDuration:0.2 animations:^{
            _imgCircle.frame = frame;
            _imgCircle.backgroundColor = [UIColor redColor];
            _imgBtnBg.backgroundColor = [UIColor grayColor];
        }];
    }
    [self.delegate isOn:_flag];
}

@end
