//
//  imgNoData.m
//  ShopApp
//
//  Created by Matrid on 05/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import "imgNoData.h"

@implementation imgNoData

-(void)loadNoDataImage{
    
    self.frame = CGRectMake(128, 252, 64, 64);
    self.image=[UIImage imageNamed:@"noDataFound.png"];
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self loadNoDataImage];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self loadNoDataImage];
    }
    return self;
}
@end
