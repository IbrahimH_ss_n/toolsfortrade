//
//  customFooter.m
//  maegntoShopApp
//
//  Created by Matrid on 23/05/16.
//  Copyright © 2016 Matrid. All rights reserved.
//

#import "customFooter.h"
#import "Constant.h"

@implementation customFooter
{
    UIViewController *currentVc;
}

-(void)SetView {
    self.frame = CGRectMake(0, 518, 320, 50);
    self.backgroundColor=[UIColor whiteColor];
    
    currentVc = [(UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController topViewController];
    
    imgHome = [[UIImageView alloc] initWithFrame:CGRectMake((80/2)-25+15, 5, 20, 20)];
    imgHome.contentMode = UIViewContentModeScaleAspectFit;
    
    imgUser = [[UIImageView alloc] initWithFrame:CGRectMake((80/2)-22+90, 5, 20, 20)];
    imgUser.contentMode = UIViewContentModeScaleAspectFit;
    
   
    
    imgMyCart = [[UIImageView alloc] initWithFrame:CGRectMake((80/2)-22+170, 5, 20, 20)];
    imgMyCart.contentMode = UIViewContentModeScaleAspectFit;

    imgMore = [[UIImageView alloc] initWithFrame:CGRectMake((80/2)-25+255, 5, 20, 20)];
    imgMore.contentMode = UIViewContentModeScaleAspectFit;

    btnHome = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 80, 49)];
    [btnHome addTarget:self action:@selector(home) forControlEvents:UIControlEventTouchUpInside];
    if ([currentVc isKindOfClass:[HomeViewController class]]) {
        btnHome.enabled = NO;
        
        imgHome.image = [UIImage imageNamed:@"Home-iconGreen"];
        imgUser.image = [UIImage imageNamed:@"loginBlack"];
        imgMyCart.image = [UIImage imageNamed:@"cartBlack"];
        imgMore.image = [UIImage imageNamed:@"moreBlack"];
        
    }
    
    UIButton *btnUser = [[UIButton alloc] initWithFrame:CGRectMake(80, 0, 80, 49)];
    [btnUser addTarget:self action:@selector(user) forControlEvents:UIControlEventTouchUpInside];
    if ([currentVc isKindOfClass:[UserViewController class]]) {
        btnUser.enabled = NO;
       
        imgHome.image = [UIImage imageNamed:@"Home-iconBlack"];
        imgUser.image = [UIImage imageNamed:@"loginGreen"];
        imgMyCart.image = [UIImage imageNamed:@"cartBlack"];
        imgMore.image = [UIImage imageNamed:@"moreBlack"];
    }

    UIButton *btnMyCart = [[UIButton alloc] initWithFrame:CGRectMake(160, 0, 80, 49)];
    [btnMyCart addTarget:self action:@selector(cart) forControlEvents:UIControlEventTouchUpInside];
    if ([currentVc isKindOfClass:[CartViewController class]]) {
        btnMyCart.enabled = NO;
        imgHome.image = [UIImage imageNamed:@"Home-iconBlack"];
        imgUser.image = [UIImage imageNamed:@"loginBlack"];
        imgMyCart.image = [UIImage imageNamed:@"cartGreen"];
        imgMore.image = [UIImage imageNamed:@"moreBlack"];
    }

    UIButton *btnMore = [[UIButton alloc] initWithFrame:CGRectMake(240, 0, 80, 49)];
    [btnMore addTarget:self action:@selector(more) forControlEvents:UIControlEventTouchUpInside];
    if ([currentVc isKindOfClass:[MoreViewController class]]) {
        btnMore.enabled = NO;
        imgHome.image = [UIImage imageNamed:@"Home-iconBlack"];
        imgUser.image = [UIImage imageNamed:@"loginBlack"];
        imgMyCart.image = [UIImage imageNamed:@"cartBlack"];
        imgMore.image = [UIImage imageNamed:@"moreGreen"];
        //[imgMore setImage:[UIImage imageNamed:@"moreGreen"]];
    }
    
    if ([currentVc isKindOfClass:[CategoryListViewController class]]) {
        imgHome.image = [UIImage imageNamed:@"Home-iconBlack"];
        imgUser.image = [UIImage imageNamed:@"loginBlack"];
        imgMyCart.image = [UIImage imageNamed:@"cartBlack"];
        imgMore.image = [UIImage imageNamed:@"moreBlack"];
    }
    if ([currentVc isKindOfClass:[CategoryHomeViewController class]]) {
        imgHome.image = [UIImage imageNamed:@"Home-iconBlack"];
        imgUser.image = [UIImage imageNamed:@"loginBlack"];
        imgMyCart.image = [UIImage imageNamed:@"cartBlack"];
        imgMore.image = [UIImage imageNamed:@"moreBlack"];
    }
    if ([currentVc isKindOfClass:[WishListViewController class]]) {
        imgHome.image = [UIImage imageNamed:@"Home-iconBlack"];
        imgUser.image = [UIImage imageNamed:@"loginBlack"];
        imgMyCart.image = [UIImage imageNamed:@"cartBlack"];
        imgMore.image = [UIImage imageNamed:@"moreBlack"];
    }
    if ([currentVc isKindOfClass:[VendorViewController class]]) {
        imgHome.image = [UIImage imageNamed:@"Home-iconBlack"];
        imgUser.image = [UIImage imageNamed:@"loginBlack"];
        imgMyCart.image = [UIImage imageNamed:@"cartBlack"];
        imgMore.image = [UIImage imageNamed:@"moreBlack"];
    }
    if ([currentVc isKindOfClass:[YourOrderViewController class]]) {
        imgHome.image = [UIImage imageNamed:@"Home-iconBlack"];
        imgUser.image = [UIImage imageNamed:@"loginBlack"];
        imgMyCart.image = [UIImage imageNamed:@"cartBlack"];
        imgMore.image = [UIImage imageNamed:@"moreBlack"];
    }
    if ([currentVc isKindOfClass:[OrderDetailViewController class]]) {
        imgHome.image = [UIImage imageNamed:@"Home-iconBlack"];
        imgUser.image = [UIImage imageNamed:@"loginBlack"];
        imgMyCart.image = [UIImage imageNamed:@"cartBlack"];
        imgMore.image = [UIImage imageNamed:@"moreBlack"];
    }
    if ([currentVc isKindOfClass:[DownloadableProductsVC class]]) {
        imgHome.image = [UIImage imageNamed:@"Home-iconBlack"];
        imgUser.image = [UIImage imageNamed:@"loginBlack"];
        imgMyCart.image = [UIImage imageNamed:@"cartBlack"];
        imgMore.image = [UIImage imageNamed:@"moreBlack"];
    }

 
//    UIImageView *circle = [[UIImageView alloc] initWithFrame:CGRectMake((80/2)-20+260, 2, 15, 15)];
//    circle.layer.cornerRadius = circle.bounds.size.height/2;
//    circle.backgroundColor = [UIColor colorWithRed:250/255.0 green:126/255.0 blue:108/255.0 alpha:1.0];
    
    UILabel *lblCount = [[UILabel alloc] initWithFrame:CGRectMake((80/2)-20+260, 2, 15, 15)];
   // [lblCount setText:[NSString stringWithFormat:@"%lu",(unsigned long)[Singletons singleton].cartData.count]];
    [lblCount setFont:[UIFont systemFontOfSize:11]];
    lblCount.textAlignment = NSTextAlignmentCenter;
    [lblCount setTextColor:[UIColor blackColor]];
    [lblCount setBackgroundColor:[UIColor clearColor]];
    
    
    UILabel *lblHome = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, 80, 13)];
    [lblHome setText:@"Home"];
    
    lblHome.font = [UIFont fontWithName:gmFont size:9];
    lblHome.textAlignment = NSTextAlignmentCenter;
    lblHome.textColor darkGrayColor;
    
    UILabel *lblUser = [[UILabel alloc] initWithFrame:CGRectMake(80, 30, 80, 13)];
    [lblUser setText:@"User"];
    lblUser.font = [UIFont fontWithName:gmFont size:9];
    lblUser.textAlignment = NSTextAlignmentCenter;
    lblUser.textColor darkGrayColor;
    
    UILabel *lblMyCart = [[UILabel alloc] initWithFrame:CGRectMake(160, 30, 80, 13)];
    [lblMyCart setText:@"My Cart"];
    lblMyCart.font = [UIFont fontWithName:gmFont size:9];
    lblMyCart.textAlignment = NSTextAlignmentCenter;
    lblMyCart.textColor darkGrayColor;
    
    UILabel *lblMore = [[UILabel alloc] initWithFrame:CGRectMake(240, 30, 80, 13)];
    [lblMore setText:@"More"];
    lblMore.font = [UIFont fontWithName:gmFont size:9];
    lblMore.textAlignment = NSTextAlignmentCenter;
    lblMore.textColor darkGrayColor;
    
    
//    UIImageView *border=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];
//    [border setBackgroundColor:[UIColor lightGrayColor]];
//    
//    [self addSubview:border];
    
    [self addSubview:imgHome];
    [self addSubview:imgUser];
    [self addSubview:imgMyCart];
    [self addSubview:imgMore];
    //[self addSubview:circle];
    
    //[self addSubview:lblCount];
    [self addSubview:lblHome];
    [self addSubview:lblUser];
    [self addSubview:lblMyCart];
    [self addSubview:lblMore];
    
    [self addSubview:btnHome];
    [self addSubview:btnUser];
    [self addSubview:btnMyCart];
    [self addSubview:btnMore];
}

- (void)home {
    NSLog(@"Home");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"homeFooter" object:nil];
    
}

- (void)user {
    NSLog(@"User");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"userFooter" object:nil];
    
}

- (void)cart {
    NSLog(@"Cart");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"cartFooter" object:nil];
   
    
}
- (void)more {
    NSLog(@"More");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"moreFooter" object:nil];
    
}


- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self SetView];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self SetView];
    }
    return self;
}



@end
