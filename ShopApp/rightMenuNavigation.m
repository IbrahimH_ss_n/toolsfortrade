//

//  rightMenuNavigation.m
//  mIracleNoodle
//
//  Created by Matrid on 05/04/16.
//  Copyright © 2016 Matrid. All rights reserved.
//

#import "rightMenuNavigation.h"


@implementation rightMenuNavigation {
    
    int currentSection;
    
}

@synthesize tblNavigationMenu, btnBackground, viewMenuHeader, lblBlank;

-(void)navigationBar {
    
    curView = [(UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController topViewController];

    self.frame=CGRectMake(0, 0, 320, 568);
    self.tag = 1000;
    currentSection = -1;
    
    arrMenuList=[[NSArray alloc]initWithObjects:@"Home",@"Shop by Category",@"Your Wish List",@"Your Account",@"Customer Service",@"Contact Us",@"FAQ", nil];
    arrImageMenu=[[NSArray alloc]initWithObjects:@"icon-home",@"icon-cat",@"icon-wishlist",@"icon-login",@"icon-customer",@"icon-contact",@"icon-faq", nil];
    
    //  CREATING NAVIGATION LIST
    
    btnBackground = [[UIButton alloc] initWithFrame:CGRectMake( -322, 0, 320, 568)];
    btnBackground.backgroundColor = [UIColor blackColor];
    btnBackground.alpha = 0.35;
    btnBackground.tag = 1002;
    [btnBackground addTarget:self action:@selector(hidesNavigation) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnBackground];
    
    viewMenuHeader=[[UIView alloc]initWithFrame:CGRectMake(-322, 0, 240, 55)];
    viewMenuHeader.tag=1003;
    viewMenuHeader.backgroundColor = [UIColor colorWithRed:220/255.0 green:4/255.0 blue:17/255.0 alpha:1.0];
    
    UILabel *lblHello=[[UILabel alloc]initWithFrame:CGRectMake(10, 25, 45, 30)];
//    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:@"Hello," attributes:@{NSFontAttributeName: [UIFont italicSystemFontOfSize:17]}];
    //[str appendAttributedString:str];
    lblHello.text=@"Hello,";
    lblHello.font=[UIFont fontWithName:@"Arial-ItalicMT" size:17];
    //lblHello.attributedText=str;
    lblHello.textColor=[UIColor whiteColor];
    [viewMenuHeader addSubview:lblHello];
    
    btnHelloUserName=[[UIButton alloc]initWithFrame:CGRectMake(60, 25, 150, 30)];
    btnHelloUserName.titleLabel.font = [UIFont fontWithName:@"Arial-ItalicMT" size:17];
    
    NSString * loginUser = [[NSUserDefaults standardUserDefaults] objectForKey:@"firstname"];
    if ([loginUser isEqualToString:@""] || loginUser == nil) {
        [btnHelloUserName setTitle:@"Sign In" forState:UIControlStateNormal];
        [btnHelloUserName addTarget:self action:@selector(btnSignIn) forControlEvents:UIControlEventTouchUpInside];
    }
    else {
        [btnHelloUserName setTitle:loginUser forState:UIControlStateNormal];
    }
    

    btnHelloUserName.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    btnHelloUserName.titleLabel.textColor = [UIColor whiteColor];
    [viewMenuHeader addSubview:btnHelloUserName];
    
    [self addSubview:viewMenuHeader];
    
    lblBlank=[[UILabel alloc]initWithFrame:CGRectMake(-322, 55, 240, 30)];
    lblBlank.backgroundColor=[UIColor whiteColor];
    [self addSubview:lblBlank];

    
    
    tblNavigationMenu = [[UITableView alloc] initWithFrame:CGRectMake(-322, 74, 240, 494) style:UITableViewStyleGrouped];
    tblNavigationMenu.delegate = self;
    tblNavigationMenu.dataSource = self;
    tblNavigationMenu.tag = 1001;
    tblNavigationMenu.scrollEnabled=NO;
    tblNavigationMenu.allowsSelection = YES;
    tblNavigationMenu.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [tblNavigationMenu setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:tblNavigationMenu];
    
    //viewMenulist = [[UIView alloc] initWithFrame:CGRectMake(-322, 0, 200, 568)];
    
    
    // END ..

}

- (void)btnSignIn {
    
    [Global hideNavigation];
    LoginViewController *login = [[LoginViewController alloc]init];
    [curView.navigationController pushViewController:login animated:YES];
    //NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray: curView.navigationController.viewControllers];
    
    // [navigationArray removeAllObjects];    // This is just for remove all view controller from navigation stack.
    //[navigationArray removeObjectAtIndex: 0];  // You can pass your index here
   // curView.navigationController.viewControllers = navigationArray;
}

-(void)hidesNavigation{
    [Global hideNavigation];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //navigationList =nil; //[Singletons singleton].navData;
    return arrMenuList.count;//navigationList.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section==1 || section==4) {
        return 5;
    }
    else
        return 0.1;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath;
{
    /// Here you can set also height according to your section and row
    return 42;
}

- (UIView *)tableView:(UITableView *)tv viewForFooterInSection:(NSInteger)section
{
    if (section==1 || section==4) {
        UIView *viewMenuFooter=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 240, 5)];
        UILabel *lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, 3, 240, 1)];
        lblLine.backgroundColor=[UIColor lightGrayColor];
        [viewMenuFooter addSubview:lblLine];
        
        return viewMenuFooter;
    }
    else
    {
        UIView *viewMenuFooter=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 240, 0)];
        return viewMenuFooter;
    }
    

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *defaultCell = @"cellDefault";
    navigationMenuCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:defaultCell];
    
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"navigationMenuCustomCell" owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    
    cell.lblName.text=[arrMenuList objectAtIndex:indexPath.section];
    cell.lblName.font=[UIFont fontWithName:gmFont size:13];
    cell.lblName.textColor grayColor;
    cell.imgMenuList.image=[UIImage imageNamed:[arrImageMenu objectAtIndex:indexPath.section]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
     [Global hideNavigation];
    if (indexPath.section==0) {
        for (UIViewController *view in curView.navigationController.viewControllers) {
            if ([view isKindOfClass:[HomeViewController class]]) {
                HomeViewController *home = (HomeViewController *)view;
                [curView.navigationController popToViewController:home animated:NO];
            }
        }
        //[Global hideNavigation];

    }
    
    if (indexPath.section==1) {
        //[Global hideNavigation];
        CategoryListViewController *category = [[CategoryListViewController alloc] init];
        
        category.strTitle=arrMenuList[1];
        
        [curView.navigationController pushViewController:category animated:YES];
    }
    if (indexPath.section==2) {
        //[Global hideNavigation];
        NSString *customer_Id = [[NSUserDefaults standardUserDefaults] objectForKey:@"Uid"];
        
        NSInteger cutom_Id = [customer_Id intValue];
        if (cutom_Id == 0) {
            [Global alertAdd:@"Please sign in first!" withTitle:nil navBack:NO];
        }
        else
        {
            WebViewController *web = [[WebViewController alloc] init];
            web.urlString = @"view_Wishlist";
            
            [curView.navigationController pushViewController:web animated:YES];
            /* WishListViewController *wish = [[WishListViewController alloc] init];
             
             [self.navigationController pushViewController:wish animated:YES];*/
        }
    }
    if (indexPath.section==3) {
        
        
        UserViewController *user = [[UserViewController alloc] init];
        [curView.navigationController pushViewController:user animated:YES];
    }
    
    if (indexPath.section==4) {
        //[Global hideNavigation];
        WebViewController *web = [[WebViewController alloc] init];
        
        web.urlString = @"CustomerService";
        
        [curView.navigationController pushViewController:web animated:YES];
    }
    if (indexPath.section==5) {
        
        WebViewController *web = [[WebViewController alloc] init];
        
        web.urlString = @"ContactUs";
        
        [curView.navigationController pushViewController:web animated:YES];

        
    }
    
    if (indexPath.section==6) {
        //[Global hideNavigation];
        WebViewController *web = [[WebViewController alloc] init];
        
        web.urlString = @"FAQ";
        
        [curView.navigationController pushViewController:web animated:YES];
    }
    if (indexPath.section==7) {
        //[Global hideNavigation];
        WebViewController *web = [[WebViewController alloc] init];
        
        web.urlString = @"ContactUs";
        
        [curView.navigationController pushViewController:web animated:YES];
    }
    if (indexPath.section==8) {
        //[Global hideNavigation];
        WebViewController *web = [[WebViewController alloc] init];
        
        web.urlString = @"FAQ";
        
        [curView.navigationController pushViewController:web animated:YES];
    }
    
    
}

-(id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self navigationBar];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self navigationBar];
    }
    return self;
}

@end
