//
//  CategoryListViewController.h
//  ShopApp
//
//  Created by Matrid on 07/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface CategoryListViewController : UIViewController
{
    UIButton *btnBack;
    NSArray *shopByCategoryArrayMenu;
    NSDictionary *dicSubCategoryTemp;
    NSInteger selectedSection;
    
}

@property (strong, nonatomic) NSString *strTitle;
@property (weak, nonatomic) IBOutlet UITableView *tblCategoryList;
@property (weak, nonatomic) IBOutlet UILabel *lblMenuTitle;

@end
