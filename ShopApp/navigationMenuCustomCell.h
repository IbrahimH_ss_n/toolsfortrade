//
//  navigationMenuCustomCell.h
//  mIracleNoodle
//
//  Created by Matrid on 05/04/16.
//  Copyright © 2016 Matrid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface navigationMenuCustomCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIImageView *imgMenuList;

@end
