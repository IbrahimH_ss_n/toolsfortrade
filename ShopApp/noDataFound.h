//
//  noDataFound.h
//  ShopApp
//
//  Created by Matrid on 05/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface noDataFound : UIView
{
    UIImageView *imgview;
    UILabel *lblNoDataText;
}
@property (strong, nonatomic) NSString *strErrorMessage;

- (void)setView;
@end
