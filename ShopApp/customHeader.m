//
//  customHeader.m
//  maegntoShopApp
//
//  Created by Matrid on 23/05/16.
//  Copyright © 2016 Matrid. All rights reserved.
//

#import "customHeader.h"
#import "Constant.h"

@implementation customHeader
{
    UIViewController *currentVc;;
}

@synthesize lblHeaderTitle,lblItemFound,btnBack,strTitle,btnNav,btnNotification,btnSearch,txtSearch,mTitle;

-(void)setView {
    
    self.frame = CGRectMake(0, 0, 320, 60);
    
    self.backgroundColor greenColor;
    
     currentVc = [(UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController topViewController];
    
    //Creaing title icon
    mTitle=[[UIImageView alloc] initWithFrame:CGRectMake(120,22,95,17)];
    mTitle.image=[UIImage imageNamed:@"whitelogo.png"];
  //  [[UIImageView alloc]initWithFrame:<#(CGRect)#>]
    // CREATING HEADER TITLE FOR SIGNUP PAGE
    
    lblHeaderTitle = [[UILabel alloc] init];
  // NSString *name = lblHeaderTitle.text;
    
    
    //lblHeaderTitle.text=strTitle;//LocalizedString(@"Hello");
   // lblHeaderTitle.font = [UIFont fontWithName:@"zAristaLight" size:21];
    CGRect frame1 = [Global label:lblHeaderTitle];
    frame1.origin.x = (self.frame.size.width/2)-(frame1.size.width/2);
    frame1.origin.y = 28;//(self.frame.size.height/2)-(frame1.size.height/2)+4;
 //   lblHeaderTitle.lineBreakMode = NSLineBreakByWordWrapping;
  //  [lblHeaderTitle sizeToFit];
  //  lblHeaderTitle.frame = frame1;
  //  [lblHeaderTitle setTextColor:[UIColor whiteColor]];
  //  [lblHeaderTitle setTextAlignment:NSTextAlignmentCenter];
   // lblHeaderTitle.tag = 1111;
    [self addSubview:mTitle];
    
    // CREATING ITEM FOUND
    
    lblItemFound = [[UILabel alloc] init];
    //lblItemFound.text=@"";//LocalizedString(@"Hello");
    lblItemFound.font = [UIFont fontWithName:gmFont size:12];
    lblItemFound.tag = 111;
//    CGRect frameItem = [Global label:lblItemFound];
//    frameItem.origin.x = (self.frame.size.width/2)-(frameItem.size.width/2);
//    frameItem.origin.y = 42;//(self.frame.size.height/2)-(frame1.size.height/2)+4;
//    lblItemFound.lineBreakMode = NSLineBreakByWordWrapping;
//    [lblItemFound sizeToFit];
//    lblItemFound.frame = frameItem;
//    [lblItemFound setTextColor:[UIColor whiteColor]];
    [lblItemFound setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:lblItemFound];

    
    // CREATING HEADER BUTTON TO TAKE USER TO PREVIOUS SCREEN
    
    btnBack=[[UIButton alloc] initWithFrame:CGRectMake(0, 20, 35, 40)];
    //btnBack.backgroundColor = [UIColor blueColor];
    [btnBack setImage:[UIImage imageNamed:@"back"] forState:normal];
    btnBack.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    [btnBack addTarget:self action:@selector(btnBackAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnBack];
    //[self bringSubviewToFront:btnNav];

    // CREATING HEADER BUTTON TO TAKE USER TO OPEN NAVIGATION MENU
    
    btnNav=[[UIButton alloc] initWithFrame:CGRectMake(30, 20, 40, 40)];
    //btnNav.backgroundColor = [UIColor redColor];
    [btnNav setImage:[UIImage imageNamed:@"menu-icon"] forState:normal];
    btnNav.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    [btnNav addTarget:self action:@selector(btnNavAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnNav];
    //[self bringSubviewToFront:btnNav];
    
    
    
    btnNotification=[[UIButton alloc] initWithFrame:CGRectMake(240, 20, 40, 40)];
    //btnNotification.backgroundColor = [UIColor redColor];
    [btnNotification setImage:[UIImage imageNamed:@"notification"] forState:normal];
    [btnNotification addTarget:self action:@selector(btnNavNotification:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnNotification];
   // [self bringSubviewToFront:btnNotification];
    
    _lblNotification=[[UILabel alloc]initWithFrame:CGRectMake(257, 20, 15 , 15)];
    _lblNotification.backgroundColor=[UIColor redColor];
    _lblNotification.font=[UIFont fontWithName:@"Arial" size:10];
    _lblNotification.layer.cornerRadius=_lblNotification.frame.size.width/2;
    _lblNotification.clipsToBounds=YES;
    [_lblNotification setText:@"10"];
    _lblNotification.textColor=[UIColor whiteColor];
    _lblNotification.textAlignment=NSTextAlignmentCenter;
    [self addSubview:_lblNotification];
    //[self bringSubviewToFront:_lblNotification];
    
    
    btnSearch=[[UIButton alloc] initWithFrame:CGRectMake(280, 20, 40, 40)];
   // btnSearch.backgroundColor=[UIColor blueColor];
    [btnSearch setImage:[UIImage imageNamed:@"search"] forState:normal];
    [btnSearch addTarget:self action:@selector(btnNavSearch:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnSearch];
    [self bringSubviewToFront:btnSearch];
    

    // END ..

}

-(void)btnBackAction:(id)sender {
    UIViewController *curView=[(UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController topViewController];
    [curView.navigationController popViewControllerAnimated:YES];
    
}
-(void)btnNavNotification:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationHeader" object:self];
}

-(void)btnNavAction:(id)sender {
    menuOpen = [Singletons singleton].ismenuOpen;
    if(menuOpen){
        [self closeMenu];
    }else{
        [self openMenu];
    }
    
}

- (void) openMenu {
    //[Singletons singleton].ismenuOpen = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"navigationListOpen" object:self];
}

- (void) closeMenu {
    //[Singletons singleton].ismenuOpen = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"navigationListClose" object:self];
}

-(void)btnNavSearch:(id)sender {
//    //btnSearch.hidden=YES;
    // rightMenuNavigation *menu = [[rightMenuNavigation alloc] init];
    UIViewController *curView=[(UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController topViewController];
    
    [Singletons singleton].isFrom = @"Search";
    ProductListViewController *product=[[ProductListViewController alloc]init];
    [curView.navigationController pushViewController:product animated:YES];
    
    
    
}




//- (instancetype)initWithCoder:(NSCoder *)coder
//{
//    self = [super initWithCoder:coder];
//    if (self) {
//        [self setView];
//    }
//    return self;
//}
//
//- (instancetype)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        [self setView];
//    }
//    return self;
//}


@end
