//
//  YourOrderViewController.h
//  ShopApp
//
//  Created by Matrid on 02/05/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface YourOrderViewController : UIViewController
{
    NSArray *arrOrderDetails;
}
@property (weak, nonatomic) IBOutlet UITableView *tblOrderDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblYourOrder;

@end
