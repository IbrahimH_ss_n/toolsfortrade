//
//  FilterByViewController.m
//  ShopApp
//
//  Created by Matrid on 17/03/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import "FilterByViewController.h"
#import "Category.h"
#import "SubCategory.h"
#import "Utility.h"

@interface FilterByViewController ()
@property (weak, nonatomic) IBOutlet UITextField *searchBarCategoryTF;

@property (strong, nonatomic) Category *category;
@property (strong, nonatomic) NSMutableDictionary *parametersDict;
@property (strong, nonatomic) NSString *searchString;
@end

@implementation FilterByViewController {
    BOOL isFiltered;
    int countOfFiteredData;
}

@synthesize catgory_Id;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.searchBarCategoryTF.delegate = self;
    
    selectedRow = 0;
    _tblFilterGroup.allowsMultipleSelection = false;
    self.searchString = [[NSString alloc]init];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == _tblFilterGroup) {
        return [arrFlter count];
    }
    else if(tableView == _tblFilterChild) {
        if (!isFiltered) {
            _category = (Category *)[arrFlter objectAtIndex:selectedRow];
            return [_category getSubCategoryCountFor:nil];
        } else {
//            NSLog(@"TextField Text: %@", _searchBarCategoryTF.text);
            _category = (Category *)[arrFlter objectAtIndex:selectedRow];
            return [_category getSubCategoryCountFor:self.searchString];
        }
        //        return [[[arrFlter objectAtIndex:selectedRow] valueForKey:@"data"] count];
        //        return 4;//[[(Category *)[arrFlter objectAtIndex:selectedRow] options] count];
    }
    return 0;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if ([textField.text length] == 0) {
        isFiltered = false;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSLog(@"Range : %@",NSStringFromRange(range));
    NSLog(@"Replacement Text: %@", string);
    if ([textField.text length] == 0) {
        self.searchString = [[NSString alloc]initWithFormat:@"%@", string] ;
    } else {
        self.searchString = [[NSString alloc]initWithFormat:@"%@%@", textField.text, string];
    }
    if (range.length == 1) {
        NSLog(@"Erasing Content");
        if ([self.searchString length] > 0) {
            self.searchString = [self.searchString substringToIndex:[self.searchString length]-1];
        }
//        if ([self.searchString length] == 1) {
//            self.searchString = [NSString new];
//        }
//        self.searchString = [self.searchString substringToIndex:self.searchString.length-(self.searchString.length>0)];
    }
    NSLog(@"searchString %@", self.searchString);
    if ([self.searchString length] > 0) {
        isFiltered = true;
    } else {
        if ([self.searchString length] == 0) {
            isFiltered = false;
        }
    }
    [_tblFilterChild reloadData];
    return TRUE;
}

- (void)viewWillAppear:(BOOL)animated {
    _parametersDict = [[NSMutableDictionary alloc] init];
    _viewFilterGroup.backgroundColor = [UIColor clearColor];
    _viewFilterGroup.layer.borderWidth = 1;
    _viewFilterGroup.layer.borderColor = [[UIColor colorWithRed:220.0/255.0 green:13.0/255.0 blue:17.0/255.0 alpha:1.0] CGColor];
    
    _viewFilterFooter.layer.borderWidth = 1;
    _viewFilterFooter.layer.borderColor = [[UIColor colorWithRed:220.0/255.0 green:13.0/255.0 blue:17.0/255.0 alpha:1.0] CGColor];
    _viewFilterFooter.clipsToBounds = YES;
    
    [self fetchFilter];
    isFiltered = false;
}
//@"7"
- (void)fetchFilter{
     NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:catgory_Id,@"category_id", nil];
    NSLog(@"%@", dic);
    //http://stage.tools4trade.co.uk/T4T-mage-api.php?method=filterByCategory&category_id=44
    //[apiDomain stringByAppendingString:apiUrlCategoryFilters]
    [Global urlString:@"http://stage.tools4trade.co.uk/T4T-mage-api.php?method=filterByCategory" parameter:dic indicator:YES currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response) {
        NSLog(@"%@", response);
        if(result)
        {
            
            if ([[response valueForKey:@"Status"] isEqualToString:@"SUCCESS"]) {
                
//                NSArray *arr = [[[response valueForKey:@"Details"] valueForKey:@"code"] mutableCopy];
//                NSArray *labelValue = [[[response valueForKey:@"Details"] valueForKey:@"options"] valueForKey:@"label"];
//                NSArray *count = [[NSArray alloc]init];
//                arrFlter = [NSMutableArray arrayWithArray:arr];

                NSArray *array1234 = [[response valueForKey:@"Details"] mutableCopy];
//                NSArray *labelValue = [[[response valueForKey:@"Details"] valueForKey:@"options"] valueForKey:@"label"];
//                NSArray *count = [[NSArray alloc]init];
//                arrFlter = [NSMutableArray arrayWithArray:arr];

                NSMutableArray *categories = [[NSMutableArray alloc]init];
                for (NSDictionary *dictionary in array1234) {
                    self.category = [[Category alloc]initWithDictionary:dictionary];
                    [categories addObject:self.category];
                }
                arrFlter = [NSMutableArray arrayWithArray:categories];
                
//                id listObj = [response objectForKey:@"Details"];
//                if (!listObj || [listObj isEqual:[NSNull null]]) {
//                    return nil;
//                }
//                NSMutableArray* listData = [[NSMutableArray alloc] init];
//                if (listObj && [listObj isKindOfClass:[NSArray class]])
//                {
//                    NSArray* propertyArray = (NSArray*)listObj;
//                    for (NSDictionary* listingDictionary in propertyArray) {
//                        Category* userDetails = [[Category alloc] initWithDictionary:listingDictionary];
//                    }
//                }
                
                
                
                
                
                
                
                
                
                
                [_tblFilterGroup reloadData];
                [_tblFilterChild reloadData];
            }
        }
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
   
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.1;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UILabel *lblLine = [[UILabel alloc]init];
    lblLine.backgroundColor minGrayColor;
    radioBtnCount = [[NSMutableArray alloc]init];
    if (tableView == _tblFilterGroup) {

        static NSString *defaultCell = @"cell";
        tblFilterGroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:defaultCell];

        if (cell == nil) {
            NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"tblFilterGroupTableViewCell" owner:self options:nil];
            cell = [array objectAtIndex:0];
        }
        if (indexPath.row == selectedRow) {
            [cell.btnFilterGroupName setTitleColor:[UIColor colorWithRed:220.0/255.0 green:13.0/255.0 blue:17.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        }

    _category = (Category*)[arrFlter objectAtIndex:indexPath.row];
    [cell.btnFilterGroupName setTitle:_category.title forState:UIControlStateNormal];
//        [cell.btnFilterGroupName setTitle:[[arrFlter objectAtIndex:indexPath.row] valueForKey:@"attribute_name"] forState:UIControlStateNormal];

//        [cell.btnFilterGroupName setTitle:[arrFlter objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        cell.btnFilterGroupName.tag = indexPath.row;
        [cell.btnFilterGroupName addTarget:self action:@selector(btnGroupName:) forControlEvents:UIControlEventTouchUpInside];

        lblLine.frame = CGRectMake(0, 43, 210, 1);
        [cell addSubview:lblLine];
        return cell;
    }

    static NSString *defaultCell = @"cell";
    tblFilterChildTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:defaultCell];


    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"tblFilterChildTableViewCell" owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    _category = (Category *)[arrFlter objectAtIndex:selectedRow];
    SubCategory *subCategory = (SubCategory *)[_category filteredOptionsFor:self.searchString And:indexPath.row];

//    SubCategory *subCategory = (SubCategory *)[_category.options objectAtIndex: indexPath.row];
    NSString *filterName = subCategory.label;
    NSNumber *count = subCategory.countVal;
    cell.lblFilterChild.text = filterName;
    cell.lblFilterChildCount.text = [NSString stringWithFormat:@"%@", count];
    
    
    BOOL isSelected = [_category isOptionSelectedFor:subCategory.label];//[_category isOptionSelectedAt:indexPath.row];
    [self.parametersDict addEntriesFromDictionary:[_category dictionaryValues]];
    NSLog(@"%@", self.parametersDict);
    if (isSelected) {
        [cell.btnFilterChildCheck setImage:[UIImage imageNamed:@"tick-new"] forState:UIControlStateNormal];
    } else {
        [cell.btnFilterChildCheck setImage:[UIImage imageNamed:@"tick-un"] forState:UIControlStateNormal];
//        [self.parametersDict removeObjectForKey:_category.code];
    }
//    NSString *filterName = [[[[arrFlter objectAtIndex:selectedRow] valueForKey:@"data"] objectAtIndex:indexPath.row] valueForKey:@"option_label"];
//    NSNumber *filterCount = [[[[arrFlter objectAtIndex:selectedRow] valueForKey:@"data"] objectAtIndex:indexPath.row] valueForKey:@"count"];

//    flagRadioBtn = [[[[arrFlter objectAtIndex:selectedRow] valueForKey:@"data"] objectAtIndex:indexPath.row] valueForKey:@"flag"];
    
//    cell.lblFilterChild.text = filterName;
    
//    cell.lblFilterChildCount.text = [NSString stringWithFormat:@"%@",filterCount];
    
    [cell.btnFilterChildCheck addTarget:self action:@selector(checkFilterButton:) forControlEvents:UIControlEventTouchUpInside];

    cell.btnFilterChildCheck.tag = indexPath.row;
    
    
//    if([[[[[arrFlter objectAtIndex:selectedRow] valueForKey:@"data"] objectAtIndex:indexPath.row] valueForKey:@"status"] isEqualToString:@"Untick"] && [flagRadioBtn isEqualToString:@"no"]){
//
//        [cell.btnFilterChildCheck setImage:[UIImage imageNamed:@"tick-un"] forState:UIControlStateNormal];
//    }
//    else if([[[[[arrFlter objectAtIndex:selectedRow] valueForKey:@"data"] objectAtIndex:indexPath.row] valueForKey:@"status"] isEqualToString:@"Tick"] && [flagRadioBtn isEqualToString:@"no"]){
//        [cell.btnFilterChildCheck setImage:[UIImage imageNamed:@"tick-new"] forState:UIControlStateNormal];
//    }
    
//    else if([[[[[arrFlter objectAtIndex:selectedRow] valueForKey:@"data"] objectAtIndex:indexPath.row] valueForKey:@"status"] isEqualToString:@"Untick"] && [flagRadioBtn isEqualToString:@"yes"]){
//        [cell.btnFilterChildCheck setImage:[UIImage imageNamed:@"radio-btn"] forState:UIControlStateNormal];
//    }
    
//    else if([[[[[arrFlter objectAtIndex:selectedRow] valueForKey:@"data"] objectAtIndex:indexPath.row] valueForKey:@"status"] isEqualToString:@"Tick"] && [flagRadioBtn isEqualToString:@"yes"]) {
//        [cell.btnFilterChildCheck setImage:[UIImage imageNamed:@"check-radio-btn"] forState:UIControlStateNormal];
//    }
    
    lblLine.frame = CGRectMake(0, 43, 210, 1);
    [cell addSubview:lblLine];
    
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tblFilterGroup) {
        selectedRow = indexPath.row;
        [_tblFilterChild reloadData];
    }
//    selectedRow = indexPath.row;
}

- (void)checkFilterButton:(UIButton*) sender{
    _category = (Category *)[arrFlter objectAtIndex:selectedRow];
    SubCategory *subCategory = (SubCategory *)[_category filteredOptionsFor:self.searchString And:sender.tag];
    
    

    [_category handleSelectionFor:subCategory.label];
    
//    [_category handleSelectionAt:sender.tag];
    [_tblFilterChild reloadData];
    
    
//    if ([flagRadioBtn isEqualToString:@"yes"]) {
//        NSMutableDictionary *dict;
////        for ( int i = 0; i <[[[arrFlter objectAtIndex:selectedRow] valueForKey:@"data"] count]; i++) {
////            dict = [[[[arrFlter objectAtIndex:selectedRow] valueForKey:@"data"] objectAtIndex:i] mutableCopy];
////
////            if ([[dict valueForKey:@"status"] isEqualToString:@"Tick"]) {
////                [dict setObject:@"Untick" forKey:@"status"];
////                NSMutableArray *mutableArray = [NSMutableArray arrayWithArray:arrFlter];
////                mutableArray = [mutableArray mutableCopy];
////                [[[mutableArray objectAtIndex:selectedRow] valueForKey:@"data"] replaceObjectAtIndex:i withObject:dict];
////                arrFlter = mutableArray;
////            }
////        }
////        dict = [[[[arrFlter objectAtIndex:selectedRow] valueForKey:@"data"] objectAtIndex:sender.tag] mutableCopy];
////        [dict setObject:@"Tick" forKey:@"status"];
//        NSMutableArray *mutableArray = [NSMutableArray arrayWithArray:arrFlter];
//        mutableArray = [mutableArray mutableCopy];
////        [[[mutableArray objectAtIndex:selectedRow] valueForKey:@"data"] replaceObjectAtIndex:sender.tag withObject:dict];
//        arrFlter = mutableArray;
//
//        [_tblFilterChild reloadData];
//
//    }
//    else {
////        NSMutableDictionary *dict = [[[[arrFlter objectAtIndex:selectedRow] valueForKey:@"data"] objectAtIndex:sender.tag] mutableCopy];
//
////        if ([[dict valueForKey:@"status"] isEqualToString:@"Untick"]) {
////            [dict setObject:@"Tick" forKey:@"status"];
////        }
////        else {
////            [dict setObject:@"Untick" forKey:@"status"];
////        }
//
//        NSMutableArray *mutableArray = [NSMutableArray arrayWithArray:arrFlter];
//        mutableArray = [mutableArray mutableCopy];
////        [[[mutableArray objectAtIndex:selectedRow] valueForKey:@"data"] replaceObjectAtIndex:sender.tag withObject:dict];
////        arrFlter = mutableArray;
//
////        [_tblFilterChild reloadData];
//    }
}

- (void)btnGroupName:(UIButton*) sender{
    selectedRow = sender.tag;
    [_tblFilterChild reloadData];
    [_tblFilterGroup reloadData];
//    [_tblFilterChild reloadData];
//    [_tblFilterGroup reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnCloseFilterClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnClearAll:(id)sender {
    for (int i = 0; i<arrFlter.count; i++) {
        Category *category = (Category *)[arrFlter objectAtIndex:i];
        [category clearAllSelection];
    }
    self.parametersDict = [[NSMutableDictionary alloc]init];
    [_tblFilterChild reloadData];
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (IBAction)btnApplyFilter:(id)sender {
    BOOL isTick = NO;
    if ([self.parametersDict count] != 0) {
        [self.parametersDict setObject: catgory_Id forKey:@"category_id"];
        [_delegate sendFilterParams:self.parametersDict];
        [self.navigationController popViewControllerAnimated:true];
    } else {
        [Global alertAdd:@"Please select a filter first" withTitle:@"Select" navBack:NO];
    }
    
//    for (int i = 0; i<arrFlter.count; i++) {
//        for (int j = 0; j<[[[arrFlter objectAtIndex:i] valueForKey:@"data"] count]; j++) {
//            if ([[[[[arrFlter objectAtIndex:i] valueForKey:@"data"] objectAtIndex:j] valueForKey:@"status"] isEqualToString:@"Tick"]) {
//                isTick = YES;
//                break;
//            }
//        }
//        if (isTick) {
//            break;
//        }
//    }
//    if (isTick) {
//        if ([_delegate respondsToSelector:@selector(sendFilterArray:)])
//        {
//            [_delegate sendFilterArray:arrFlter];
//        }
//        [self.navigationController popViewControllerAnimated:YES];
//    }
//    else
//    {
//        [Global alertAdd:@"Please select a filter first" withTitle:@"Select" navBack:NO];
//    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//// Defined in your class interface:
//NSMutableArray *searchResults = [[NSMutableArray alloc] init];
//NSMutableArray *myArray; // contains all of your objects, could be a dictionary as well


//- (void) searchTableView {
//
//    NSString *searchText = self.seachProduct.text;
//    NSMutableArray *searchArray = [[NSMutableArray alloc] init];
//
////        _category = (Category *)[arrFlter objectAtIndex:selectedRow]
//    [searchArray addObjectsFromArray:_category.options];
//
//    for (SubCategory *subcategory in searchArray) {
//        NSString *objectName = [subcategory label];
//        NSRange resultsRange = [objectName rangeOfString:searchText options:NSCaseInsensitiveSearch];
//
//        if (resultsRange.length > 0)
//            [searchResults addObject:subcategory];
//    }
//
//    searchArray = nil;
//}
@end
