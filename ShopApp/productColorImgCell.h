//
//  CollectionViewCell.h
//  ShopApp
//
//  Created by Matrid on 13/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface productColorImgCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgProductSizeImage;
@property (weak, nonatomic) IBOutlet UIImageView *imgCircleTick;

@end
