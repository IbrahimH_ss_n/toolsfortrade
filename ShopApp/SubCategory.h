//
//  SubCategory.h
//  ShopApp
//
//  Created by Md Ibrahim Hassan on 26/04/18.
//  Copyright © 2018 Test. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubCategory : NSObject
@property (strong, nonatomic)NSString *label;
@property (strong, nonatomic)NSString *value;
@property (strong, nonatomic)NSNumber *countVal;
- (id) initWithDictionary:(NSDictionary*)dictionary;
@end
