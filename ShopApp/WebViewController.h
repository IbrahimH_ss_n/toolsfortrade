//
//  WebViewController.h
//  ShopApp
//
//  Created by Matrid on 14/04/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constant.h"

@interface WebViewController : UIViewController <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (strong, nonatomic) IBOutlet NSString *urlString;

@end
