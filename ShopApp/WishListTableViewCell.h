//
//  WishListTableViewCell.h
//  ShopApp
//
//  Created by Matrid on 01/03/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WishListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgWishList;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscription;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrencyPrice;

@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIButton *btnAddToCart;

@end
