//
//  noDataFound.m
//  ShopApp
//
//  Created by Matrid on 05/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import "noDataFound.h"

@implementation noDataFound

@synthesize strErrorMessage;

-(void)setView {
    
    
    self.frame =CGRectMake(60, 184, 200, 200);
    imgview=[[UIImageView alloc]initWithFrame:CGRectMake((self.frame.size.width-64)/2, 0, 64, 64)];
    imgview.image=[UIImage imageNamed:@"noDataFound.png"];
    
    
    lblNoDataText = [[UILabel alloc] initWithFrame:CGRectMake(0, 70, 200, 200)];
    lblNoDataText.textColor = [UIColor blackColor];
    lblNoDataText.text= strErrorMessage;
    lblNoDataText.font = [UIFont fontWithName:gmFont size:15];
    CGRect frame1 = [Global label:lblNoDataText];
    frame1.origin.x = (self.frame.size.width/2)-(frame1.size.width/2);
    lblNoDataText.lineBreakMode = NSLineBreakByWordWrapping;
    lblNoDataText.numberOfLines = 0;
    [lblNoDataText sizeToFit];
    lblNoDataText.frame = frame1;
    [lblNoDataText setTextAlignment:NSTextAlignmentCenter];
    
    
    [self addSubview:imgview];
    [self addSubview:lblNoDataText];
    
    CGRect frames = self.frame;
    frames.size.height = lblNoDataText.frame.size.height + imgview.frame.size.height +5;
    self.frame = frames;
}

//- (instancetype)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        [self loadNoDataImage];
//    }
//    return self;
//}

@end
