 //
//  DownloadableProductsVC.m
//  ShopApp
//
//  Created by Matrid on 05/06/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import "DownloadableProductsVC.h"

@interface DownloadableProductsVC ()

@end

@implementation DownloadableProductsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor screenBackgroundColor;
    // Do any additional setup after loading the view from its nib.
    [Global customHeader:self headerTitle:@"S A V I S H O P S" itemFound:NO hideBackButton:NO hideSearchButton:YES hideNotificationButton:YES];
    //_lblDownloadable.backgroundColor screenBackgroundColor;
    _lblDownloadable.font = [UIFont fontWithName:gmFont size:15];
    _lblDownloadable.text = @"  My Downloadable Products";
    _lblDownloadable.textColor grayColor;
    [Global customFooter:self.view];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self getDownloadableProducts];

    //self.tblDownloadProducts.backgroundColor screenBackgroundColor;
    
}

- (void)getDownloadableProducts {
    
    NSString *customerId = @"2";//[[NSUserDefaults standardUserDefaults] valueForKey:@"Uid"];
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:customerId,@"customer_id", nil];
    
    [Global urlString:[apiDomain stringByAppendingString:apiUrlDownloadableProducts] parameter:dic indicator:YES currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response) {
        if(result) { //DATA_MISSING
            if ([[response valueForKey:@"status"] isEqualToString:@"SUCCESS"]) {
                arrDownLoadableProducts = [response valueForKey:@"details"];
                [_tblDownloadProducts reloadData];
                _viewHeader.hidden = NO;
                _viewHeader.backgroundColor = [UIColor colorWithRed:232.0/255.0 green:237.0/255.0 blue:238.0/255.0 alpha:1.0];
                
            }
        }
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrDownLoadableProducts.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 35;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return _viewHeader;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    return 280;
//}
//
//
//-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//    return _viewForFooter;
//}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"newFriendCell";
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    DownloadableCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"DownloadableCellTableViewCell" owner:self options:nil];
        cell = [array objectAtIndex:0];
    }
    
    if (indexPath.row % 2 == 0) {
        cell.backgroundColor = [UIColor whiteColor];
    }

    else
    {
        cell.backgroundColor = [UIColor colorWithRed:232.0/255.0 green:237.0/255.0 blue:238.0/255.0 alpha:1.0];
    }
    
    cell.lblOder_Id.font = [UIFont fontWithName:gmFont size:9];
    cell.lblOder_Id.textColor = [UIColor colorWithRed:63.0/255.0 green:164.0/255.0 blue:198.0/255.0 alpha:1.0];
    cell.lblOder_Id.text = [[arrDownLoadableProducts objectAtIndex:indexPath.row] valueForKey:@"order_id"];
    
    cell.lblOrder_Date.font = [UIFont fontWithName:gmFont size:9];
    cell.lblOrder_Date.textColor grayColor;
    
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *date = [dateFormatter dateFromString:[[arrDownLoadableProducts objectAtIndex:indexPath.row] valueForKey:@"created_at"]];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *strDate = [dateFormatter stringFromDate:date];
    
    cell.lblOrder_Date.text = strDate;
    
    cell.lblOderDescription.font = [UIFont fontWithName:gmFont size:9];
    cell.lblOderDescription.textColor grayColor;
    cell.lblOderDescription.text = [[arrDownLoadableProducts objectAtIndex:indexPath.row] valueForKey:@"link_title"];
    
    [cell.btnDownloadLink addTarget:self action:@selector(downloadProduct:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnDownloadLink.tag = indexPath.row;
    return cell;
}
- (void)downloadProduct:(UIButton *)sender{
    NSString *link = [[arrDownLoadableProducts objectAtIndex:sender.tag] valueForKey:@"link"];
    NSLog(@"Link>>>%@",link);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
