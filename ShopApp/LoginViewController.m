//
//  LoginViewController.m
//  ShopApp
//
//  Created by Matrid on 06/01/17.
//  Copyright © 2017 Test. All rights reserved.
//

#import "LoginViewController.h"
#import "Constant.h"
#import <FBSDKCoreKit/FBSDKAccessToken.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface LoginViewController () {
    BOOL savePwd;
}

@end

@implementation LoginViewController

@synthesize loginFrom;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    savePwd = YES;
    
    _btnLogin.layer.cornerRadius=20;
    _btnForgotSubmit.layer.cornerRadius=20;
    
    
    //    if (![[[NSUserDefaults standardUserDefaults] valueForKey:@"savedEmail"] isEqualToString:@""]) {
    //        HomeViewController *home = [[HomeViewController alloc] init];
    //        [self.navigationController pushViewController:home animated:NO];
    //    }
    
    [Global addSwitch:YES frame:CGRectMake( 24, 137, 30, 15)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ssMediaLogin:) name:@"ssSignup" object:nil];
    
    if (![[[NSUserDefaults standardUserDefaults] valueForKey:@"pwd"] isEqualToString:@""]) {
        _txtPassword.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"pwd"];
    }
    _scrollView.contentSize=CGSizeMake(320, 300);
    self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)ssMediaLogin:(NSNotification *)noti{
    noti = [noti valueForKey:@"userInfo"];
    NSDictionary *dicty = [NSDictionary dictionaryWithObjectsAndKeys:[noti valueForKey:@"email"],@"email",[noti valueForKey:@"fistName"],@"first_name",[noti valueForKey:@"lastName"],@"last_name",[noti valueForKey:@"id"],@"password", nil];
    [Global urlString:[apiDomain stringByAppendingString:apiUrlSocialSignUp] parameter:dicty indicator:YES currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response) {
        if(result) {
                  NSLog(@"Login response:-%@",response);
            if ([[response valueForKey:@"status"] isEqualToString:@"DATA_MISSING"]) {
                [Global alertAdd:@"Your entred email or password is incorrect." withTitle:nil navBack:NO];
            }
            if ([[response valueForKey:@"status"] isEqualToString:@"SUCCESS"]) {
                [[NSUserDefaults standardUserDefaults] setValue:[[response valueForKey:@"details"] valueForKey:@"id"] forKey:@"Uid"];
                [[NSUserDefaults standardUserDefaults] setValue:[[response valueForKey:@"details"] valueForKey:@"firstname"] forKey:@"firstname"];
                [Singletons singleton].loginUserName = [[response valueForKey:@"details"] valueForKey:@"firstname"];
                if (savePwd)
                {
                    [[NSUserDefaults standardUserDefaults] setValue:_txtEmail.text forKey:@"savedEmail"];
                    
                }
                else{
                    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"savedEmail"];
                }
                for (UIViewController *view in self.navigationController.viewControllers) {
                    if ([view isKindOfClass:[HomeViewController class]]) {
                        HomeViewController *home = (HomeViewController *)view;
                        [self.navigationController popToViewController:home animated:NO];
                    }
                }
            }
            if ([[response valueForKey:@"status"] isEqualToString:@"ALREADY_EXISTS"]) {
                [[NSUserDefaults standardUserDefaults] setValue:[[response valueForKey:@"details"] valueForKey:@"id"] forKey:@"Uid"];
                [[NSUserDefaults standardUserDefaults] setValue:[[response valueForKey:@"details"] valueForKey:@"firstname"] forKey:@"firstname"];
                [Singletons singleton].loginUserName = [[response valueForKey:@"details"] valueForKey:@"firstname"];
                if (savePwd)
                {
                    [[NSUserDefaults standardUserDefaults] setValue:_txtEmail.text forKey:@"savedEmail"];
                    
                }
                else{
                    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"savedEmail"];
                }
                for (UIViewController *view in self.navigationController.viewControllers) {
                    if ([view isKindOfClass:[HomeViewController class]]) {
                        HomeViewController *home = (HomeViewController *)view;
                        [self.navigationController popToViewController:home animated:NO];
                    }
                }
                
            }
        }
    }];
}

- (IBAction)btnForgotSubmit:(id)sender {
    _viewForgotPassword.hidden=YES;
    _viewLogin.hidden=NO;
}
- (IBAction)btnLogin:(id)sender {
    
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"DeviceToken"];
    
    if ([_txtEmail.text isEqualToString:@""]) {
        [Global alertAdd:@"Please enter email." withTitle:nil navBack:NO];
        return;
    }
    
    else if(![Global validateEmailWithString:_txtEmail.text]){
        [Global alertAdd:@"Please enter valid email." withTitle:nil navBack:NO];
        return;
    }
    
    else if([_txtPassword.text isEqualToString:@""]){
        [Global alertAdd:@"Please enter password." withTitle:nil navBack:NO];
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:_txtPassword.text forKey:@"savedPass"];
    [[NSUserDefaults standardUserDefaults] setValue:_txtEmail.text forKey:@"savedEmail"];

    NSDictionary *dicty = [NSDictionary dictionaryWithObjectsAndKeys:_txtEmail.text,@"user",_txtPassword.text,@"pass",nil];
    [Global urlString:apiUrlLogin parameter:dicty indicator:YES currentView:self.view Get_Post:@"POST" completion:^(BOOL result, NSDictionary *response) {
        if(result) {
            
            if ([[response valueForKey:@"status"] isEqualToString:@"invalid"]) {
                [Global alertAdd:@"Your entred email or password is incorrect." withTitle:nil navBack:NO];
                _txtEmail.text = @"";
                _txtPassword.text =@"";
            }
            else if ([[response valueForKey:@"status"] isEqualToString:@"SUCCESS"]) {
                [[NSUserDefaults standardUserDefaults] setValue:_txtPassword.text forKey:@"savedPassword"];

                
                [[NSUserDefaults standardUserDefaults] setValue:[[response valueForKey:@"Details"] valueForKey:@"customer_id"] forKey:@"Uid"];
                [[NSUserDefaults standardUserDefaults] setValue:[[response valueForKey:@"Details"] valueForKey:@"first_name"] forKey:@"firstname"];
                [[NSUserDefaults standardUserDefaults] setValue:[[response valueForKey:@"Details"] valueForKey:@"cart_id"] forKey:@"cart_id"];

                //[[NSUserDefaults standardUserDefaults] setValue:[[response valueForKey:@"details"] valueForKey:@"firstname"] forKey:@"firstname"];
                [Singletons singleton].loginUserName = [[response valueForKey:@"details"] valueForKey:@"first_name"];
                if (savePwd)
                {
                    [[NSUserDefaults standardUserDefaults] setValue:_txtPassword.text forKey:@"savedPass"];
                    
                }
                else{
                    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"savedEmail"];
                }
                //                NSArray* tempVCA = [self.navigationController viewControllers];
                //
                //                for(UIViewController *tempVC in tempVCA)
                //                {
                //                    if([tempVC isKindOfClass:[urViewControllerClass class]])
                //                    {
                //                        [tempVC removeFromParentViewController];
                //                    }
                //                }
                
                
                for (UIViewController *view in self.navigationController.viewControllers) {
                    if ([view isKindOfClass:[HomeViewController class]]) {
                        HomeViewController *home = (HomeViewController *)view;
                        [self.navigationController popToViewController:home animated:NO];
                    }
                }
                //                HomeViewController *home = [[HomeViewController alloc] init];
                //                [self.navigationController pushViewController:home animated:YES];
            }
        }
    }];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (range.location == 0 && [string isEqualToString:@" "]) {
        return NO;
    }
    return YES;
}

- (IBAction)btnForgotPassword:(id)sender {
    //    _viewForgotPassword.hidden=NO;
    //    _viewLogin.hidden=YES;
    //    UIApplication *application = [UIApplication sharedApplication];
    //    NSURL *URL = [NSURL URLWithString:@"http://demo.savishops.com/customer/account/forgotpassword"];
    //    [application openURL:URL options:@{} completionHandler:^(BOOL success) {
    //        if (success) {
    //            NSLog(@"Opened url");
    //        }
    //    }];
    WebViewController *webView = [[WebViewController alloc] init];  //Change self.view.bounds to a smaller CGRect if you don't want it to take up the
    webView.urlString = @"ForgotPassword";
    [self.navigationController pushViewController:webView animated:YES];
}

- (void)isOn:(BOOL)flag {
    savePwd = flag;
}

- (IBAction)btnSMedia:(id)sender {
    
    if([sender tag] == 0) {
        FBSDKLoginManager *logMeOut = [[FBSDKLoginManager alloc] init];
        [Singletons singleton].loginType = @"fb";
        [logMeOut logOut];
        [Global loginFor:@"fbLogin"];
    }
    else {
        [Singletons singleton].loginType = @"gs";
        [Global loginFor:@"googleLogin"];
    }
}

- (IBAction)btnSignUpOnLoginClicked:(id)sender {
    SignUpViewController *signUp=[[SignUpViewController alloc]init];
    [self.navigationController pushViewController:signUp animated:YES];
}
- (IBAction)btnBack:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:true];
}

@end
